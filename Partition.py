import numpy as np
from idstl.Polytope import Polytope
from idstl.Context import Context


class Partition:
    def __init__(self, polytope: Polytope, label_ind: np.ndarray = None, ctx: Context = None):
        assert isinstance(polytope, Polytope)
        assert ctx is None or isinstance(ctx, Context)
        self.__ctx = ctx
        self.__polytope = polytope
        self.__label_ind = label_ind
        self.__idx = Context.get_num_of_partitions(self.__ctx)

    def __contains__(self, item):
        return item in self.get_polytope()

    def __lt__(self, other):
        return self.get_idx() < other.get_idx()

    def __gt__(self, other):
        return self.get_idx() > other.get_idx()

    def __eq__(self, other):
        return self.get_idx() == other.get_idx()

    def get_context(self) -> Context:
        return self.__ctx

    def get_idx(self) -> int:
        return self.__idx

    def get_polytope(self) -> Polytope:
        return self.__polytope

    def get_labels(self) -> np.ndarray:
        return Context.get_label(self.get_label_ind())

    def get_label_ind(self) -> np.ndarray:
        return self.__label_ind

    def get_dimension(self) -> int:
        return self.get_polytope().get_dimension()
