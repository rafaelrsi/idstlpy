from typing import Union
import numpy as np
from scipy import sparse
from idstl.LPModel import LPModel, INFINITY, solve_lp
from idstl.System import System
from idstl.Polytope import Polytope
from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release


def decode_bounds(lb: Union[float, int], ub: Union[float, int], field: str):
    if lb <= -INFINITY and ub >= INFINITY:
        return field + ' free\n'
    if lb <= -INFINITY:
        return field + ' <= ' + str(ub) + '\n'
    if ub >= INFINITY:
        return str(lb) + ' <= ' + field + '\n'
    return str(lb) + ' <= ' + field + ' <= ' + str(ub) + '\n'


def decode_vector(vector: np.ndarray, field: str, is_first: bool = True):
    vector = vector.flatten()
    vector_str = ''
    for i in range(vector.size):
        elem = vector[i]
        if elem != 0:
            if not is_first:
                vector_str += ' + ' if elem > 0 else ' - '
                vector_str += str(abs(elem))
                is_first = False
            else:
                if elem == -1:
                    vector_str = '-'
                elif elem != 1:
                    vector_str = str(elem) + ' * '
            if vector.size > 1:
                vector_str += field + '_' + str(i)
            else:
                vector_str += field
    return vector_str


class TunnelModel(LPModel):
    def __init__(self, dimension: int, num_of_inputs: int, delta: Union[float, int], s_bar: Union[float, int],
                 initial_state: np.ndarray, initial_invariant: Polytope, loop_initial_step: int,
                 formula: Formula, seq_of_formula_eval: np.ndarray):
        assert initial_state.ndim == 1 and initial_state.size == dimension
        assert initial_invariant.get_dimension() == dimension
        num_of_vars = 0
        lb = np.array([])
        ub = np.array([])
        super().__init__(num_of_vars, lb, ub)
        self.n = dimension
        self.m = num_of_inputs
        self.delta = delta
        self.s_bar = s_bar
        self.x0 = np.reshape(initial_state, (dimension, 1))
        self.transitions = np.array([], dtype=object)
        self.self_loops = np.array([], dtype=object)
        self.invariants = np.array([initial_invariant], dtype=object)
        self.num_of_self_loops = np.array([], dtype=int)
        self.dyn_equalities = np.array([], dtype=int)
        self.slack_inequality = None
        self.loop_inequality = None
        self.phi = formula
        self.seq_of_formula_eval = seq_of_formula_eval
        self.clock = np.array([0], dtype=int)
        self.loop_initial_step = loop_initial_step
        self.max_clock = self.phi.get_upper_time_bound()
        self.zero_prefix_step = None

    def __str__(self):
        from idstl.Formula import AtomicProposition
        steps = np.arange(self.num_of_steps + 1)
        trace = np.zeros(self.num_of_steps + 1, dtype=np.object)
        for phi in self.phi.get_closure():
            if isinstance(phi, AtomicProposition):
                phi_idx = phi.get_idx()
                for i in steps[self.seq_of_formula_eval[phi_idx, :]]:
                    if not isinstance(trace[i], np.ndarray):
                        trace[i] = np.array([str(phi)], dtype=np.object)
                    else:
                        trace[i] = np.append(trace[i], str(phi))
        for i in range(trace.size):
            if i < self.num_of_self_loops.size:
                if not isinstance(trace[i], np.ndarray):
                    trace[i] = '{ }^' + str(int(self.num_of_self_loops[i]))
                else:
                    trace[i] = '{' + str.join(', ', trace[i]) + '}^' + str(int(self.num_of_self_loops[i]))
            else:
                if not isinstance(trace[i], np.ndarray):
                    trace[i] = '{ }'
                else:
                    trace[i] = '{' + str.join(', ', trace[i]) + '}'
        if 0 < self.loop_initial_step < self.num_of_steps:
            trace = str.join(' ', trace[:self.loop_initial_step]) + ' ( ' \
                    + str.join(' ', trace[self.loop_initial_step:]) + ' )^w'
        else:
            trace = str.join(' ', trace)
        return 'ZeroPrefix(' + str(self.zero_prefix_step) + ', ' + str(trace) + ') = ' + str(self.objVal)

    def decode_bounds(self):
        b_str = ''
        for step in range(self.num_of_steps):
            for i in range(self.num_of_self_loops[step]):
                for j in self.s_ind(step, i):
                    assert len(self.s_ind(step, i)) == 1
                    b_str += decode_bounds(self.lb[j], self.ub[j], 's[' + str(step) + ', ' + str(i) + ']')
                for j in self.v_ind(step, i):
                    b_str += decode_bounds(self.lb[j], self.ub[j], 'v[' + str(step) + ', ' + str(i) + ']_' + str(j))
                for j in self.u_ind(step, i):
                    b_str += decode_bounds(self.lb[j], self.ub[j], 'u[' + str(step) + ', ' + str(i) + ']_' + str(j))
                for j in self.x_ind(step, i):
                    b_str += decode_bounds(self.lb[j], self.ub[j], 'x[' + str(step) + ', ' + str(i) + ']_' + str(j))
        return b_str

    def decode_lhs_row(self, lhs_row: np.ndarray):
        lhs_str = ''
        for step in range(self.num_of_steps):
            for i in range(self.num_of_self_loops[step]):
                lhs_str += decode_vector(lhs_row[self.s_ind(step, i)], 's[' + str(step) + ', ' + str(i) + ']',
                                         is_first=len(lhs_str) == 0)
                lhs_str += decode_vector(lhs_row[self.v_ind(step, i)], 'v[' + str(step) + ', ' + str(i) + ']',
                                         is_first=len(lhs_str) == 0)
                lhs_str += decode_vector(lhs_row[self.u_ind(step, i)], 'u[' + str(step) + ', ' + str(i) + ']',
                                         is_first=len(lhs_str) == 0)
                lhs_str += decode_vector(lhs_row[self.x_ind(step, i)], 'x[' + str(step) + ', ' + str(i) + ']',
                                         is_first=len(lhs_str) == 0)
        return lhs_str

    def decode_const(self, const_idx: int, sense: str,
                     lhs_V: np.ndarray, lhs_I: np.ndarray, lhs_J: np.ndarray, rhs_V: np.ndarray, rhs_I: np.ndarray):
        if lhs_V.ndim == 0:
            assert lhs_I.ndim == 0 and lhs_J.ndim == 0
            lhs_V = np.array([lhs_V])
            lhs_I = np.array([lhs_I])
            lhs_J = np.array([lhs_J])
        assert lhs_V.ndim == 1 and lhs_I.ndim == 1 and lhs_J.ndim == 1
        assert lhs_V.size == lhs_I.size and lhs_V.size == lhs_J.size
        assert lhs_I.size > 0
        if rhs_I.size > 0 and rhs_I.ndim == 0:
            assert rhs_V.ndim == 0
            rhs_V = np.array([rhs_V])
            rhs_I = np.array([rhs_I])
        assert rhs_I.size == 0 or (rhs_I.ndim == 1 and rhs_V.ndim == 1 and rhs_V.size == rhs_I.size)
        const_str = ''
        if rhs_I.size > 0:
            rhs_J = np.zeros(rhs_V.shape, dtype=int)
            min_I = int(np.minimum(np.min(lhs_I), np.min(rhs_I)))
            num_of_rows = 1 + int(np.maximum(np.max(lhs_I), np.max(rhs_I)) - min_I)
            b = sparse.coo_matrix((rhs_V, (rhs_I.astype(int) - min_I, rhs_J.astype(int))),
                                  shape=(num_of_rows, 1)).toarray()
        else:
            min_I = int(np.min(lhs_I))
            num_of_rows = 1 + int(np.max(lhs_I) - min_I)
            b = np.zeros((num_of_rows, 1))
        A = sparse.coo_matrix((lhs_V, (lhs_I.astype(int) - min_I, lhs_J.astype(int))),
                              shape=(num_of_rows, self.num_of_vars)).toarray()
        for row in range(A.shape[0]):
            const_str += '(' + str(const_idx) + ', ' + str(row) + ') ' + self.decode_lhs_row(A[row, :])
            const_str += ' ' + sense + ' '
            const_str += str(b[row, 0])
            const_str += '\n'
        return const_str

    def decode_inequality(self, inequality_idx: int):
        lhs_range = self.inequality_idx_lhs_range[inequality_idx]
        rhs_range = self.inequality_idx_rhs_range[inequality_idx]
        return self.decode_const(
            const_idx=inequality_idx, sense='<=',
            lhs_V=self.A_ub_V[lhs_range],
            lhs_I=self.A_ub_I[lhs_range],
            lhs_J=self.A_ub_J[lhs_range],
            rhs_V=self.b_ub_V[rhs_range],
            rhs_I=self.b_ub_I[rhs_range]
        )

    def decode_equality(self, equality_idx: int):
        lhs_range = self.equality_idx_lhs_range[equality_idx]
        rhs_range = self.equality_idx_rhs_range[equality_idx]
        return self.decode_const(
            const_idx=equality_idx, sense='==',
            lhs_V=self.A_eq_V[lhs_range],
            lhs_I=self.A_eq_I[lhs_range],
            lhs_J=self.A_eq_J[lhs_range],
            rhs_V=self.b_eq_V[rhs_range],
            rhs_I=self.b_eq_I[rhs_range]
        )

    def to_lp(self, filename: str = None):
        if filename is None:
            filename = 'lp.txt'
        f = open(filename, 'w+')
        f.write('min ' + self.decode_lhs_row(self.c) + '\nsubject to\n')
        for inequality_idx in range(self.num_of_inequalities):
            f.write(self.decode_inequality(inequality_idx))
        for equality_idx in range(self.num_of_equalities):
            f.write(self.decode_equality(equality_idx))
        f.write(self.decode_bounds())
        f.close()

    @property
    def num_of_steps(self):
        return self.num_of_self_loops.size

    def q2(self, step: int) -> System:
        return self.transitions[step]

    def q1(self, step: int) -> System:
        return self.self_loops[step]

    def X1(self, step: int) -> Polytope:
        return self.invariants[step]

    def X2(self, step: int) -> Polytope:
        return self.invariants[step + 1]

    def u1_lb(self, step: int):
        return self.U1(step).get_lb()

    def u1_ub(self, step: int):
        return self.U1(step).get_ub()

    def u2_lb(self, step: int):
        return self.U2(step).get_lb()

    def u2_ub(self, step: int):
        return self.U2(step).get_ub()

    def x1_lb(self, step: int):
        return self.X1(step).get_lb()

    def x1_ub(self, step: int):
        return self.X1(step).get_ub()

    def x2_lb(self, step: int):
        return self.X2(step).get_lb()

    def x2_ub(self, step: int):
        return self.X2(step).get_ub()

    def N(self, step: int) -> int:
        return int(self.num_of_self_loops[step])

    def U1(self, step: int) -> Polytope:
        return self.q1(step).get_U()

    def U2(self, step: int) -> Polytope:
        return self.q2(step).get_U()

    def step_start(self, step: int, i: int) -> int:
        return 1 + (1 + 2 * self.n + self.m) * (i + np.sum(self.num_of_self_loops[:step]))

    def v_ind(self, step: int, i: int) -> range:
        start = self.step_start(step, i)
        return range(start, start + self.n)

    def u_ind(self, step: int, i: int) -> range:
        start = self.step_start(step, i) + self.n
        return range(start, start + self.m)

    def x_ind(self, step: int, i: int) -> range:
        start = self.step_start(step, i - 1) + self.n + self.m
        return range(start, start + self.n)

    def s_ind(self, step: int, i: int) -> range:
        start = self.step_start(step, i - 1) + 2 * self.n + self.m
        return range(start, start + 1)

    def D(self, step: int) -> int:
        return self.dyn_equalities[step]

    def get_trajectory(self):
        # X = [s_0,v_0,u_0,x_1,s_1,...,v_i-1,u_i-1,x_i,s_i,...,s_K-1,v_K-1,u_K-1,x_K]
        K = self.K
        L = self.L
        x = self.x
        x = np.reshape(x, (K, 1 + 2 * self.n + self.m))
        u = x[:, 1 + self.n:1 + self.n + self.m]
        if L is not None and 0 < L <= K:
            u = np.vstack((u, u[L - 1, :]))
        else:
            u = np.vstack((u, np.zeros(self.m)))
        x = np.vstack((self.x0.flatten(), x[:, 1 + self.n + self.m:]))
        return self.objVal, x, u

    def append_transition(self, transition: System, target: Polytope, self_loop: System):
        assert transition.get_dimension() == self.n
        assert transition.get_num_of_inputs() == self.m
        assert target.get_dimension() == self.n
        assert self_loop.get_dimension() == self.n
        assert self_loop.get_num_of_inputs() == self.m
        self.transitions = np.hstack((self.transitions, transition))
        self.invariants = np.hstack((self.invariants, target))
        self.self_loops = np.hstack((self.self_loops, self_loop))
        self.num_of_self_loops = np.hstack((self.num_of_self_loops, 0))
        self.clock = np.hstack((self.clock, self.clock[-1]))
        self.dyn_equalities = np.hstack((self.dyn_equalities, -1))
        self.add_self_loop_at(self.transitions.size - 1)

    def create_loop(self):
        if self.loop_inequality is not None:
            self.del_inequality(self.loop_inequality)
        K = self.K
        L = self.L
        if L is not None and 0 < L <= K:
            if self.loop_inequality is None:
                self.loop_inequality = self.num_of_inequalities
            if L == 1:
                self.add_equality(
                    equality_idx=self.loop_inequality,
                    lhs_V=np.ones(self.n),
                    lhs_I=np.arange(self.n),
                    lhs_J=np.array(self.x_ind(0, K)),
                    rhs_V=self.x0.flatten(),
                    rhs_I=np.arange(self.n)
                )
            else:
                self.add_equality(
                    equality_idx=self.loop_inequality,
                    lhs_V=np.hstack((np.ones(self.n), -np.ones(self.n))),
                    lhs_I=np.hstack((np.arange(self.n), np.arange(self.n))),
                    lhs_J=np.hstack((np.array(self.x_ind(0, K)), np.array(self.x_ind(0, L - 1)))),
                    rhs_V=np.zeros(0),
                    rhs_I=np.zeros(0)
                )

    def add_variables_at(self, idx: int, lb: np.ndarray, ub: np.ndarray, c: np.ndarray = None):
        assert lb.ndim == 1 and ub.ndim == 1 and lb.size == ub.size
        assert 0 <= idx <= self.num_of_vars
        num_of_vars = lb.size
        if c is None:
            c = np.zeros(num_of_vars)
        self.c = np.hstack((self.c[:idx], c, self.c[idx:]))
        self.lb = np.hstack((self.lb[:idx], lb, self.lb[idx:]))
        self.ub = np.hstack((self.ub[:idx], ub, self.ub[idx:]))
        self.A_ub_J[self.A_ub_J >= idx] += num_of_vars
        self.A_eq_J[self.A_eq_J >= idx] += num_of_vars
        self.num_of_vars += num_of_vars

    def add_proxy_const(self, inequality_idx: int, step: int, i: int):
        self.add_inequality(
            inequality_idx=inequality_idx,
            lhs_V=np.hstack((
                np.ones(self.n), -np.ones(self.n),  # v_i <= s_i
                -np.ones(self.n), -np.ones(self.n)  # - v_i <= s_i
            )),
            lhs_I=np.hstack((
                np.arange(self.n), np.arange(self.n),  # v_i <= s_i
                np.arange(self.n, 2 * self.n), np.arange(self.n, 2 * self.n)  # - v_i <= s_i
            )),
            lhs_J=np.hstack((
                self.v_ind(step, i), np.repeat(self.s_ind(step, i), self.n),  # v_i <= s_i
                self.v_ind(step, i), np.repeat(self.s_ind(step, i), self.n)  # - v_i <= s_i
            )),
            rhs_V=np.zeros(0),
            rhs_I=np.zeros(0)
        )

    def add_polytope_at(self, inequality_idx: int, equality_idx: int, var_offset: int, polytope):
        from idstl.Polytope import Polytope
        assert isinstance(polytope, Polytope)
        if polytope.get_num_of_inequalities() > 0:
            A_ub = polytope.get_A_ub()
            lhs = sparse.coo_matrix((A_ub.data, (A_ub.row, A_ub.col + var_offset)))
            rhs = polytope.get_b_ub()
            self.add_inequality_from_sparse(inequality_idx=inequality_idx, lhs=lhs, rhs=rhs)
        if polytope.get_num_of_equalities() > 0:
            A_eq = polytope.get_A_eq()
            lhs = sparse.coo_matrix((A_eq.data, (A_eq.row, A_eq.col + var_offset)))
            rhs = polytope.get_b_eq()
            self.add_equality_from_sparse(equality_idx=equality_idx, lhs=lhs, rhs=rhs)

    def replace_ineq_cols(self, inequality_idx: int, J_bef: range, J_aft: range):
        M_I = self.inequality_idx_lhs_range[inequality_idx]
        M_J = self.A_ub_J[M_I]
        M_J[np.isin(M_J, J_bef)] -= J_bef.start - J_aft.start
        self.A_ub_J[M_I] = M_J

    def replace_eq_cols(self, equality_idx: int, J_bef: range, J_aft: range):
        M_I = self.equality_idx_lhs_range[equality_idx]
        M_J = self.A_eq_J[M_I]
        M_J[np.isin(M_J, J_bef)] -= J_bef.start - J_aft.start
        self.A_eq_J[M_I] = M_J

    def add_self_loop_at(self, step: int):
        """
        X_step = [s_0,v_0,u_0,x_1,s_1,...,v_i-1,u_i-1,x_i,s_i,...,s_N-1,v_N-1,u_N-1,x_N]

        where:
        * s_i is an slack variable (R)
        * v_i is a vector of proxy slack variables (R^n)
        * u_i is a vector of input variables (R^m)
        * x_i is a vector of state variables (R^n)

        v_0,...,v_N-1 free
        s_0,...,s_N-1 >= 0
        x_N = A2 x_i-1 + B2 u_i-1 + c2 + v_i-1 (where q2 = <A2,B2,c2>)
        x_N in X2
        u_N-1 in U2
        v_N-1 <= s_N-1, -v_N-1 <= s_N-1
        for i = 1..N-1
        x_i = A1 x_i-1 + B1 u_i-1 + c1 + v_i-1 (where q1 = <A1,B1,c1>)
        x_i in X1
        u_i-1 in U1
        v_i-1 <= s_i-1, -v_i-1 <= s_i-1


        """
        self.num_of_self_loops[step] += 1
        self.clock[step:] += 1
        z_n = np.zeros((self.n, 1))
        I_n = np.identity(self.n)
        if step == 0 and self.N(step) == 1:
            self.add_variables_at(
                idx=self.s_ind(step, 0).start,
                lb=np.hstack((0.0, -INFINITY * np.ones(self.n), self.u2_lb(step), self.x2_lb(step))),
                ub=np.hstack((INFINITY, INFINITY * np.ones(self.n), self.u2_ub(step), self.x2_ub(step)))
            )
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.x_ind(step, 1).start,
                                 polytope=self.X2(step))
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.u_ind(step, 0).start,
                                 polytope=self.U2(step))
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            self.dyn_equalities[step] = self.num_of_equalities
            self.add_equality_from_sparse(
                equality_idx=self.D(step),
                lhs=sparse.hstack((z_n, -I_n, -self.q2(step).get_B(), I_n), format='coo'),
                rhs=sparse.coo_matrix(self.q2(step).get_c() + self.q2(step).get_A() @ self.x0)
            )
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            # (D<step>) x_1 = A2 @ x0 + B2 @ u_0 + c2 + v_0
            self.add_proxy_const(inequality_idx=self.num_of_inequalities, step=step, i=0)
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            # (D<step>) x_1 = A2 @ x0 + B2 @ u_0 + c2 + v_0
            # v_0 <= s_0, -v_0 <= s_0
        elif step > 0 and self.N(step) == 1:
            self.add_variables_at(
                idx=self.s_ind(step, 0).start,
                lb=np.hstack((0.0, -INFINITY * np.ones(self.n), self.u2_lb(step), self.x2_lb(step))),
                ub=np.hstack((INFINITY, INFINITY * np.ones(self.n), self.u2_ub(step), self.x2_ub(step)))
            )
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.x_ind(step, 1).start,
                                 polytope=self.X1(step))
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.u_ind(step, 0).start,
                                 polytope=self.U1(step))
            self.dyn_equalities[step] = self.num_of_equalities
            lhs = sparse.hstack((-self.q2(step).get_A(), np.zeros((self.n, 1)), -I_n, -self.q2(step).get_B(), I_n),
                                format='coo')
            self.add_equality_from_sparse(
                equality_idx=self.D(step),
                lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.x_ind(step, 0).start))),
                rhs=self.q2(step).get_c()
            )
            self.add_proxy_const(inequality_idx=self.num_of_inequalities, step=step, i=0)
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            # (D<step>) x_1 = A2 @ x_0 + B2 @ u_0 + c2 + v_0
            # v_0 <= s_0, -v_0 <= s_0
        elif step == 0 and self.N(step) == 2:
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            # (D<step>) x_1 = A2 @ x0 + B2 @ u_0 + c2 + v_0
            # v_0 <= s_0, -v_0 <= s_0
            self.del_equality(self.D(step))
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            # v_0 <= s_0, -v_0 <= s_0
            self.add_variables_at(
                idx=self.s_ind(step, 0).start,
                lb=np.hstack((0.0, -INFINITY * np.ones(self.n), self.u1_lb(step), self.x1_lb(step))),
                ub=np.hstack((INFINITY, INFINITY * np.ones(self.n), self.u1_ub(step), self.x1_ub(step)))
            )
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.x_ind(step, 1).start,
                                 polytope=self.X1(step))
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.u_ind(step, 0).start,
                                 polytope=self.U1(step))
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            # x_1 in X1
            # u_0 in U1
            lhs = sparse.hstack((-self.q2(step).get_A(), np.zeros((self.n, 1)), -I_n, -self.q2(step).get_B(), I_n),
                                format='coo')
            self.add_equality_from_sparse(
                equality_idx=self.num_of_equalities,
                lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.x_ind(step, 1).start))),
                rhs=self.q2(step).get_c()
            )
            self.add_equality_from_sparse(
                equality_idx=self.D(step),
                lhs=sparse.hstack((z_n, -I_n, -self.q1(step).get_B(), I_n), format='coo'),
                rhs=sparse.coo_matrix(self.q1(step).get_c() + self.q1(step).get_A() @ self.x0)
            )
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            # x_1 in X1
            # u_0 in U1
            # x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
            # (D<step>) x_1 = A1 @ x0 + B1 @ u_0 + c1 + v_0
            self.add_proxy_const(inequality_idx=self.num_of_inequalities, step=step, i=0)
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            # x_1 in X1
            # u_0 in U1
            # x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
            # (D<step>) x_1 = A1 @ x0 + B1 @ u_0 + c1 + v_0
            # v_0 <= s_0, -v_0 <= s_0
        elif step > 0 and self.N(step) == 2:
            # output:
            # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub
            # x_1 in X2
            # u_0 in U2
            # (D<step>) x_1 = A2 @ x_0 + B2 @ u_0 + c2 + v_0
            # v_0 <= s_0, -v_0 <= s_0
            self.add_variables_at(
                idx=self.s_ind(step, 0).start,
                lb=np.hstack((0.0, -INFINITY * np.ones(self.n), self.u1_lb(step), self.x1_lb(step))),
                ub=np.hstack((INFINITY, INFINITY * np.ones(self.n), self.u1_ub(step), self.x1_ub(step)))
            )
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # (D<step>) x_2 = A2 @ x_0 + B2 @ u_1 + c2 + v_1
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            self.replace_eq_cols(self.D(step), self.x_ind(step, 0), self.x_ind(step, 1))
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # (D<step>) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.x_ind(step, 1).start,
                                 polytope=self.X1(step))
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.u_ind(step, 0).start,
                                 polytope=self.U1(step))
            self.dyn_equalities[step] = self.num_of_equalities
            lhs = sparse.hstack((-self.q1(step).get_A(), np.zeros((self.n, 1)), -I_n, -self.q1(step).get_B(), I_n),
                                format='coo')
            self.add_equality_from_sparse(
                equality_idx=self.D(step),
                lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.x_ind(step, 0).start))),
                rhs=self.q1(step).get_c()
            )
            self.add_proxy_const(inequality_idx=self.num_of_inequalities, step=step, i=0)
            # output:
            # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub
            # x_2 in X2
            # u_1 in U2
            # x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
            # v_1 <= s_1, -v_1 <= s_1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            # x_1 in X1
            # u_0 in U1
            # (D<step>) x_1 = A2 @ x_0 + B2 @ u_0 + c2 + v_0
            # v_0 <= s_0, -v_0 <= s_0
        else:
            # output:
            # 0 <= s_N-2, v_N-2 free, u2_lb <= u_N-2 <= u2_ub, x2_lb <= x_N-1 <= x2_ub
            # x_N-1 in X2
            # u_N-2 in U2
            # v_N-2 <= s_N-2, -v_N-2 <= s_N-2
            # x_N-1 = A2 @ x_N-2 + B2 @ u_N-2 + c2 + v_N-2
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_1 <= x1_ub
            # x_1 in X1
            # u_0 in U1
            # (D<step>) x_1 = A1 @ x0 + B1 @ u_0 + c1 + v_0
            # for i = N-3..1
            # x1_lb <= x_i <= x1_ub, 0 <= s_i, v_i free, u1_lb <= u_i <= u1_ub
            # x_i in X1
            # u_i in U1
            # x_i+1 = A1 @ x_i + B1 @ u_i + c1 + v_i
            # v_i <= s_1, -v_i <= s_1
            self.add_variables_at(
                idx=self.x_ind(step, 1).start,
                lb=np.hstack((self.x1_lb(step), 0.0, -INFINITY * np.ones(self.n), self.u1_lb(step))),
                ub=np.hstack((self.x1_ub(step), INFINITY, INFINITY * np.ones(self.n), self.u1_ub(step)))
            )
            # output:
            # 0 <= s_N-1, v_N-1 free, u2_lb <= u_N-1 <= u2_ub, x2_lb <= x_N <= x2_ub
            # x_N in X2
            # u_N-1 in U2
            # v_N-1 <= s_N-1, -v_N-1 <= s_N-1
            # x_N = A2 @ x_N-1 + B2 @ u_N-1 + c2 + v_N-1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub
            # x_2 in X1
            # u_0 in U1
            # (D<step>) x_2 = A1 @ x0 + B1 @ u_0 + c1 + v_0
            # for i = N-2..2
            # x1_lb <= x_i <= x1_ub, 0 <= s_i, v_i free, u1_lb <= u_i <= u1_ub
            # x_i in X1
            # u_i in U1
            # x_i+1 = A1 @ x_i + B1 @ u_i + c1 + v_i
            # v_i <= s_1, -v_i <= s_1
            # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_i free, u1_lb <= u_1 <= u1_ub
            self.replace_eq_cols(self.D(step), self.x_ind(step, 2), self.x_ind(step, 1))
            # output:
            # 0 <= s_N-1, v_N-1 free, u2_lb <= u_N-1 <= u2_ub, x2_lb <= x_N <= x2_ub
            # x_N in X2
            # u_N-1 in U2
            # v_N-1 <= s_N-1, -v_N-1 <= s_N-1
            # x_N = A2 @ x_N-1 + B2 @ u_N-1 + c2 + v_N-1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub
            # x_2 in X1
            # u_0 in U1
            # (D<step>) x_1 = A1 @ x0 + B1 @ u_0 + c1 + v_0
            # for i = N-2..2
            # x1_lb <= x_i <= x1_ub, 0 <= s_i, v_i free, u1_lb <= u_i <= u1_ub
            # x_i in X1
            # u_i in U1
            # x_i+1 = A1 @ x_i + B1 @ u_i + c1 + v_i
            # v_i <= s_1, -v_i <= s_1
            # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_i free, u1_lb <= u_1 <= u1_ub
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.x_ind(step, 1).start,
                                 polytope=self.X1(step))
            self.add_polytope_at(inequality_idx=self.num_of_inequalities,
                                 equality_idx=self.num_of_equalities,
                                 var_offset=self.u_ind(step, 1).start,
                                 polytope=self.U1(step))
            lhs = sparse.hstack((-self.q1(step).get_A(), np.zeros((self.n, 1)), -I_n, -self.q1(step).get_B(), I_n),
                                format='coo')
            self.add_equality_from_sparse(
                equality_idx=self.num_of_equalities,
                lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.x_ind(step, 1).start))),
                rhs=self.q1(step).get_c()
            )
            self.add_proxy_const(inequality_idx=self.num_of_inequalities, step=step, i=1)
            # output:
            # 0 <= s_N-1, v_N-1 free, u2_lb <= u_N-1 <= u2_ub, x2_lb <= x_N <= x2_ub
            # x_N in X2
            # u_N-1 in U2
            # v_N-1 <= s_N-1, -v_N-1 <= s_N-1
            # x_N = A2 @ x_N-1 + B2 @ u_N-1 + c2 + v_N-1
            # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub
            # x_2 in X1
            # u_0 in U1
            # (D<step>) x_2 = A1 @ x0 + B1 @ u_0 + c1 + v_0
            # for i = N-2..1
            # x1_lb <= x_i <= x1_ub, 0 <= s_i, v_i free, u1_lb <= u_i <= u1_ub
            # x_i in X1
            # u_i in U1
            # x_i+1 = A1 @ x_i + B1 @ u_i + c1 + v_i
            # v_i <= s_1, -v_i <= s_1

    def add_slack_const(self, inequality_idx: int, step: int, i: int):
        self.add_inequality(
            inequality_idx=inequality_idx,
            lhs_V=np.hstack((
                (self.delta / self.s_bar), -1,  # (delta/s_bar) s_i <= s_i+1
            )),
            lhs_I=np.hstack((
                0, 0,  # (delta/s_bar) s_i <= s_i+1
            )),
            lhs_J=np.hstack((
                self.s_ind(step, i), self.s_ind(step, i + 1),  # (delta/s_bar) s_i <= s_i+1
            )),
            rhs_V=np.zeros(0),
            rhs_I=np.zeros(0)
        )

    def zero_prefix(self, step: int = None):
        if step is None:
            step = self.num_of_steps - 1
        self.create_loop()
        if self.slack_inequality is not None:
            self.del_inequality(self.slack_inequality)
            self.c = np.zeros(self.c.shape)
            self.slack_inequality = None
        s_ind = np.array([self.s_ind(j, i) for j in range(step + 1) for i in range(self.N(j))]).flatten()
        self.set_objective(1, s_ind)
        if s_ind.size > 1:
            self.slack_inequality = self.num_of_inequalities
            # sum((s_bar/delta) s_0,...,s_K* - 1) <= s_K*
            self.add_inequality(
                inequality_idx=self.slack_inequality,
                lhs_V=np.hstack((np.repeat((self.s_bar / self.delta), s_ind.size - 1), -1)),
                lhs_I=np.zeros(s_ind.size),  # (s_bar/delta) s_i <= s_i+1
                lhs_J=s_ind,
                rhs_V=np.zeros(0),
                rhs_I=np.zeros(0)
            )
        Aub = None
        bub = None
        Aeq = None
        beq = None
        if self.num_of_inequalities > 0:
            Aub = self.get_A_ub()
            bub = self.get_b_ub().toarray().flatten()
        if self.num_of_equalities > 0:
            Aeq = self.get_A_eq()
            beq = self.get_b_eq().toarray().flatten()
        elapsed_time, self.is_optimal, self.objVal, self.x = solve_lp(
            self.c, self.lb, self.ub, Aub, bub, Aeq, beq, LPModel.env, require_optimal=True
        )
        return elapsed_time

    def is_sat(self):
        return self.check(self.phi, 0)

    def is_unsat(self):
        return self.K >= self.max_clock

    @property
    def K(self) -> int:
        return int(self.clock[-1])
    
    @property
    def L(self) -> int:
        if 0 < self.loop_initial_step <= self.num_of_self_loops.size:
            return self.clock_lb(self.loop_initial_step)
        return self.K + 1

    def clock_lb(self, idx: int) -> int:
        return self.clock[idx]

    def clock_ub(self, idx: int) -> int:
        return self.clock[idx + 1]

    @property
    def formula(self) -> Formula:
        return self.phi

    @property
    def formula_evaluation_array(self) -> np.ndarray:
        return self.seq_of_formula_eval

    @property
    def formula_upper_time_bound(self) -> int:
        return self.max_clock

    def __check_atomic_proposition(self, formula: AtomicProposition, idx: int) -> (bool, bool):
        return self.seq_of_formula_eval[formula.get_idx(), idx] is True

    def __check_not(self, formula: Not, idx: int) -> (bool, bool):
        return self.seq_of_formula_eval[formula.get_idx(), idx] is True \
               and self.__check_atomic_proposition(formula.get_child(), idx) is False

    def __check_and(self, formula: And, idx: int) -> bool:
        return self.seq_of_formula_eval[formula.get_idx(), idx] is True \
               and all([self.check(formula.get_child(child_idx), idx)
                        for child_idx in range(formula.get_num_of_children())])

    def __check_or(self, formula: Or, idx: int) -> bool:
        return self.seq_of_formula_eval[formula.get_idx(), idx] is True \
               and any([self.check(formula.get_child(child_idx), idx)
                        for child_idx in range(formula.get_num_of_children())])

    def __check_until(self, formula: Until, idx: int) -> bool:
        """

        Let trace = L(q_0)L(q_1)...L(q_{L-1}) ( L(q_L)...L(q_K) )^w

        if k < L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_{L-1}) ( L(q_L)...L(q_K) )^w,

        if k > L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_K) ( L(q_{L + K + 1 - k})...L(q_{2 * K + 1 - k}) )^w

        --------------


        (K,L)-trace semantics (i.e., L(q_k)L(q_{k+1})...L(q_{L-1}) ( L(q_L)...L(q_K) )^w:


        Suffix(k, trace) [a,b] satisfies phi_1 U[a,b] phi_2 iff:

        * there exists kp in [k..K] AND [k + a..k + b] such that Suffix(kp, trace) [a,b] satisfies phi_2 
        and for all kpp in [k..kp], Suffix(kpp, trace) [a,b] satisfies phi_1; or,
        * there exists kp in [L..K] such that Suffix(kp, trace) [a,b] satisfies phi_2 and for all kpp in [k..K], 
        Suffix(kpp, trace) [a,b] satisfies phi_1; or,


        :param formula:
        :param idx:
        :return: Suffix(k, trace) satisfies phi
        """
        if self.seq_of_formula_eval[formula.get_idx(), idx] is True:
            phi_1 = formula.get_phi_1()
            phi_2 = formula.get_phi_2()
            a, b = formula.get_interval()
            try:
                ip = next(i for i in range(self.num_of_steps) if self.check(phi_2, i) is True)
            except StopIteration:
                return False
            if (self.clock_lb(idx) + a <= self.clock_ub(ip)
                    and self.clock_lb(ip) <= self.clock_ub(idx) + b):
                return all([self.check(phi_1, i) for i in range(idx, ip)])
            return all([self.check(phi_1, i) for i in range(idx, self.num_of_steps)])
        return False

    def __check_release(self, formula: Release, idx: int) -> bool:
        """

        Let trace = L(q_0)L(q_1)...L(q_{L-1}) ( L(q_L)...L(q_K) )^w

        if k < L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_{L-1}) ( L(q_L)...L(q_K) )^w,

        if k >= L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_K) ( L(q_{L + K + 1 - k})...L(q_{2 * K + 1 - k}) )^w

        ---------------

        (K,L)-trace semantics (i.e., Suffix(k, trace) = L(q_0)L(q_1)...L(q_{L-1}) ( L(q_L)...L(q_K) )^w:

        Suffix(k, trace) [a,b] satisfies phi iff:

        * there exists kp in [k + a..k + b] such that Suffix(kp, trace) [a,b] satisfies phi_1 and for all kpp 
        in [k + a..kp], Suffix(kpp, trace) [a,b] satisfies phi_2; or,
        * there exists kp in [L..K] such that Suffix(kp, trace) [a,b] satisfies phi_1 and for all kpp in [L..K], 
        Suffix(kpp, trace) [a,b] satisfies phi_2; or,
        * there exists NO kp in [k..K] such that Suffix(kp, trace) [a,b] satisfies phi_1, AND k + b <= K, AND 
        for all kpp in [k + a..k + b], Suffix(kpp, trace) [a,b] satisfies phi_2; or,
        * there exists NO kp in [k..K] such that Suffix(kp, trace) [a,b] satisfies phi_1, AND K < k + b, 
        AND b - a <= K - L, AND for all kpp in [L..L + K - k - b] OR [k + a..K], Suffix(kpp, trace) [a,b] 
        satisfies phi_2; or,
        * there exists NO kp in [k..K] such that Suffix(kp, trace) [a,b] satisfies phi_1, AND K < k + b, 
        AND b - a > K - L, AND for all kpp in [min(k + a, L)..K], Suffix(kpp, trace) [a,b] satisfies phi_2; or,


        :param formula:
        :param idx:
        :return: Suffix(k, trace) satisfies phi
        """
        if self.seq_of_formula_eval[formula.get_idx(), idx] is True:
            phi_1 = formula.get_phi_1()
            phi_2 = formula.get_phi_2()
            a, b = formula.get_interval()
            k = {'a': self.clock_lb(idx), 'b': self.clock_ub(idx)}
            try:
                ip = next(i for i in range(self.num_of_steps) if self.check(phi_1, i) is True)
                if (self.clock_lb(idx) + a <= self.clock_ub(ip)
                        and self.clock_lb(ip) <= self.clock_ub(idx) + b):
                    return all([self.check(phi_2, i) for i in range(ip + 1)
                                if k['a'] + a < self.clock_ub(i) and self.clock_lb(i) < k['b'] + b])
            except StopIteration:
                if idx < self.loop_initial_step:
                    L = {'a': self.K - k['b'], 'b': self.K - k['a']}
                    K = {'a': self.K - self.L + L['a'],
                         'b': self.K - self.L + L['b']}
                    if k['b'] + b <= K['a']:
                        # => k['b'] + a <= K['a']
                        return all([self.check(phi_2, i) for i in range(self.num_of_steps)
                                    if k['a'] + a < self.clock_ub(i) and self.clock_lb(i) < k['b'] + b])
                    elif b - a <= K['b'] - L['a']:
                        return all([self.check(phi_2, i) for i in range(self.num_of_steps)
                                    if ((self.loop_initial_step <= i
                                         and self.clock_lb(i) < L['b'] + K['b'] - k['b'] - b)
                                        or k['a'] + a < self.clock_ub(i))])
                    else:
                        return all([self.check(phi_2, i) for i in range(self.num_of_steps)
                                    if self.loop_initial_step <= i or k['a'] + a < self.clock_ub(i)])
            return all([self.check(phi_2, i) for i in range(self.loop_initial_step, self.num_of_steps)])
        return False

    def check(self, formula: Formula, idx: int = 0) -> bool:
        ret_val = formula.get_value()
        if isinstance(formula, AtomicProposition):
            ret_val = self.__check_atomic_proposition(formula, idx)
        elif isinstance(formula, Not):
            ret_val = self.__check_not(formula, idx)
        elif isinstance(formula, And):
            ret_val = self.__check_and(formula, idx)
        elif isinstance(formula, Or):
            ret_val = self.__check_or(formula, idx)
        elif isinstance(formula, Until):
            ret_val = self.__check_until(formula, idx)
        elif isinstance(formula, Release):
            ret_val = self.__check_release(formula, idx)
        return ret_val
