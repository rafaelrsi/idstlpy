# README #

Iterative Deepening Signal Temporal Logic

# Dependences

* Gurobi
* Z3

# References

da Silva, Rafael Rodrigues, Vince Kurtz, and Hai Lin. "Automatic Trajectory Synthesis for Real-Time Temporal Logic." arXiv preprint arXiv:2009.06436 (2020).

da Silva, Rafael Rodrigues, and Hai Lin. "Scalable Integrated Task and Motion Planning from Signal Temporal Logic Specifications." arXiv preprint arXiv:1803.11247 (2018).