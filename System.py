from typing import Union
import numpy as np
from scipy import sparse
from idstl.Polytope import Polytope
from idstl.LPModel import LPModel


class System:
    def __init__(self, dimension: int, num_of_inputs: int,
                 A: Union[list, np.ndarray, sparse.spmatrix],
                 B: Union[list, np.ndarray, sparse.spmatrix],
                 c: Union[None, list, np.ndarray, sparse.spmatrix],
                 U: Polytope):
        """
        Create a linear control system as a difference equation of the form:
        
        x_{k+1} = A x_k + B u_k + c, if u_k in U
        
        :param A: 
        :param B: 
        :param c: 
        :param U:
        """
        A = sparse.coo_matrix(A, shape=(dimension, dimension))
        B = sparse.coo_matrix(B, shape=(dimension, num_of_inputs))
        if c is None:
            c = sparse.coo_matrix(np.zeros((dimension, 1)))
        else:
            c = sparse.coo_matrix(np.reshape(c, (dimension, 1)))
        assert isinstance(U, Polytope)
        self.n = dimension
        self.m = num_of_inputs
        self.A = A
        self.B = B
        self.c = c
        self.U = U

    def get_dimension(self) -> int:
        return self.n

    def get_num_of_inputs(self) -> int:
        return self.m

    def get_A(self):
        return self.A

    def get_B(self):
        return self.B

    def get_c(self):
        return self.c

    def get_U(self):
        return self.U

    def sim(self, x0: np.ndarray, u: np.ndarray):
        assert x0.ndim == 1 and x0.size == self.n
        if (self.m == 1 and u.ndim == 0) or (u.ndim == 1 and u.size == self.m):
            u = np.reshape(u, (self.m, 1))
        elif u.ndim != 2 or u.shape[0] != self.m:
            raise IndexError
        K = u.shape[1]
        assert np.all([u[:, k] in self.U for k in range(K)])
        x = np.zeros((self.n, K + 1))
        x[:, 0] = x0
        for k in range(K):
            x[:, k + 1] = self.A.toarray() @ x[:, k] + self.B.toarray() @ u[:, k] + self.c.toarray().flatten()
        return x

    def is_reachable(self, X_0: Polytope, X_target: Polytope) -> bool:
        """
        Solves the following linear programming problem:

        max s

        subject to

        x1 = A x0 + B u0 + c + v,
        x0 in X_0, u0 in U, x1 in X_target,
        s <= v, -s <= v


        :param X_target:
        :param X_0:
        :return True if and only if X_target is reachable from a state x0 in X_0:
        """
        assert X_0.get_dimension() == self.get_dimension()
        assert X_target.get_dimension() == self.get_dimension()
        U = self.get_U()
        A = self.get_A()
        B = self.get_B()
        c = self.get_c()
        n = self.get_dimension()
        m = self.get_num_of_inputs()
        lb = np.hstack((X_0.get_lb(), U.get_lb(), X_target.get_lb()))
        ub = np.hstack((X_0.get_ub(), U.get_ub(), X_target.get_ub()))
        model = LPModel(num_of_vars=lb.size, lb=lb, ub=ub)
        model.add_equality_from_sparse(
            equality_idx=model.num_of_equalities,
            lhs=sparse.hstack((-A, -B, np.identity(n)), format='coo'),
            rhs=c
        )
        if X_0.get_A_ub() is not None:
            A_ub = X_0.get_A_ub()
            b_ub = X_0.get_b_ub()
            model.add_inequality(inequality_idx=model.num_of_inequalities,
                                 lhs_V=A_ub.data, lhs_I=A_ub.row, lhs_J=A_ub.col,
                                 rhs_V=b_ub.data, rhs_I=b_ub.row)
        if X_0.get_A_eq() is not None:
            A_eq = X_0.get_A_eq()
            b_eq = X_0.get_b_eq()
            model.add_equality(equality_idx=model.num_of_equalities,
                               lhs_V=A_eq.data, lhs_I=A_eq.row, lhs_J=A_eq.col,
                               rhs_V=b_eq.data, rhs_I=b_eq.row)
        if U.get_A_ub() is not None:
            A_ub = U.get_A_ub()
            b_ub = U.get_b_ub()
            model.add_inequality(inequality_idx=model.num_of_inequalities,
                                 lhs_V=A_ub.data, lhs_I=A_ub.row, lhs_J=A_ub.col + n,
                                 rhs_V=b_ub.data, rhs_I=b_ub.row)
        if U.get_A_eq() is not None:
            A_eq = U.get_A_eq()
            b_eq = U.get_b_eq()
            model.add_equality(equality_idx=model.num_of_equalities,
                               lhs_V=A_eq.data, lhs_I=A_eq.row, lhs_J=A_eq.col + n,
                               rhs_V=b_eq.data, rhs_I=b_eq.row)
        if X_target.get_A_ub() is not None:
            A_ub = X_target.get_A_ub()
            b_ub = X_target.get_b_ub()
            model.add_inequality(inequality_idx=model.num_of_inequalities,
                                 lhs_V=A_ub.data, lhs_I=A_ub.row, lhs_J=A_ub.col + n + m,
                                 rhs_V=b_ub.data, rhs_I=b_ub.row)
        if X_target.get_A_eq() is not None:
            A_eq = X_target.get_A_eq()
            b_eq = X_target.get_b_eq()
            model.add_equality(equality_idx=model.num_of_equalities,
                               lhs_V=A_eq.data, lhs_I=A_eq.row, lhs_J=A_eq.col + n + m,
                               rhs_V=b_eq.data, rhs_I=b_eq.row)
        model.solve()
        return model.is_optimal
