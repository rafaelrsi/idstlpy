import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import networkx as nx


def plot_polytope(polytope, axis: list = None, **kwargs):
    if axis is None:
        axis = [0, 1]
    from idstl.Polytope import Polytope
    assert isinstance(polytope, Polytope)
    assert len(axis) == 2
    assert polytope.is_full_dimensional()
    v = polytope.get_vertices(axis)
    return plt.fill(v[:, 0], v[:, 1], **kwargs)


def plot_partition(partition, axis: list = None, **kwargs):
    from idstl.Partition import Partition
    if axis is None:
        axis = [0, 1]
    assert isinstance(partition, Partition)
    assert len(axis) == 2
    h = plot_polytope(partition.get_polytope(), axis, **kwargs)
    h = h[0]
    centroid = partition.get_polytope().get_centroid(axis)
    plt.text(centroid[0], centroid[1], '$\mathcal{P}_{' + str(partition.get_idx()) + '}$', ha='center', va='center')
    return h


def plot_transition_system(transition_system, axis: list = None,
                           list_of_colors: list = None):
    from idstl.TransitionSystem import TransitionSystem
    assert isinstance(transition_system, TransitionSystem)
    if axis is None:
        axis = [0, 1]
    if list_of_colors is None:
        list_of_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
                          'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    map_label_color = np.zeros((0, 2), dtype=np.object)
    list_of_handlers = []
    for partition in transition_system.get_partitions():
        labels_ind = partition.get_label_ind()
        if len(labels_ind) == 0:
            plot_partition(partition, axis, color=list_of_colors[0], alpha=0.75)
        else:
            key = ','.join(np.sort(labels_ind).astype(str))
            idx = np.searchsorted(map_label_color[:, 0], key, 'right') - 1
            if 0 <= idx < map_label_color.shape[0] and map_label_color[idx, 0] == key:
                color_idx = map_label_color[idx, 1]
                plot_partition(partition, axis, color=list_of_colors[color_idx], alpha=0.75)
            else:
                color_idx = (map_label_color.shape[0] + 1) % len(list_of_colors)
                map_label_color = np.insert(map_label_color, idx + 1, [key, color_idx], axis=0)
                h = plot_partition(partition, axis, color=list_of_colors[color_idx], alpha=0.75,
                                   label='{' + ', '.join(partition.get_labels()) + '}')
                list_of_handlers.append(h)
    plt.legend(handles=list_of_handlers)
    plt.gca().set_aspect('equal', 'box')
    return plt


def draw_transition_system(transition_system, list_of_colors: list = None):
    from idstl.TransitionSystem import TransitionSystem
    from idstl.Transition import Transition
    assert isinstance(transition_system, TransitionSystem)
    if list_of_colors is None:
        list_of_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
                          'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    map_label_color = np.zeros((0, 2), dtype=np.object)
    list_of_handlers = [Line2D([0], [0], marker='o', color='w',
                               label='{ }', markerfacecolor=list_of_colors[0], markersize=15)]
    G = nx.DiGraph()
    node_names = {}
    node_colors = np.zeros(len(transition_system), dtype='O')
    for partition in transition_system.get_partitions():
        partition_idx = partition.get_idx()
        node_names[partition_idx] = '$\mathcal{P}_{' + str(partition_idx) + '}$'
        G.add_node(partition_idx)
        labels_ind = partition.get_label_ind()
        if labels_ind.size == 0:
            color_idx = 0
        else:
            key = ','.join(np.sort(labels_ind).astype(str))
            idx = np.searchsorted(map_label_color[:, 0], key, 'right') - 1
            if 0 <= idx < map_label_color.shape[0] and map_label_color[idx, 0] == key:
                color_idx = map_label_color[idx, 1]
            else:
                color_idx = (map_label_color.shape[0] + 1) % len(list_of_colors)
                map_label_color = np.insert(map_label_color, idx + 1, [key, color_idx], axis=0)
                list_of_handlers.append(Line2D([0], [0], marker='o', color='w',
                                               label='{' + ', '.join(partition.get_labels()) + '}',
                                               markerfacecolor=list_of_colors[color_idx], markersize=15))
        node_colors[partition_idx] = list_of_colors[color_idx]
    edges = []
    symbols = {}
    for transition in transition_system.get_array_of_transitions():
        assert isinstance(transition, Transition)
        edges.append((transition.get_from_idx(), transition.get_to_idx()))
        symbols[edges[-1]] = transition.get_symbol_idx()
    G.add_edges_from(edges)
    pos = nx.spring_layout(G)
    nx.draw_networkx_nodes(G, pos, node_color=node_colors)
    nx.draw_networkx_labels(G, pos, node_names)
    nx.draw_networkx_edges(G, pos, edge_color='black')
    nx.draw_networkx_edge_labels(G, pos, edge_labels=symbols)
    plt.axis("off")
    plt.legend(handles=list_of_handlers)
    return plt


def plot_tunnel(tunnel, axis: list = None, list_of_colors=None, title=None):
    from idstl.Tunnel import Tunnel
    assert isinstance(tunnel, Tunnel)
    if axis is None:
        axis = [0, 1]
    if list_of_colors is None:
        list_of_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
                          'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    if title is not None:
        plt.title(title)
    plot_transition_system(tunnel.ts, axis, list_of_colors)
    seq = np.unique(tunnel.seq_of_partition_idx)
    for idx in seq:
        plot_partition(tunnel.ts.get_partition_from_idx(idx), alpha=0, hatch='/')
    return plt


def plot_trajectory_state(tunnel, axis: list = None, title=None):
    from idstl.Tunnel import Tunnel
    assert isinstance(tunnel, Tunnel)
    if axis is None:
        axis = [0, 1]
    if len(axis) != 2:
        raise NotImplementedError
    distance, x, u = tunnel.get_trajectory()
    if title is not None:
        plt.title(title)
    plot_transition_system(tunnel.ts)
    plt.plot(x[:, 0], x[:, 1], 'r*-')
    return plt


def plot_trajectory_time(tunnel, axis: list = None, title=None):
    from idstl.Tunnel import Tunnel
    assert isinstance(tunnel, Tunnel)
    if axis is None:
        axis = [0, 1]
    fig, axs = plt.subplots(len(axis))
    distance, x, u = tunnel.get_trajectory()
    if title is not None:
        fig.suptitle(title)
    for i in range(len(axis)):
        axs[i].plot(x[:, axis[i]], 'r*-')
    return fig
