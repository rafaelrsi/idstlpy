import numpy as np


def true(ctx=None):
    return Formula(True, ctx)


def false(ctx=None):
    return Formula(False, ctx)


class Formula:
    def __init__(self, value: bool = None, ctx=None):
        from idstl.Context import Context
        assert value is None or (isinstance(value, int) and (value == 0 or value == 1)) or isinstance(value, bool)
        assert ctx is None or isinstance(ctx, Context)
        self.__ctx = ctx
        self.value = value
        self.idx = None
        self.a = 0
        self.idx = Context.add_formula(self, ctx)

    def __len__(self):
        from idstl.Context import Context
        return Context.get_num_of_formulas(self.get_context())

    def __and__(self, other):
        return And(self, other)

    def __or__(self, other):
        return Or(self, other)

    def __str__(self):
        return str(self.get_value())

    def __lt__(self, other):
        assert isinstance(other, Formula)
        return self.get_idx() < other.get_idx()

    def __gt__(self, other):
        assert isinstance(other, Formula)
        return self.get_idx() > other.get_idx()

    def __eq__(self, other):
        assert isinstance(other, Formula)
        return self.get_idx() == other.get_idx()

    def get_idx(self):
        return self.idx

    def get_context(self):
        return self.__ctx

    def get_num_of_formulas(self) -> int:
        from idstl.Context import Context
        return Context.get_num_of_formulas(self.get_context())

    def get_subformula(self, idx: int):
        from idstl.Context import Context
        return Context.get_formula(idx, self.get_context())

    def get_upper_time_bound(self) -> int:
        return 0

    def get_closure(self) -> np.ndarray:
        return np.array([self])

    def is_true(self):
        return self.get_value() is not None and self.get_value()

    def is_false(self):
        return self.get_value() is not None and not self.get_value()

    def until(self, *args):
        """
        Until temporal operator.

        ----------

        Syntax:

        * phi_1.until(phi_2): for unbounded temporal operator
        * phi_1.until(a, b, phi_2): for bounded temporal operator

        __________

        Semantics:


        Therefore:

        * Segment(path, k, inf) satisfies phi_1.until(phi_2) iff (a) there exists k^p in [k,inf] such that 
        Segment(path, k^p, inf) satisfies phi_2 and (b), for all k^pp in [k..k^p], Segment(path, k^pp, inf) 
        satisfies phi_1

        * Segment(path, k, inf) satisfies phi_1.until(a, b, phi_2) iff there exists k^p in [k + a..k + b] such that 
        (path, k^p, inf) satisfies phi_2 and, for all k^pp in [k..k^p], Segment(path, k^pp, inf) satisfies phi_1.

        where:

        * Path = s_0, s_1, ...: is a sequence of states of a transition system,
        * Segment(Path, a, b) = s_a, s_{a+1},...,s_{b-1},s_b: is a segment of a sequence defined by the path Path, and
        * [a..b] = {a,a+1,...,b-1,b}: is an interval of discrete instants.

        :param args:
        :return: Formula
        """
        from idstl.Context import MAX_INT
        assert len(args) == 3 or (len(args) == 1 and isinstance(args[0], Formula))
        assert len(args) == 1 or (len(args) == 3 and
                                  isinstance(args[0], int) and args[0] >= 0 and
                                  isinstance(args[1], int) and args[1] >= args[0] and
                                  isinstance(args[2], Formula))
        if len(args) == 1:
            a = 0
            b = MAX_INT
            phi = args[0]
        else:
            a = args[0]
            b = args[1]
            phi = args[2]
        return Until(self, a, b, phi, self.get_context())

    def release(self, *args):
        """
        Release temporal operator.

        ----------

        Syntax:

        * phi_1.release(phi_2): for unbounded temporal operator
        * phi_1.release(a, b, phi_2): for bounded temporal operator

        __________

        Semantics:


        Therefore:

        * Segment(path, k, inf) satisfies phi_1.release(phi_2) iff: i) (a) there exists k^p in [k, inf] such that 
        Segment(path, k^p, inf) satisfies phi_1 and (b), for all k^pp in [k..k^p], Segment(path, k^pp, inf) satisfies 
        phi_2; or ii) Segment(path, k^pp, inf) satisfies phi_2, for kpp = k,k+1,...

        * Segment(path, k, inf) satisfies phi_1.release(a, b, phi_2) iff: i) (a) there exists k^p in [k, k + b] 
        such that Segment(path, k^p, inf) satisfies phi_1 and (b), for all k^pp in [k + a..k^p], 
        Segment(path, k^pp, inf) satisfies phi_2; or ii) Segment(path, k^pp, inf) satisfies phi_2, for all kpp 
        in [k + a..k + b]

        where:

        * Path = s_0, s_1, ...: is a sequence of states of a transition system,
        * Segment(Path, a, b) = s_a, s_{a+1},...,s_{b-1},s_b: is a segment of a sequence defined by the path Path, and
        * [a..b] = {a,a+1,...,b-1,b}: is an interval of discrete instants.

        :param args:
        :return: Formula
        """
        from idstl.Context import MAX_INT
        assert len(args) == 3 or (len(args) == 1 and isinstance(args[0], Formula))
        assert len(args) == 1 or (len(args) == 3 and
                                  isinstance(args[0], int) and args[0] >= 0 and
                                  isinstance(args[1], int) and args[1] >= args[0] and
                                  isinstance(args[2], Formula))
        if len(args) == 1:
            a = 0
            b = MAX_INT
            phi = args[0]
        else:
            a = args[0]
            b = args[1]
            phi = args[2]
        return Release(self, a, b, phi, self.get_context())

    def eventually(self, *args):
        """
        Eventually temporal operator.

        ----------

        Syntax:

        * phi.eventually(): for unbounded temporal operator
        * phi.eventually(a, b): for bounded temporal operator

        __________

        Semantics:


        Therefore:

        * Segment(path, k, inf) satisfies phi.eventually() iff there exists k^p in [k,inf] such that 
        Segment(path, k^p, inf) satisfies phi.

        * Segment(path, k, inf) satisfies phi.eventually(a, b) iff there exists k^p in [k + a..k + b] such that 
        Segment(path, k^p, inf) satisfies phi.

        where:

        * Path = s_0, s_1, ...: is a sequence of states of a transition system,
        * Segment(Path, a, b) = s_a, s_{a+1},...,s_{b-1},s_b: is a segment of a sequence defined by the path Path, and
        * [a..b] = {a,a+1,...,b-1,b}: is an interval of discrete instants.

        :param args:
        :return: Formula
        """
        from idstl.Context import MAX_INT
        assert len(args) == 0 or (len(args) == 2 and
                                  isinstance(args[0], int) and args[0] >= 0 and
                                  isinstance(args[1], int) and args[1] >= args[1])
        if len(args) == 0:
            a = 0
            b = MAX_INT
        else:
            a = args[0]
            b = args[1]
        return Until(true(), a, b, self, self.get_context())

    def always(self, *args):
        """
        Always temporal operator.

        ----------

        Syntax:

        * phi.always(): for unbounded temporal operator
        * phi.always(a, b): for bounded temporal operator

        __________

        Semantics:


        Therefore:

        * Segment(path, k, inf) satisfies phi.always() iff Segment(path, k^pp, inf) satisfies phi, for kpp = k,k+1,...

        * Segment(path, k, inf) satisfies phi.always(a, b) iff Segment(path, k^pp, inf) satisfies phi, for all kpp 
        in [k + a..k + b]

        where:

        * Path = s_0, s_1, ...: is a sequence of states of a transition system,
        * Segment(Path, a, b) = s_a, s_{a+1},...,s_{b-1},s_b: is a segment of a sequence defined by the path Path, and
        * [a..b] = {a,a+1,...,b-1,b}: is an interval of discrete instants.

        :param args:
        :return: Formula
        """
        from idstl.Context import MAX_INT
        assert len(args) == 0 or (len(args) == 2 and
                                  isinstance(args[0], int) and args[0] >= 0 and
                                  isinstance(args[1], int) and args[1] >= args[1])
        if len(args) == 0:
            a = 0
            b = MAX_INT
        else:
            a = args[0]
            b = args[1]
        return Release(false(), a, b, self, self.get_context())

    def get_value(self):
        return self.value


class AtomicProposition(Formula):
    def __init__(self, label: str, label_idx: int, ctx=None):
        assert isinstance(label, str), "The label must be a string"
        assert isinstance(label_idx, int), "The label_idx must be an integer value"
        self.label = label
        self.__label_idx = label_idx
        Formula.__init__(self, ctx=ctx)

    def __str__(self):
        return self.label

    def __invert__(self):
        return Not(self)

    def get_label_idx(self):
        return self.__label_idx

    def get_upper_time_bound(self) -> int:
        return 0

    def get_closure(self) -> np.ndarray:
        return np.array([self])


class Not(Formula):
    def __init__(self, atomic_proposition: AtomicProposition, ctx=None):
        assert isinstance(atomic_proposition, AtomicProposition), \
            "Not(atomic_proposition), where atomic_proposition is an Atomic Proposition"
        self.child = atomic_proposition
        Formula.__init__(self, ctx=ctx)

    def __str__(self):
        return 'Not(' + str(self.child) + ')'

    def get_label_idx(self):
        return self.child.get_label_idx()

    def get_child(self):
        return self.child

    def get_upper_time_bound(self) -> int:
        return 0

    def get_closure(self) -> np.ndarray:
        return np.hstack((self, self.get_child().get_closure()))


class And(Formula):
    def __init__(self, *args, ctx=None):
        """
        Usage:

        And(phi_1, ..., phi_N), where phi_i is a formula and N >= 2

        :param args:
        """
        assert len(args) >= 2 and all([isinstance(a, Formula) for a in args])
        self.children = []
        for child in args:
            if isinstance(child, And):
                for grandchild in child.children:
                    self.children.append(grandchild)
            else:
                self.children.append(child)
        self.children = np.array(self.children)
        Formula.__init__(self, ctx=ctx)

    def __str__(self):
        my_str = '(' + str(self.get_child(0)) + ' & ' + str(self.get_child(1))
        for i in range(2, self.get_num_of_children()):
            my_str += ' & ' + str(self.get_child(i))
        my_str += ')'
        return my_str

    def get_child(self, idx: int):
        return self.children[idx]

    def get_num_of_children(self) -> int:
        return self.children.size

    def get_upper_time_bound(self) -> int:
        return np.max([self.get_child(i).get_upper_time_bound() for i in range(self.get_num_of_children())])

    def get_closure(self) -> np.ndarray:
        return np.hstack((self,
                          np.hstack([self.get_child(i).get_closure() for i in range(self.get_num_of_children())])))


class Or(Formula):
    def __init__(self, *args, ctx=None):
        assert len(args) >= 2 and all([isinstance(a, Formula) for a in args]), \
            "Or(phi_1, ..., phi_N), where phi_i is a formula and N >= 2"
        self.children = []
        for child in args:
            if isinstance(child, Or):
                for grandchild in child.children:
                    self.children.append(grandchild)
            else:
                self.children.append(child)
        self.children = np.array(self.children)
        Formula.__init__(self, ctx=ctx)

    def __str__(self):
        my_str = '(' + str(self.children[0]) + ' & ' + str(self.children[1])
        for i in range(2, len(self.children)):
            my_str += ' | ' + str(self.children[i])
        my_str += ')'
        return my_str

    def get_child(self, idx: int):
        return self.children[idx]

    def get_num_of_children(self) -> int:
        return self.children.size

    def get_upper_time_bound(self) -> int:
        return np.max([self.get_child(i).get_upper_time_bound() for i in range(self.get_num_of_children())])

    def get_closure(self) -> np.ndarray:
        return np.hstack((self,
                          np.hstack([self.get_child(i).get_closure() for i in range(self.get_num_of_children())])))


class Until(Formula):
    def __init__(self, phi_1: Formula, a: int, b: int, phi_2: Formula, ctx=None):
        assert isinstance(a, int) and a >= 0 and isinstance(b, int) and b >= a \
               and isinstance(phi_1, Formula) and isinstance(phi_2, Formula)
        self.child_1 = phi_1
        self.a = a
        self.b = b
        self.child_2 = phi_2
        Formula.__init__(self, ctx=ctx)

    def __str__(self):
        from idstl.Context import MAX_INT
        if self.get_phi_1().is_true():
            my_str = '(E'
        else:
            my_str = '(' + str(self.get_phi_1()) + ' U'
        a, b = self.get_interval()
        if a > 0 or b < MAX_INT:
            my_str += '[' + str(a) + '..' + str(b) + ']'
        my_str += ' ' + str(self.get_phi_2()) + ')'
        return my_str

    def get_phi_1(self):
        return self.child_1

    def get_phi_2(self):
        return self.child_2

    def get_interval(self):
        return self.a, self.b

    def get_upper_time_bound(self) -> int:
        a, b = self.get_interval()
        return max(b + self.get_phi_2().get_upper_time_bound(), self.get_phi_1().get_upper_time_bound())

    def get_closure(self) -> np.ndarray:
        return np.hstack((self, self.get_phi_1().get_closure(), self.get_phi_2().get_closure()))


class Release(Formula):
    def __init__(self, phi_1, a, b, phi_2, ctx=None):
        assert isinstance(a, int) and a >= 0 and isinstance(b, int) and b >= a \
               and isinstance(phi_1, Formula) and isinstance(phi_2, Formula)
        self.child_1 = phi_1
        self.a = a
        self.b = b
        self.child_2 = phi_2
        Formula.__init__(self, ctx=ctx)

    def __str__(self):
        from idstl.Context import MAX_INT
        if self.get_phi_1().is_false():
            my_str = '(A'
        else:
            my_str = '(' + str(self.get_phi_1()) + ' U'
        a, b = self.get_interval()
        if a > 0 or b < MAX_INT:
            my_str += '[' + str(a) + '..' + str(b) + ']'
        my_str += ' ' + str(self.get_phi_2()) + ')'
        return my_str

    def get_phi_1(self):
        return self.child_1

    def get_phi_2(self):
        return self.child_2

    def get_interval(self):
        return self.a, self.b

    def get_upper_time_bound(self) -> int:
        a, b = self.get_interval()
        return max(b + self.get_phi_2().get_upper_time_bound(), self.get_phi_1().get_upper_time_bound())

    def get_closure(self) -> np.ndarray:
        return np.hstack((self, self.get_phi_1().get_closure(), self.get_phi_2().get_closure()))
