import numpy as np
from idstl.Tunnel import Tunnel


def best_first_search(root: Tunnel) -> (Tunnel, list):
    from idstl.Context import gstuffs, is_verbose, is_plot
    max_zero_prefix_step = int(root.zero_prefix_step)
    if root.is_infeasible():
        if is_verbose():
            print('tunnel ' + str(root) + ' is infeasible')
        if is_plot():
            ax = gstuffs.plot_tunnel(root)
            ax.title('infeasible root '
                     + str(list(root.tunnel_model.num_of_self_loops)))
            ax.show()
        return max_zero_prefix_step
    if root.is_feasible():
        if is_verbose():
            print('tunnel ' + str(root) + ' is feasible')
            print('root ' + str(list(root.tunnel_model.num_of_self_loops)) + ' is feasible')
        if is_plot():
            ax = gstuffs.plot_trajectory_state(root)
            ax.title('sol =  ' + str(list(root.tunnel_model.num_of_self_loops)))
            ax.show()
        return root
    open_set = np.array([root])
    while len(open_set) > 0:
        parent = open_set[0]
        assert isinstance(parent, Tunnel)
        open_set = np.delete(open_set, 0)
        if is_plot():
            ax = gstuffs.plot_trajectory_state(parent)
            ax.title('parent ' + str(list(parent.tunnel_model.num_of_self_loops))
                     + ' zero_prefix_step = ' + str(parent.zero_prefix_step)
                     + ' distance = ' + str(np.round(parent.distance, 3)))
            ax.show()
        for step in range(parent.child_repeats_idx, parent.zero_prefix_step + 1):
            child = parent.repeat_at(step)
            assert isinstance(child, Tunnel)
            if child.zero_prefix_step > max_zero_prefix_step:
                max_zero_prefix_step = int(child.zero_prefix_step)
            if child.is_infeasible():
                if is_verbose():
                    print('child ' + str(list(child.tunnel_model.num_of_self_loops)) + ' is infeasible')
                if is_plot():
                    ax = gstuffs.plot_tunnel(child)
                    ax.title('infeasible child[' + str(step) + '] '
                             + str(list(child.tunnel_model.num_of_self_loops)))
                    ax.show()
            elif child.is_feasible():
                if is_verbose():
                    print('tunnel ' + str(root) + ' is feasible')
                    print('child ' + str(list(child.tunnel_model.num_of_self_loops)) + ' is feasible')
                if is_plot():
                    ax = gstuffs.plot_trajectory_state(child)
                    ax.title('sol = child[' + str(step) + '] ' + str(list(child.tunnel_model.num_of_self_loops)))
                    ax.show()
                return child
            else:
                if is_verbose():
                    print('child ' + str(list(child.tunnel_model.num_of_self_loops)) + ' is added to open set')
                if is_plot():
                    ax = gstuffs.plot_trajectory_state(child)
                    ax.title('child[' + str(step) + '] ' + str(list(child.tunnel_model.num_of_self_loops))
                             + ' zero_prefix_step = ' + str(child.zero_prefix_step)
                             + ' distance = ' + str(np.round(child.distance, 3)))
                    ax.show()
                # noinspection PyTypeChecker
                idx = np.searchsorted(open_set, child, 'right')
                # noinspection PyTypeChecker
                open_set = np.insert(open_set, idx, child)
            if child.zero_prefix_step == step:
                break
    return max_zero_prefix_step
