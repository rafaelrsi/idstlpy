from typing import Union
import numpy as np
# from idstl.Formula import AtomicProposition
from idstl.Partition import Partition, Polytope


class TransitionSystem:
    def __init__(self,
                 initial_state: Union[list, np.ndarray],
                 initial_partition: Partition = None,
                 accepting_partitions: Union[set, list, np.ndarray] = None,
                 ctx=None):
        from idstl.Context import Context
        assert ctx is None or isinstance(ctx, Context)
        self.__ctx = ctx
        assert Context.get_num_of_transitions(self.get_context()) > 0
        initial_state = np.array(initial_state).flatten()
        assert initial_state.size == Context.get_dimension(self.get_context())
        if initial_partition is None:
            buff = [i for i in range(Context.get_num_of_partitions(self.get_context()))
                    if initial_state in Context.get_partition(i, self.get_context())]
            assert len(buff) >= 1
            initial_partition = Context.get_partition(buff[0])
        else:
            assert initial_partition in Context.get_partitions(
                self.get_context()) and initial_state in initial_partition
        if accepting_partitions is not None:
            accepting_partitions = np.array(accepting_partitions).flatten()
            assert set(accepting_partitions).issubset(set(Context.get_partitions(self.get_context())))
        else:
            accepting_partitions = Context.get_partitions(self.get_context())
        self.__bit_len = int(Context.get_num_of_partitions(self.get_context())).bit_length()
        self.__accepting_partitions = np.array([partition.get_idx() for partition in accepting_partitions], dtype=int)
        self.__initial_partition = initial_partition.get_idx()
        if self.get_num_of_symbols() == 1:
            self.__discrete_transitions = np.array([
                [Context.get_transition(idx, self.__ctx).get_from_idx(),
                 Context.get_transition(idx, self.__ctx).get_to_idx()]
                for idx in range(Context.get_num_of_transitions(self.__ctx))
            ])
            is_self_loop = self.__discrete_transitions[:, 0] == self.__discrete_transitions[:, 1]
        else:
            self.__discrete_transitions = np.array([
                [Context.get_transition(idx, self.__ctx).get_from_idx(),
                 Context.get_transition(idx, self.__ctx).get_symbol_idx(),
                 Context.get_transition(idx, self.__ctx).get_to_idx()]
                for idx in range(Context.get_num_of_transitions(self.__ctx))
            ])
            e_idx = Context.get_symbol_idx('')
            is_self_loop = np.logical_and(self.__discrete_transitions[:, 0] == self.__discrete_transitions[:, 2],
                                          self.__discrete_transitions[:, 1] == e_idx)
        self.__discrete_transitions = self.__discrete_transitions[np.logical_not(is_self_loop), :].astype(int)

    def __len__(self):
        return self.get_num_of_partitions()

    def sim(self, a: np.ndarray, u: np.ndarray):
        from idstl.Transition import Transition
        assert a.ndim == 1
        K = a.size
        assert u.ndim == 2 and u.shape[0] == self.m and u.shape[1] == K
        q = np.zeros(K + 1, dtype=int)
        x = np.zeros((self.n, K + 1))
        q[0] = self.get_initial_partition_idx()
        x[:, 0] = self.get_initial_state()
        transitions = self.get_array_of_transitions()
        for k in range(K):
            if a[k] == 0:
                t_idx = next(i for i in range(transitions.size)
                             if transitions[i].get_from_idx() == q[k]
                             and transitions[i].get_symbol_idx() == a[k]
                             and transitions[i].get_to_idx() == q[k])
                transition = transitions[t_idx]
                x_t = transition.sim(x[:, k], u[:, k])
                if x_t is not None:
                    q[k + 1] = q[k]
                    x[:, k + 1] = x_t[:, 1]
                else:
                    t_ind = np.array([i for i in range(transitions.size)
                                      if transitions[i].get_from_idx() == q[k]
                                      and transitions[i].get_symbol_idx() == a[k]
                                      and transitions[i].get_to_idx() != q[k]])
                    assert t_ind.size > 0
                    i = 0
                    x_t = transitions[t_ind[i]].sim(x[:, k], u[:, k])
                    while x_t is None and i + 1 < t_ind.size:
                        i += 1
                        x_t = transitions[t_ind[i]].sim(x[:, k], u[:, k])
                    assert x_t is not None
                    q[k + 1] = transitions[t_ind[i]].get_to_idx()
                    x[:, k + 1] = x_t[:, 1]
            else:
                num_of_valid_transitions = np.sum([
                    1 for i in range(transitions.size)
                    if transitions[i].get_from_idx() == q[k] and transitions[i].get_symbol_idx() == a[k]
                ])
                assert num_of_valid_transitions == 1
                t_idx = next(i for i in range(transitions.size)
                             if transitions[i].get_from_idx() == q[k] and transitions[i].get_symbol_idx() == a[k])
                transition = transitions[t_idx]
                assert isinstance(transition, Transition)
                assert transition.get_from_idx() == q[k] and transition.get_symbol_idx() == a[k]
                q[k + 1] = transition.get_to_idx()
                x_t = transition.sim(x[:, k], u[:, k])
                assert x_t is not None
                x[:, k + 1] = x_t[:, 1]
        return q, x

    @property
    def m(self):
        return self.get_num_of_inputs()

    @property
    def n(self):
        return self.get_dimension()

    def get_context(self):
        return self.__ctx

    def get_num_of_partitions(self):
        from idstl.Context import Context
        return Context.get_num_of_partitions(self.get_context())

    def bit_length(self):
        return self.__bit_len

    def get_dimension(self) -> int:
        from idstl.Context import Context
        return Context.get_dimension(self.get_context())

    def get_num_of_inputs(self) -> int:
        from idstl.Context import Context
        return Context.get_num_of_inputs(self.get_context())

    def get_initial_state(self) -> np.ndarray:
        from idstl.Context import Context
        return Context.get_initial_state(self.get_context())

    def get_initial_partition(self) -> Partition:
        from idstl.Context import Context
        return Context.get_partition(self.get_initial_partition_idx(), self.get_context())

    def get_initial_partition_idx(self) -> int:
        return self.__initial_partition

    def get_array_of_transitions(self) -> np.ndarray:
        from idstl.Context import Context
        return Context.get_transitions(self.get_context())

    def get_array_of_discrete_transitions(self) -> np.ndarray:
        return self.__discrete_transitions

    def get_transition_from_idx(self, partition_from_idx: int, symbol_idx: int, partition_to_idx: int):
        from idstl.Context import Context
        idx = Context.get_transition_idx((partition_from_idx, symbol_idx, partition_to_idx), self.get_context())
        if idx is not None:
            return Context.get_transition(idx, self.get_context())
        if partition_from_idx == partition_to_idx and symbol_idx == 0:
            raise NotImplementedError
        return None

    def is_there_transition_from_idx(self, partition_from_idx: int, symbol_idx: int, partition_to_idx: int) -> bool:
        return self.get_transition_from_idx(partition_from_idx, symbol_idx, partition_to_idx) is not None

    def get_partitions(self) -> np.ndarray:
        from idstl.Context import Context
        return Context.get_partitions(self.get_context())

    def get_partition_from_idx(self, idx: int) -> Partition:
        from idstl.Context import Context
        return Context.get_partition(idx, self.get_context())

    def get_polytope_from_idx(self, idx: int) -> Polytope:
        return self.get_partition_from_idx(idx).get_polytope()

    def get_num_of_symbols(self) -> int:
        from idstl.Context import Context
        return Context.get_num_of_symbols(self.get_context())

    def get_symbol(self, idx: int) -> str:
        from idstl.Context import Context
        return Context.get_symbol(idx, self.get_context())

    def get_symbol_idx(self, symbol: str) -> int:
        from idstl.Context import Context
        idx = Context.get_symbol_idx(symbol, self.get_context())
        assert idx is not None, 'symbol not found!'
        return idx

    def get_accepting_partition_ind(self):
        return self.__accepting_partitions

    def get_num_of_labels(self) -> int:
        from idstl.Context import Context
        return Context.get_num_of_labels(self.get_context())

    def get_label(self, idx: int) -> str:
        from idstl.Context import Context
        return Context.get_label(idx, self.get_context())

    # def get_label_idx(self, label: str) -> int:
    #     from idstl.Context import Context
    #     idx = Context.get_label_idx(label, self.get_context())
    #     assert idx is not None, 'label not found!'
    #     return idx

    # def get_atomic_proposition(self, label) -> AtomicProposition:
    #     return AtomicProposition(label, label_idx=self.get_label_idx(label))

    def labeling_function(self, partition_idx: int) -> np.ndarray:
        from idstl.Context import Context
        return Context.labelling_function(partition_idx, self.get_context())

    def inv_labeling_function(self, label_idx: int) -> np.ndarray:
        from idstl.Context import Context
        return Context.inv_labelling_function(label_idx, self.get_context())
