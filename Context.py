from typing import Union, Iterable
import sys
import numpy as np
from scipy import sparse
from idstl.Polytope import Polytope
from idstl.System import System
from idstl.Formula import AtomicProposition
import idstl.GraphicStuffs as gstuffs
import time

TOLERANCE = 1e-6
MAX_INT = 2 ** sys.int_info.bits_per_digit


class Context:
    __stat_lp = 0
    __stat_bmc = 0
    __dimension = None
    __num_of_inputs = None
    __silent = False
    __plot = False
    __ts = None
    __phi = None
    __x0 = None
    __P0 = None
    __solution = None
    __is_sat = False
    __is_unsat = False
    __accepting_partitions = None
    __map_idx_formulas = np.array([], dtype=np.object)
    __map_idx_atomic_propositions = np.array([], dtype=np.object)
    __map_idx_partitions = np.array([], dtype=np.object)
    __map_idx_transition = np.array([], dtype=np.object)
    __map_transition_idx = np.zeros((0, 2), dtype=np.object)
    __map_idx_label = np.array([], dtype=np.object)
    __map_label_idx = np.zeros((0, 2), dtype=np.object)
    __tb_partition_label = np.zeros((0, 0), dtype=bool)
    __map_idx_symbol = np.array([], dtype=np.object)
    __map_symbol_idx = np.zeros((0, 2), dtype=np.object)
    __locked = False

    def __init__(self):
        raise NotImplementedError

    @staticmethod
    def clear(ctx: 'Context' = None):
        if ctx is None:
            Context.__locked = False
        else:
            raise NotImplementedError

    @staticmethod
    def lock(ctx: 'Context' = None):
        if ctx is None:
            Context.__locked = True
        else:
            raise NotImplementedError

    @staticmethod
    def get_num_of_partitions(ctx: 'Context' = None):
        if ctx is None:
            num_of_partitions = int(Context.__map_idx_partitions.size)
        else:
            raise NotImplementedError
        return num_of_partitions

    @staticmethod
    def get_label(idx, ctx: 'Context' = None):
        if ctx is None:
            label = Context.__map_idx_label[idx]
        else:
            raise NotImplementedError
        return label

    @staticmethod
    def get_num_of_labels(ctx: 'Context' = None):
        if ctx is None:
            num_of_labels = int(Context.__map_idx_label.size)
        else:
            raise NotImplementedError
        return num_of_labels

    @staticmethod
    def get_symbol(idx, ctx: 'Context' = None):
        if ctx is None:
            symbol = Context.__map_idx_symbol[idx]
        else:
            raise NotImplementedError
        return symbol

    @staticmethod
    def get_symbol_idx(symbol, ctx: 'Context' = None):
        if ctx is None:
            symbol_idx = search_map_object_idx(Context.__map_symbol_idx, symbol)
        else:
            raise NotImplementedError
        return symbol_idx

    @staticmethod
    def get_partition(idx, ctx: 'Context' = None):
        if ctx is None:
            partition = Context.__map_idx_partitions[idx]
        else:
            raise NotImplementedError
        return partition

    @staticmethod
    def get_dimension(ctx: 'Context' = None):
        if ctx is None:
            dimension = int(Context.__dimension)
        else:
            raise NotImplementedError
        return dimension

    @staticmethod
    def get_num_of_inputs(ctx: 'Context' = None):
        if ctx is None:
            num_of_inputs = int(Context.__num_of_inputs)
        else:
            raise NotImplementedError
        return num_of_inputs

    @staticmethod
    def get_num_of_symbols(ctx: 'Context' = None):
        if ctx is None:
            num_of_symbols = int(Context.__map_idx_symbol.size)
        else:
            raise NotImplementedError
        return num_of_symbols

    @staticmethod
    def get_transition_system(ctx: 'Context' = None):
        from idstl.TransitionSystem import TransitionSystem
        if ctx is None:
            ts = Context.__ts
            assert isinstance(ts, TransitionSystem)
        else:
            raise NotImplementedError
        return ts

    @staticmethod
    def get_partitions(ctx: 'Context' = None):
        if ctx is None:
            partitions = Context.__map_idx_partitions
        else:
            raise NotImplementedError
        return partitions

    @staticmethod
    def get_transition(idx, ctx: 'Context' = None):
        if ctx is None:
            transition = Context.__map_idx_transition[idx]
        else:
            raise NotImplementedError
        return transition

    @staticmethod
    def get_transitions(ctx: 'Context' = None):
        if ctx is None:
            transitions = Context.__map_idx_transition
        else:
            raise NotImplementedError
        return transitions

    @staticmethod
    def get_transition_idx(transition, ctx: 'Context' = None):
        """

        :param transition: (from_partition_idx,symbol_idx,to_partition_idx) or Transition
        :param ctx:
        :return:
        """
        from idstl.Transition import Transition, TransitionTriplet
        assert isinstance(transition, (tuple, Transition))
        if ctx is None:
            if isinstance(transition, tuple):
                transition = TransitionTriplet(partition_from_idx=transition[0],
                                               symbol_idx=transition[1],
                                               partition_to_idx=transition[2])
            transition_idx = search_map_object_idx(Context.__map_transition_idx, transition)
        else:
            raise NotImplementedError
        return transition_idx

    @staticmethod
    def get_num_of_transitions(ctx: 'Context' = None):
        if ctx is None:
            num_of_transitions = int(Context.__map_idx_transition.size)
        else:
            raise NotImplementedError
        return num_of_transitions

    @staticmethod
    def get_num_of_formulas(ctx: 'Context' = None):
        if ctx is None:
            num_of_formulas = int(Context.__map_idx_formulas.size)
        else:
            raise NotImplementedError
        return num_of_formulas

    @staticmethod
    def get_initial_state(ctx: 'Context' = None):
        if ctx is None:
            initial_state = Context.__x0
            assert isinstance(initial_state, np.ndarray)
        else:
            raise NotImplementedError
        return initial_state

    @staticmethod
    def labelling_function(partition, ctx: 'Context' = None):
        """

        :param partition: partition_idx or Partition
        :param ctx:
        :return: array of label indices
        """
        from idstl.Partition import Partition
        assert isinstance(partition, (int, Partition))
        if ctx is None:
            if isinstance(partition, int):
                partition_idx = partition
            else:
                assert isinstance(partition, Partition)
                partition_idx = partition.get_idx()
            label_ind = search_tb_col_ind(Context.__tb_partition_label, partition_idx)
            assert isinstance(label_ind, np.ndarray)
        else:
            raise NotImplementedError
        return label_ind

    @staticmethod
    def inv_labelling_function(label: Union[int, str], ctx: 'Context' = None):
        """

        :param label:
        :param ctx:
        :return: array of partition indices
        """
        if ctx is None:
            if isinstance(label, int):
                label_idx = label
            else:
                assert isinstance(label, str)
                label_idx = Context.__map_label_idx[label]
                assert label_idx is not None
            partition_ind = search_tb_row_ind(Context.__tb_partition_label, label_idx)
            assert isinstance(partition_ind, np.ndarray)
        else:
            raise NotImplementedError
        return partition_ind

    @staticmethod
    def add_partition(dimension: int, vertices: Iterable = None,
                      A_ub: Iterable = None, b_ub: Iterable = None,
                      A_eq: Iterable = None, b_eq: Iterable = None,
                      atomic_propositions: Union[set, list, np.ndarray] = None,
                      ctx: 'Context' = None):
        """
        Create a polytope P of the form:

        P := {x in R^dimension : A_ub x <= b_ub, A_eq x == b_eq} such that for all x in P, lb <= x <= ub

        or a full-dimensional polytope P of the form:

        P := {x in R^dimension : A_ub x <= b_ub} == conv(vertices) such that for all x in P, lb <= x <= ub


        --------

        Usage:

        Polytope(dimension, vertices): It returns a full-dimensional polytope. [Requires number of vertices greater than
        dimension]

        Polytope(dimension, Optional[vertices], A_ub, b_ub, Optional[A_eq, b_eq]): Returns a polytope (not necessarily
        full-dimensional).

        :param dimension:
        :param vertices:
        :param A_ub:
        :param b_ub:
        :param A_eq:
        :param b_eq:
        :param atomic_propositions:
        :param ctx:
        :return:
        """
        from idstl.Partition import Partition
        if ctx is None:
            if atomic_propositions is None or len(atomic_propositions) == 0:
                label_ind = np.array([], dtype=int)
            else:
                label_ind = np.array([
                    pi.get_label_idx() if isinstance(pi, AtomicProposition)
                    else Context.mk_atomic_proposition(pi).get_label_idx()
                    for pi in atomic_propositions
                ])
            partition = Partition(
                polytope=Polytope(dimension=dimension, vertices=vertices, A_ub=A_ub, b_ub=b_ub, A_eq=A_eq, b_eq=b_eq),
                label_ind=label_ind
            )
            Context.__map_idx_partitions = add_map_idx_object(Context.__map_idx_partitions,
                                                              partition.get_idx(), partition)
            Context.__tb_partition_label = add_tb_row(Context.__tb_partition_label,
                                                      partition.get_idx(), partition.get_label_ind())
        else:
            raise NotImplementedError
        return partition

    @staticmethod
    def add_transition(system: System, partition_from, partition_to, symbol: str = '', check_feasibility: bool = False,
                       ctx: 'Context' = None):
        from idstl.Partition import Partition
        from idstl.Transition import Transition
        assert isinstance(partition_from, Partition)
        assert isinstance(partition_to, Partition)
        if ctx is None:
            symbol_idx, Context.__map_idx_symbol, Context.__map_symbol_idx = add_map(
                Context.__map_idx_symbol, Context.__map_symbol_idx, symbol
            )
            from_idx = partition_from.get_idx()
            to_idx = partition_to.get_idx()
            transition = Transition(system, from_idx, symbol_idx, to_idx)
            if not check_feasibility or transition.is_feasible():
                transition_idx, Context.__map_idx_transition, Context.__map_transition_idx = add_map(
                    Context.__map_idx_transition, Context.__map_transition_idx, transition
                )
                if transition_idx == 0:
                    Context.__dimension = system.get_dimension()
                    Context.__num_of_inputs = system.get_num_of_inputs()
                else:
                    assert Context.__dimension == system.get_dimension()
                    assert Context.__num_of_inputs == system.get_num_of_inputs()
                assert partition_from.get_dimension() == Context.__dimension
                assert partition_to.get_dimension() == Context.__dimension
        else:
            raise NotImplementedError
        return transition

    @staticmethod
    def mk_atomic_proposition(label: str, ctx: 'Context' = None):
        assert isinstance(label, str) and len(label) > 0
        if ctx is None:
            label_idx, Context.__map_idx_label, Context.__map_label_idx = add_map(
                Context.__map_idx_label, Context.__map_label_idx, label
            )
            Context.__tb_partition_label = add_tb_col(Context.__tb_partition_label, label_idx)
            if (label_idx >= Context.__map_idx_atomic_propositions.size
                    or not isinstance(Context.__map_idx_atomic_propositions[label_idx], AtomicProposition)):
                Context.__map_idx_atomic_propositions = add_map_idx_object(
                    Context.__map_idx_atomic_propositions, label_idx, AtomicProposition(label, label_idx)
                )
            atomic_proposition = Context.__map_idx_atomic_propositions[label_idx]
            assert isinstance(atomic_proposition, AtomicProposition)
        else:
            raise NotImplementedError
        return atomic_proposition

    @staticmethod
    def get_formula(formula_idx: int, ctx: 'Context' = None):
        assert 0 <= formula_idx < Context.get_num_of_formulas(ctx)
        if ctx is None:
            formula = Context.__map_idx_formulas[formula_idx]
        else:
            raise NotImplementedError
        return formula

    @staticmethod
    def add_formula(formula, ctx: 'Context' = None):
        from idstl.Formula import Formula
        assert isinstance(formula, Formula)
        if ctx is None:
            formula_idx = int(Context.__map_idx_formulas.size)
            Context.__map_idx_formulas = add_map_idx_object(Context.__map_idx_formulas, formula_idx, formula)
        else:
            raise NotImplementedError
        return formula_idx

    @staticmethod
    def set_specification(formula, ctx: 'Context' = None):
        from idstl.Formula import Formula
        assert isinstance(formula, Formula)
        if ctx is None:
            Context.__phi = formula
        else:
            raise NotImplementedError

    @staticmethod
    def set_initial_state(x0: Union[Iterable, np.ndarray], ctx: 'Context' = None):
        x0 = np.array(x0).flatten()
        if ctx is None:
            Context.__x0 = x0
        else:
            raise NotImplementedError

    @staticmethod
    def set_initial_partition(initial_partition, ctx: 'Context' = None):
        from idstl.Partition import Partition
        assert initial_partition is None or isinstance(initial_partition, Partition)
        if ctx is None:
            Context.__P0 = initial_partition
        else:
            raise NotImplementedError

    @staticmethod
    def set_accepting_partitions(accepting_partitions: np.ndarray, ctx: 'Context' = None):
        from idstl.Partition import Partition
        assert isinstance(accepting_partitions, np.ndarray) and accepting_partitions.ndim == 1
        assert all([isinstance(partition, Partition) for partition in accepting_partitions])
        if ctx is None:
            Context.__accepting_partitions = accepting_partitions
        else:
            raise NotImplementedError

    @staticmethod
    def check(delta: Union[int, float], ctx: 'Context' = None):
        from idstl.TransitionSystem import TransitionSystem
        from idstl.Partition import Partition
        from idstl.BMC import BMC
        from idstl.Tunnel import Tunnel
        from idstl.FeasibilitySearch import best_first_search
        elapsed_time = None
        if ctx is None:
            start = None
            Context.lock()
            Context.__ts = TransitionSystem(initial_state=Context.__x0,
                                            initial_partition=Context.__P0,
                                            accepting_partitions=Context.__accepting_partitions)
            s_bar = None
            for p_idx in range(Context.get_num_of_partitions()):
                P = Context.get_partition(p_idx)
                assert isinstance(P, Partition)
                P = P.get_polytope()
                s_bar_P = np.max(P.get_ub() - P.get_lb())
                if s_bar is None or s_bar_P > s_bar:
                    s_bar = s_bar_P
            bmc = BMC(Context.__phi, Context.__ts)
            print('Checking ...')
            if Context.is_silent():
                Context.__stat_lp = 0.0
                Context.__stat_bmc = 0.0
                start = time.time()
            if Context.is_plot():
                gstuffs.plt.subplot(121)
                gstuffs.plot_transition_system(Context.get_transition_system())
                gstuffs.plt.subplot(122)
                gstuffs.draw_transition_system(Context.get_transition_system())
                gstuffs.plt.show()
            feasible_tunnel = None
            example = bmc.check()
            while example is not None:
                if Context.is_verbose():
                    print(bmc)
                root = Tunnel(transition_system=Context.__ts, delta=delta, s_bar=s_bar,
                              seq_of_partitions_idx=example['seq_of_partition_ind'],
                              seq_of_symbol_idx=example['seq_of_symbol_ind'],
                              formula=Context.__phi,
                              seq_of_formula_eval=example['formula_eval'],
                              loop_initial_idx=example['L'])
                if Context.is_plot():
                    ax = gstuffs.plot_tunnel(root)
                    ax.title('root ' + str(list(root.tunnel_model.num_of_self_loops))
                             + ' zero_prefix_step = ' + str(root.zero_prefix_step)
                             + ' distance = ' + str(np.round(root.distance, 3)))
                    gstuffs.plt.show()
                feasible_tunnel = best_first_search(root)
                if isinstance(feasible_tunnel, Tunnel):
                    break
                example = bmc.check(example['seq_of_partition_ind'][:feasible_tunnel + 1],
                                    example['seq_of_symbol_ind'][:feasible_tunnel])
            Context.__is_sat = isinstance(feasible_tunnel, Tunnel)
            Context.__is_unsat = example is None
            Context.__solution = feasible_tunnel
            if Context.is_plot() and feasible_tunnel is not None and isinstance(feasible_tunnel, Tunnel):
                gstuffs.plot_trajectory_state(feasible_tunnel)
                gstuffs.plt.show()
            if Context.is_verbose() and feasible_tunnel is not None and isinstance(feasible_tunnel, Tunnel):
                print(feasible_tunnel)
            if Context.is_silent():
                end = time.time()
                elapsed_time = end - start
                print('Elapsed time: ' + str(end - start) + ' seconds.')
            print(bmc)
            print('LP solver elapsed time: ' + str(Context.__stat_lp) + ' seconds.')
            print('SMT solver elapsed time: ' + str(Context.__stat_bmc) + ' seconds.')
        else:
            raise NotImplementedError
        return elapsed_time

    @staticmethod
    def is_sat(ctx: 'Context' = None):
        if ctx is None:
            it_is_sat = Context.__is_sat
        else:
            raise NotImplementedError
        return it_is_sat

    @staticmethod
    def is_unsat(ctx: 'Context' = None):
        if ctx is None:
            it_is_unsat = Context.__is_unsat
        else:
            raise NotImplementedError
        return it_is_unsat

    @staticmethod
    def get_solution(ctx: 'Context' = None):
        from idstl.Tunnel import Tunnel
        dist = None
        x = None
        u = None
        q = None
        if ctx is None:
            if Context.is_sat():
                assert isinstance(Context.__solution, Tunnel)
                dist, x, u = Context.__solution.get_trajectory()
                q = Context.__solution.get_path()
        else:
            raise NotImplementedError
        return dist, q, x, u

    @staticmethod
    def get_solution_tunnel(ctx: 'Context' = None):
        from idstl.Tunnel import Tunnel
        tunnel = None
        if ctx is None:
            if Context.is_sat():
                assert isinstance(Context.__solution, Tunnel)
                tunnel = Context.__solution
        else:
            raise NotImplementedError
        return tunnel

    @staticmethod
    def get_path(ctx: 'Context' = None):
        from idstl.Tunnel import Tunnel
        path = None
        if ctx is None:
            if Context.is_sat():
                assert isinstance(Context.__solution, Tunnel)
                path = Context.__solution.get_path()
        else:
            raise NotImplementedError
        return path

    @staticmethod
    def set_plot():
        Context.__plot = True

    @staticmethod
    def set_silent():
        Context.__silent = True

    @staticmethod
    def set_verbose():
        Context.__silent = False

    @staticmethod
    def is_silent():
        return Context.__silent

    @staticmethod
    def is_plot():
        return Context.__plot

    @staticmethod
    def is_verbose():
        return bool(Context.__silent is False)

    @staticmethod
    def add_lp_elapsed_time(elapsed_time):
        Context.__stat_lp += elapsed_time

    @staticmethod
    def add_bmc_elapsed_time(elapsed_time):
        Context.__stat_bmc += elapsed_time

    @staticmethod
    def sim(a: np.ndarray, u: np.ndarray, ctx: 'Context' = None):
        from idstl.TransitionSystem import TransitionSystem
        if ctx is None:
            Context.__ts = TransitionSystem(initial_state=Context.__x0,
                                            initial_partition=Context.__P0,
                                            accepting_partitions=Context.__accepting_partitions)
            q, x = Context.__ts.sim(a, u)
        else:
            raise NotImplementedError
        return q, x


def sim(a: np.ndarray, u: np.ndarray):
    return Context.sim(a, u)


def is_sat():
    return Context.is_sat()


def is_unsat():
    return Context.is_unsat()


def get_solution():
    return Context.get_solution()


def set_plot():
    Context.set_plot()


def set_silent():
    Context.set_silent()


def set_verbose():
    Context.set_verbose()


def is_silent():
    return Context.is_silent()


def is_verbose():
    return Context.is_verbose()


def is_plot():
    return Context.is_plot()


def mk_atomic_proposition(label: str):
    return Context.mk_atomic_proposition(label)


def add_partition(dimension: int, vertices: Iterable = None,
                  A_ub: Iterable = None, b_ub: Iterable = None,
                  A_eq: Iterable = None, b_eq: Iterable = None,
                  atomic_propositions: Union[set, list, np.ndarray] = None):
    """
        Create a polytope P of the form:

        P := {x in R^dimension : A_ub x <= b_ub, A_eq x == b_eq} such that for all x in P, lb <= x <= ub

        or a full-dimensional polytope P of the form:

        P := {x in R^dimension : A_ub x <= b_ub} == conv(vertices) such that for all x in P, lb <= x <= ub


        --------

        Usage:

        Polytope(dimension, vertices): It returns a full-dimensional polytope. [Requires number of vertices greater than
        dimension]

        Polytope(dimension, Optional[vertices], A_ub, b_ub, Optional[A_eq, b_eq]): Returns a polytope (not necessarily
        full-dimensional).

        :param dimension:
        :param vertices:
        :param A_ub:
        :param b_ub:
        :param A_eq:
        :param b_eq:
        :param atomic_propositions:
        :return:
    """
    return Context.add_partition(dimension=dimension, vertices=vertices, A_ub=A_ub, b_ub=b_ub, A_eq=A_eq, b_eq=b_eq,
                                 atomic_propositions=atomic_propositions)


def add_transition(system: System, partition_from, partition_to, symbol: str = '', check_feasibility: bool = False):
    return Context.add_transition(system, partition_from, partition_to, symbol, check_feasibility)


def set_specification(formula):
    Context.set_specification(formula)


def set_initial_state(x0: np.ndarray):
    Context.set_initial_state(x0)


def set_initial_partition(initial_partition):
    Context.set_initial_partition(initial_partition)


def set_accepting_partitions(accepting_partitions: np.ndarray):
    Context.set_accepting_partitions(accepting_partitions)


def check(delta: Union[int, float]):
    return Context.check(delta)


def is_iterable(a):
    try:
        _ = iter(a)
    except TypeError:
        return False
    return True


def is_vector(a):
    return ((isinstance(a, (np.ndarray, sparse.spmatrix)) and np.issubdtype(a.dtype, np.number) and
             ((a.ndim == 2 and a.shape[0] == 1) or (a.ndim == 2 and a.shape[1] == 1)))
            or (isinstance(a, list) and all([isinstance(e, (int, float)) for e in a])))


def is_matrix(a):
    return ((isinstance(a, (np.ndarray, sparse.spmatrix)) and np.issubdtype(a.dtype, np.number) and a.ndim == 2)
            or (isinstance(a, list) and all([isinstance(e, list) for e in a])
                and all([[isinstance(col, (int, float)) for col in row] for row in a])))


def get_partition(idx):
    from idstl.Partition import Partition
    partition = Context.get_partition(idx)
    assert isinstance(partition, Partition)
    return partition


def get_partitions():
    return Context.get_partitions()


def get_num_of_partitions():
    return Context.get_num_of_partitions()


def plot_trajectory(axis: Iterable[int] = None):
    if axis is None:
        axis = [0, 1]
    tunnel = Context.get_solution_tunnel()
    gstuffs.plot_trajectory_state(tunnel, axis=axis, title=str(tunnel))
    gstuffs.plt.show()


def plot_time_trajectory(axis: Iterable[int] = None):
    if axis is None:
        axis = [0, 1]
    tunnel = Context.get_solution_tunnel()
    gstuffs.plot_trajectory_time(tunnel, axis=axis, title=str(tunnel))
    gstuffs.plt.show()


def plot_polytope(polytope, axis=None, **kwargs):
    gstuffs.plot_polytope(polytope, axis, **kwargs)


def add_map_object_idx(map_object_idx: np.ndarray, idx: int, value):
    obj = np.searchsorted(map_object_idx[:, 0], value, 'right')
    if 0 < obj and map_object_idx[obj - 1, 0] == value:
        raise ValueError('value must be unique')
    return np.insert(map_object_idx, obj, np.array([value, idx]), axis=0)


def add_map_idx_object(map_idx_object: np.ndarray, idx: int, value):
    if idx >= map_idx_object.size:
        map_idx_object = np.hstack((map_idx_object, np.zeros((idx + 1 - map_idx_object.size))))
    map_idx_object[idx] = value
    return map_idx_object


def add_map(map_idx_object: np.ndarray, map_object_idx: np.ndarray, value):
    idx = search_map_object_idx(map_object_idx, value)
    if idx is None:
        idx = map_idx_object.size
        map_idx_object = add_map_idx_object(map_idx_object, idx, value)
        map_object_idx = add_map_object_idx(map_object_idx, idx, value)
    return idx, map_idx_object, map_object_idx


def search_map_object_idx(map_object_idx: np.ndarray, value):
    idx = np.searchsorted(map_object_idx[:, 0], value, 'right') - 1
    if 0 <= idx and map_object_idx[idx, 0] == value:
        return int(map_object_idx[idx, 1])
    return None


def search_tb_col_ind(tb: np.ndarray, row_idx):
    return np.where(tb[row_idx, :])[0]


def search_tb_row_ind(tb: np.ndarray, col_idx):
    return np.where(tb[:, col_idx])[0]


def add_tb_row(tb: np.ndarray, row_idx, col_ind: np.ndarray = None):
    if tb.shape[0] <= row_idx:
        tb = np.vstack((tb, np.zeros((tb.shape[0] + 1 - row_idx, tb.shape[1]), dtype=bool)))
    if col_ind is not None:
        tb[row_idx, col_ind] = True
    return tb


def add_tb_col(tb: np.ndarray, col_idx, row_ind: np.ndarray = None):
    if tb.shape[1] <= col_idx:
        tb = np.hstack((tb, np.zeros((tb.shape[0], tb.shape[1] + 1 - col_idx), dtype=bool)))
    if row_ind is not None:
        tb[row_ind, col_idx] = True
    return tb
