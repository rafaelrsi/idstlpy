from idstl.Context import Context, TOLERANCE, MAX_INT, add_partition, add_transition, mk_atomic_proposition, \
    set_initial_partition, set_accepting_partitions, set_initial_state, set_specification, check, \
    get_solution, is_sat, is_unsat, set_silent, set_verbose, get_partition, get_num_of_partitions, \
    plot_trajectory, plot_time_trajectory, plot_polytope, set_plot, sim
from idstl.System import System
from idstl.Polytope import Polytope
from idstl.Partition import Partition
from idstl.LPModel import INFINITY
from idstl.Formula import Not, And, Or, Until, Release, true, false

if __name__ == '__main__':
    import numpy as np
    num_of_obstacles = 2
    assert isinstance(num_of_obstacles, int) and 0 <= num_of_obstacles <= 8
    v_max = 2
    a_max = 2
    vxy_vertices = np.array([[-v_max, -v_max], [v_max, -v_max], [v_max, v_max], [-v_max, v_max]])
    x_vertices = np.hstack((np.vstack((0, np.reshape(np.arange(0.5, 30, 2), (-1, 1)))),
                            np.vstack((np.reshape(np.arange(0.5, 30, 2), (-1, 1)), 30))))
    y_vertices = [
        np.array([[0, 30, -1]]),  # free
        np.array([[0, 5, -1],  # free
                  [5, 30, 0]]),  # obstacle 1
        np.array([[0, 30, -1]]),  # free
        np.array([[27, 30, -1],  # free
                  [0, 27, 0]]),  # obstacle 2
        np.array([[0, 30, -1]]),  # free
        np.array([[0, 8, -1],  # free
                  [8, 30, 0]]),  # obstacle 3
        np.array([[0, 30, -1]]),  # free
        np.array([[24, 30, -1],  # free
                  [0, 24, 0]]),  # obstacle 4
        np.array([[0, 30, -1]]),  # free
        np.array([[0, 11, -1],  # free
                  [11, 30, 0]]),  # obstacle 5
        np.array([[0, 30, -1]]),  # free
        np.array([[21, 30, -1],  # free
                  [0, 21, 0]]),  # obstacle 6
        np.array([[0, 30, -1]]),  # free
        np.array([[0, 14, -1],  # free
                  [14, 30, 0]]),  # obstacle 7
        np.array([[0, 30, -1]]),  # free
        np.array([[21, 30, -1],  # free
                  [18, 21, 1],  # target
                  [0, 18, 0]])  # obstacle 8
    ]
    pxy_vertices = []
    for i in range(2 * num_of_obstacles):
        x0 = x_vertices[i, 0]
        x1 = x_vertices[i, 1]
        for j in range(y_vertices[i].shape[0]):
            y0 = y_vertices[i][j, 0]
            y1 = y_vertices[i][j, 1]
            label = None
            if y_vertices[i][j, 2] == 0:
                label = ['o']
            elif y_vertices[i][j, 2] == 1:
                label = ['t']
            pxy_vertices.append((np.array([[x0, y0], [x1, y0], [x1, y1], [x0, y1]]), label))
    if 0 <= num_of_obstacles < 8:
        x0 = x_vertices[2 * num_of_obstacles, 0]
        x1 = 28.5
        y0 = 0
        y1 = 30
        pxy_vertices.append((np.array([[x0, y0], [x1, y0], [x1, y1], [x0, y1]]), None))
        i = -1
        x0 = x_vertices[i, 0]
        x1 = x_vertices[i, 1]
        for j in range(y_vertices[i].shape[0]):
            y0 = y_vertices[i][j, 0]
            y1 = y_vertices[i][j, 1]
            label = None
            if y_vertices[i][j, 2] == 1:
                label = ['t']
            pxy_vertices.append((np.array([[x0, y0], [x1, y0], [x1, y1], [x0, y1]]), label))
    q0 = System(dimension=4, num_of_inputs=2,
                A=[[1, 0, 0.5, 0], [0, 1, 0, 0.5], [0, 0, 1, 0], [0, 0, 0, 1]],
                B=[[0.25, 0], [0, 0.25], [0.5, 0], [0, 0.5]],
                c=None,
                U=Polytope(dimension=2, vertices=[[a_max, a_max], [-a_max, a_max], [-a_max, -a_max], [a_max, -a_max]]))
    for i in range(len(pxy_vertices)):
        add_partition(dimension=4,
                      vertices=np.hstack((np.tile(pxy_vertices[i][0], (4, 1)), np.repeat(vxy_vertices, 4, 0))),
                      atomic_propositions=pxy_vertices[i][1])
    for i in range(get_num_of_partitions()):
        for j in range(get_num_of_partitions()):
            if i == j or q0.is_reachable(get_partition(i).get_polytope(), get_partition(j).get_polytope()):
                add_transition(q0, get_partition(i), get_partition(j))
    obstacle = mk_atomic_proposition('o')
    target = mk_atomic_proposition('t')
    phi = Not(obstacle).until(0, 1000, target.always(0, 10))
    set_specification(phi)
    set_initial_state(np.array([1, 1, 0, 0]))
    set_silent()
    # set_plot()
    is_sat = check(0.01)
    assert is_sat
    plot_trajectory([0, 1])
    plot_time_trajectory([2, 3])
