from typing import Union
import numpy as np
from scipy import sparse
from idstl.LPModel import LPModel, INFINITY
from copy import copy


def get_d_eq_idx(k: int):
    return 3 * k + 2


def get_x_eq_idx(k: int):
    return 3 * k


def get_u_eq_idx(k: int):
    return 3 * k + 1


def get_x_ineq_idx(k: int):
    return 3 * k + 1


def get_u_ineq_idx(k: int):
    return 3 * k + 2


def get_s_ineq_idx(k: int):
    return 3 * k


class DistanceModel(LPModel):
    def __init__(self, dimension: int, num_of_inputs: int, delta: Union[float, int], s_bar: Union[float, int],
                 q_prev, x0,
                 q_self_loop, x_inv,
                 q_next, x1):
        from idstl.Polytope import Polytope
        from idstl.System import System
        is_initialized = not isinstance(x0, Polytope)
        if is_initialized:
            x0 = sparse.coo_matrix(np.reshape(x0, (dimension, 1)))
        assert isinstance(q_prev, System)
        assert isinstance(q_self_loop, System)
        assert isinstance(x_inv, Polytope)
        assert isinstance(q_next, System)
        assert isinstance(x1, Polytope)
        u0_lb = q_prev.get_U().get_lb()
        u0_ub = q_prev.get_U().get_ub()
        u1_lb = q_self_loop.get_U().get_lb()
        u1_ub = q_self_loop.get_U().get_ub()
        u2_lb = q_next.get_U().get_lb()
        u2_ub = q_next.get_U().get_ub()
        x1_lb = x_inv.get_lb()
        x1_ub = x_inv.get_ub()
        x2_lb = x1.get_lb()
        x2_ub = x1.get_ub()
        if is_initialized:
            x0_lb = None
            x0_ub = None
        else:
            assert isinstance(x0, Polytope)
            x0_lb = x0.get_lb()
            x0_ub = x0.get_ub()
        super().__init__(0, np.array([]), np.array([]))
        self.is_initialized = is_initialized
        self.delta = delta
        self.s_bar = s_bar
        self.x0 = x0
        self.q0 = q_prev
        self.q1 = q_self_loop
        self.x1 = x_inv
        self.q2 = q_next
        self.x2 = x1
        self.u0_lb = u0_lb
        self.u0_ub = u0_ub
        self.x0_lb = x0_lb
        self.x0_ub = x0_ub
        self.u1_lb = u1_lb
        self.u1_ub = u1_ub
        self.x1_lb = x1_lb
        self.x1_ub = x1_ub
        self.u2_lb = u2_lb
        self.u2_ub = u2_ub
        self.x2_lb = x2_lb
        self.x2_ub = x2_ub
        self.n = dimension
        self.m = num_of_inputs
        self.K = 0
        self.add_step()

    def add_variables_at(self, idx: int, lb: np.ndarray, ub: np.ndarray, c: np.ndarray = None):
        assert lb.ndim == 1 and ub.ndim == 1 and lb.size == ub.size
        assert 0 <= idx <= self.num_of_vars
        num_of_vars = lb.size
        if c is None:
            c = np.zeros(num_of_vars)
        self.c = np.hstack((self.c[:idx], c, self.c[idx:]))
        self.lb = np.hstack((self.lb[:idx], lb, self.lb[idx:]))
        self.ub = np.hstack((self.ub[:idx], ub, self.ub[idx:]))
        self.A_ub_J[self.A_ub_J >= idx] += num_of_vars
        self.A_eq_J[self.A_ub_J >= idx] += num_of_vars

    def add_slack_const(self, inequality_idx: int, k: int):
        self.add_inequality(
            inequality_idx=inequality_idx,
            lhs_V=np.hstack((
                -np.ones(self.n), np.ones(self.n),  # v_k <= s_k
                -np.ones(self.n), -np.ones(self.n),  # - v_k <= s_k
                -(self.s_bar / self.delta), 1,  # (delta/s_bar) s_k <= s_k+1
            )),
            lhs_I=np.hstack((
                np.arange(self.n), np.arange(self.n),  # v_k <= s_k
                np.arange(self.n, 2 * self.n), np.arange(self.n, 2 * self.n),  # - v_k <= s_k
                2 * self.n, 2 * self.n,  # (delta/s_bar) s_k <= s_k+1
            )),
            lhs_J=np.hstack((
                np.repeat(self.get_s_ind(k), self.n), self.get_v_ind(k),  # v_k <= s_k
                np.repeat(self.get_s_ind(k), self.n), self.get_v_ind(k),  # - v_k <= s_k
                self.get_s_ind(k), self.get_s_ind(k + 1),  # (delta/s_bar) s_k <= s_k+1
            )),
            rhs_V=np.zeros(0),
            rhs_I=np.zeros(0)
        )

    def add_step(self):
        """
        if is initialized then

            X = [s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        else

            X = [x_0,s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        where:
        * s_k is an slack variable (R)
        * v_k is a vector of proxy slack variables (R^n)
        * u_k is a vector of input variables (R^m)
        * x_k is a vector of state variables (R^n)

        Therefore, :
        if is initialized, X = [s_0,v_0,u_0, ... ,x_K+1,s_K+1]
        else, X = [x_0,s_0,v_0,u_0, ... ,x_K+1,s_K+1]
        """
        self.K += 1
        z_n = np.zeros((self.n, 1))
        I_n = np.identity(self.n)
        if self.is_initialized:
            if self.K == 1:
                self.add_variables_at(
                    idx=0,
                    lb=np.hstack((0.0, -INFINITY * np.ones(self.n), self.u2_lb, self.x2_lb, 0.0)),
                    ub=np.hstack((INFINITY, INFINITY * np.ones(self.n), self.u2_ub, self.x2_ub, INFINITY)),
                    c=np.hstack((np.zeros(1 + 2 * self.n + self.m), 1.0))
                )
                # output:
                # min s_1
                # s.t.
                # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub, 0 <= s_1
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(self.K),
                                     equality_idx=get_x_ineq_idx(self.K),
                                     var_offset=self.get_x_ind(1).start,
                                     polytope=self.x2)
                self.add_polytope_at(inequality_idx=get_u_ineq_idx(self.K),
                                     equality_idx=get_u_ineq_idx(self.K),
                                     var_offset=self.get_u_ind(0).start,
                                     polytope=self.q2.get_U())
                # output:
                # min s_1
                # s.t.
                # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub, 0 <= s_1
                # (X1) x_1 in x2
                # (U1) u_0 in u2
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(self.K),
                    lhs=sparse.hstack((z_n, -I_n, -self.q2.get_B(), I_n), format='coo'),
                    rhs=sparse.coo_matrix(self.q2.get_c() + self.q2.get_A() @ self.x0)
                )
                # output:
                # min s_1
                # s.t.
                # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub, 0 <= s_1
                # (X1) x_1 in x2
                # (U1) u_0 in u2
                # (D1) x_1 = A2 @ x0 + B2 @ u_0 + c2 + v_0
                self.add_slack_const(inequality_idx=get_s_ineq_idx(self.K), k=0)
                # output:
                # min s_1
                # s.t.
                # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub, 0 <= s_1
                # (X1) x_1 in x2
                # (U1) u_0 in u2
                # (D1) x_1 = A2 @ x0 + B2 @ u_0 + c2 + v_0
                # (S1) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
            elif self.K == 2:
                self.del_equality(get_d_eq_idx(self.K - 1))
                # output:
                # min s_1
                # s.t.
                # 0 <= s_0, v_0 free, u2_lb <= u_0 <= u2_ub, x2_lb <= x_1 <= x2_ub, 0 <= s_1
                # (X1) x_1 in x2
                # (U1) u_0 in u2
                # (S1) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                self.add_variables_at(
                    idx=0,
                    lb=np.hstack((0.0, -INFINITY * np.ones(self.n), self.u0_lb, self.x1_lb)),
                    ub=np.hstack((INFINITY, INFINITY * np.ones(self.n), self.u0_ub, self.x1_ub))
                )
                # output:
                # min s_2
                # s.t.
                # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X1) x_2 in x2
                # (U1) u_1 in u2
                # (S1) v_1 <= s_1, -v_1 <= s_1, (s_bar/delta) * s_1 <= s_2
                M = sparse.hstack((-self.q2.get_A(), -I_n, -self.q2.get_B(), I_n),
                                  format='coo')  # type: sparse.coo_matrix
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(self.K - 1),
                    lhs=sparse.coo_matrix((M.data, (M.row, M.col + self.get_x_ind(1).start))),
                    rhs=self.q2.get_c()
                )
                # output:
                # min s_2
                # s.t.
                # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X1) x_2 in x2
                # (U1) u_1 in u2
                # (S1) v_1 <= s_1, -v_1 <= s_1, (s_bar/delta) * s_1 <= s_2
                # (D1) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(self.K),
                                     equality_idx=get_x_ineq_idx(self.K),
                                     var_offset=self.get_x_ind(1).start,
                                     polytope=self.x1)
                self.add_polytope_at(inequality_idx=get_u_ineq_idx(self.K),
                                     equality_idx=get_u_ineq_idx(self.K),
                                     var_offset=self.get_u_ind(0).start,
                                     polytope=self.q0.get_U())
                # output:
                # min s_2
                # s.t.
                # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X1) x_2 in x2
                # (U1) u_1 in u2
                # (S1) v_1 <= s_1, -v_1 <= s_1, (s_bar/delta) * s_1 <= s_2
                # (D1) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
                # (X2) x_1 in x1
                # (U2) u_0 in u0
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(self.K - 1),
                    lhs=sparse.hstack((z_n, -I_n, -self.q0.get_B(), I_n), format='coo'),
                    rhs=sparse.coo_matrix(self.q0.get_c() + self.q0.get_A() @ self.x0)
                )
                # output:
                # min s_2
                # s.t.
                # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X1) x_2 in x2
                # (U1) u_1 in u2
                # (S1) v_1 <= s_1, -v_1 <= s_1, (s_bar/delta) * s_1 <= s_2
                # (D1) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
                # (X2) x_1 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                self.add_slack_const(inequality_idx=get_s_ineq_idx(self.K), k=0)
                # output:
                # min s_2
                # s.t.
                # 0 <= s_1, v_1 free, u2_lb <= u_1 <= u2_ub, x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X1) x_2 in x2
                # (U1) u_1 in u2
                # (S1) v_1 <= s_1, -v_1 <= s_1, (s_bar/delta) * s_1 <= s_2
                # (D1) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
                # (X2) x_1 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
            else:
                # output:
                # min s_K-1
                # s.t.
                # 0 <= s_K-2, v_K-2 free, u2_lb <= u_K-2 <= u2_ub, x2_lb <= x_K-1 <= x2_ub, 0 <= s_K-1
                # (X1) x_K-1 in x2
                # (U1) u_K-2 in u2
                # (S1) v_K-2 <= s_K-2, -v_K-2 <= s_K-2, (s_bar/delta) * s_K-2 <= s_K-1
                # (D1) x_K-1 = A2 @ x_K-2 + B2 @ u_K-2 + c2 + v_K-2
                # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub, 0 <= s_1
                # (X2) x_2 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # for k = K-3 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K-k+1>) x_k in x1
                # (U<K-k+1>) u_k in u1
                # (D<K-k+1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K-k+1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                self.add_variables_at(
                    idx=1 + self.n + self.m,
                    lb=np.hstack((self.x1_lb, 0.0, -INFINITY * np.ones(self.n), self.u1_lb)),
                    ub=np.hstack((self.x1_ub, INFINITY, INFINITY * np.ones(self.n), self.u1_ub))
                )
                # output:
                # min s_K
                # s.t.
                # 0 <= s_K-1, v_K-1 free, u2_lb <= u_K-1 <= u2_ub, x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X1) x_K in x2
                # (U1) u_K-1 in u2
                # (S1) v_K-1 <= s_K-1, -v_K-1 <= s_K-1, (s_bar/delta) * s_K-1 <= s_K
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub,
                # (X2) x_2 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_2
                # for k = K-3 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K-k+1>) x_k in x1
                # (U<K-k+1>) u_k in u1
                # (D<K-k+1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K-k+1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                M_I = self.inequality_idx_lhs_range[get_d_eq_idx(2)]
                M_J = self.A_eq_J[M_I]
                M_J[np.isin(M_J, self.get_x_ind(2))] -= self.get_x_ind(2).start - self.get_x_ind(1).start
                self.A_eq_J[M_I] = M_J
                M_I = self.inequality_idx_lhs_range[get_s_ineq_idx(2)]
                M_J = self.A_ub_J[M_I]
                M_J[np.isin(M_J, self.get_s_ind(2))] -= self.get_s_ind(2).start - self.get_s_ind(1).start
                self.A_ub_J[M_I] = M_J
                # output:
                # min s_K
                # s.t.
                # 0 <= s_K-1, v_K-1 free, u2_lb <= u_K-1 <= u2_ub, x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X1) x_K in x2
                # (U1) u_K-1 in u2
                # (S1) v_K-1 <= s_K-1, -v_K-1 <= s_K-1, (s_bar/delta) * s_K-1 <= s_K
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub,
                # (X2) x_2 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # for k = K-3 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K-k+1>) x_k in x1
                # (U<K-k+1>) u_k in u1
                # (D<K-k+1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K-k+1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(self.K),
                                     equality_idx=get_x_ineq_idx(self.K),
                                     var_offset=self.get_x_ind(1).start,
                                     polytope=self.x1)
                self.add_polytope_at(inequality_idx=get_u_ineq_idx(self.K),
                                     equality_idx=get_u_ineq_idx(self.K),
                                     var_offset=self.get_u_ind(1).start,
                                     polytope=self.q1.get_U())
                # output:
                # min s_K
                # s.t.
                # 0 <= s_K-1, v_K-1 free, u2_lb <= u_K-1 <= u2_ub, x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X1) x_K in x2
                # (U1) u_K-1 in u2
                # (S1) v_K-1 <= s_K-1, -v_K-1 <= s_K-1, (s_bar/delta) * s_K-1 <= s_K
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub, 0 <= s_1
                # (X2) x_2 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # for k = K-2 .. 2
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K-k+1>) x_k in x1
                # (U<K-k+1>) u_k in u1
                # (D<K-k+1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K-k+1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub
                # (X<K>) x_1 in x1
                # (U<K>) u_1 in u1
                M = sparse.hstack((-self.q1.get_A(), -I_n, -self.q1.get_B(), I_n), format='coo')
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(self.K),
                    lhs=sparse.coo_matrix((M.data, (M.row, M.col + self.get_x_ind(1).start))),
                    rhs=self.q1.get_c()
                )
                # output:
                # min s_K
                # s.t.
                # 0 <= s_K-1, v_K-1 free, u2_lb <= u_K-1 <= u2_ub, x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X1) x_K in x2
                # (U1) u_K-1 in u2
                # (S1) v_K-1 <= s_K-1, -v_K-1 <= s_K-1, (s_bar/delta) * s_K-1 <= s_K
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub, 0 <= s_1
                # (X2) x_2 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # for k = K-2 .. 2
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K-k+1>) x_k in x1
                # (U<K-k+1>) u_k in u1
                # (D<K-k+1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K-k+1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub
                # (X<K>) x_1 in x1
                # (U<K>) u_1 in u1
                # (D<K>) x_2 = A1 @ x_1 + B1 @ u_1 + c1 + v_1
                self.add_slack_const(inequality_idx=get_s_ineq_idx(self.K), k=1)
                # output:
                # min s_K
                # s.t.
                # 0 <= s_K-1, v_K-1 free, u2_lb <= u_K-1 <= u2_ub, x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X1) x_K in x2
                # (U1) u_K-1 in u2
                # (S1) v_K-1 <= s_K-1, -v_K-1 <= s_K-1, (s_bar/delta) * s_K-1 <= s_K
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # 0 <= s_0, v_0 free, u1_lb <= u_0 <= u1_ub, x1_lb <= x_2 <= x1_ub, 0 <= s_1
                # (X2) x_2 in x1
                # (U2) u_0 in u0
                # (D2) x_1 = A0 @ x0 + B0 @ u_0 + c0 + v_0
                # (S2) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # for k = K-2 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K-k+1>) x_k in x1
                # (U<K-k+1>) u_k in u1
                # (D<K-k+1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K-k+1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
        else:
            if self.K == 1:
                self.K += 1
                self.add_variables_at(
                    idx=0,
                    lb=np.hstack((self.x0_lb, 0.0, -INFINITY * np.ones(self.n), self.u0_lb,
                                  self.x1_lb, 0.0, -INFINITY * np.ones(self.n), self.u2_lb,
                                  self.x2_lb, 0.0)),
                    ub=np.hstack((self.x0_lb, INFINITY, INFINITY * np.ones(self.n), self.u0_ub,
                                  self.x1_ub, INFINITY, INFINITY * np.ones(self.n), self.u2_ub,
                                  self.x2_ub, INFINITY)),
                    c=np.hstack((np.zeros(2 + 5 * self.n + 2 * self.m), 1.0))
                )
                # output:
                # min s_2
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                # x2_lb <= x_2 <= x2_ub, 0 <= s_2
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(0),
                                     equality_idx=get_x_ineq_idx(0),
                                     var_offset=self.get_x_ind(0).start,
                                     polytope=self.x0)
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(2),
                                     equality_idx=get_x_ineq_idx(2),
                                     var_offset=self.get_x_ind(1).start,
                                     polytope=self.x1)
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(1),
                                     equality_idx=get_x_ineq_idx(1),
                                     var_offset=self.get_x_ind(2).start,
                                     polytope=self.x2)
                self.add_polytope_at(inequality_idx=get_u_ineq_idx(0),
                                     equality_idx=get_u_ineq_idx(0),
                                     var_offset=self.get_u_ind(0).start,
                                     polytope=self.q0.get_U())
                self.add_polytope_at(inequality_idx=get_u_ineq_idx(1),
                                     equality_idx=get_u_ineq_idx(1),
                                     var_offset=self.get_u_ind(1).start,
                                     polytope=self.q2.get_U())
                # output:
                # min s_2
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                # x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X0) x_0 in x0
                # (X2) x_1 in x1
                # (X1) x_2 in x2
                # (U0) u_0 in u1
                # (U1) u_1 in u2
                lhs = sparse.hstack((self.q0.get_A(), -I_n, -self.q0.get_B(), I_n), format='coo')
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(0),
                    lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.get_x_ind(0).start))),
                    rhs=self.q0.get_c()
                )
                lhs = sparse.hstack((self.q2.get_A(), -I_n, -self.q2.get_B(), I_n), format='coo')
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(1),
                    lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.get_x_ind(1).start))),
                    rhs=self.q2.get_c()
                )
                # output:
                # min s_2
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                # x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X0) x_0 in x0
                # (X2) x_1 in x1
                # (X1) x_2 in x2
                # (U0) u_0 in u1
                # (U1) u_1 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
                self.add_slack_const(inequality_idx=get_s_ineq_idx(0), k=0)
                self.add_slack_const(inequality_idx=get_s_ineq_idx(1), k=1)
                # output:
                # min s_2
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                # x2_lb <= x_2 <= x2_ub, 0 <= s_2
                # (X0) x_0 in x0
                # (X2) x_1 in x1
                # (X1) x_2 in x2
                # (U2) u_0 in u1
                # (U1) u_1 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_2 = A2 @ x_1 + B2 @ u_1 + c2 + v_1
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # (S1) v_1 <= s_1, -v_1 <= s_1, (s_bar/delta) * s_1 <= s_2
            else:
                # output:
                # min s_K-1
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_K-2 <= x1_ub, 0 <= s_K-2, v_K-2 free, u1_lb <= u_K-2 <= u1_ub,
                # x2_lb <= x_K-1 <= x2_ub, 0 <= s_K-1
                # (X0) x_0 in x0
                # (X1) x_K-1 in x2
                # (U0) u_0 in u1
                # (U1) u_K-2 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_K-1 = A2 @ x_K-2 + B2 @ u_K-2 + c2 + v_K-2
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # (S1) v_K-2 <= s_K-2, -v_K-2 <= s_K-2, (s_bar/delta) * s_K-2 <= s_K-1
                # for k = K-2 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K - k + 1>) x_k in x1
                # (U<K - k + 1>) u_k in u1
                # (D<K - k + 1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K - k + 1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                self.add_variables_at(
                    idx=self.get_x_ind(1).start,
                    lb=np.hstack((self.x1_lb, 0.0, -INFINITY * np.ones(self.n), self.u1_lb)),
                    ub=np.hstack((self.x1_ub, INFINITY, INFINITY * np.ones(self.n), self.u1_ub))
                )
                # output:
                # min s_K
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_K-1 <= x1_ub, 0 <= s_K-1, v_K-1 free, u1_lb <= u_K-1 <= u1_ub,
                # x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X0) x_0 in x0
                # (X1) x_K in x2
                # (U0) u_0 in u1
                # (U1) u_K-1 in u2
                # (D0) x_2 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_2
                # (S1) v_K-2 <= s_K-2, -v_K-2 <= s_K-2, (s_bar/delta) * s_K-2 <= s_K-1
                # for k = K-2 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K - k + 1>) x_k in x1
                # (U<K - k + 1>) u_k in u1
                # (D<K - k + 1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K - k + 1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                M_I = self.inequality_idx_lhs_range[get_d_eq_idx(0)]
                M_J = self.A_eq_J[M_I]
                M_J[np.isin(M_J, self.get_x_ind(2))] -= self.get_x_ind(2).start - self.get_x_ind(1).start
                self.A_eq_J[M_I] = M_J
                M_I = self.inequality_idx_lhs_range[get_s_ineq_idx(0)]
                M_J = self.A_ub_J[M_I]
                M_J[np.isin(M_J, self.get_s_ind(2))] -= self.get_s_ind(2).start - self.get_s_ind(1).start
                self.A_ub_J[M_I] = M_J
                # output:
                # min s_K
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_K-1 <= x1_ub, 0 <= s_K-1, v_K-1 free, u1_lb <= u_K-1 <= u1_ub,
                # x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X0) x_0 in x0
                # (X1) x_K in x2
                # (U0) u_0 in u1
                # (U1) u_K-1 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # (S1) v_K-2 <= s_K-2, -v_K-2 <= s_K-2, (s_bar/delta) * s_K-2 <= s_K-1
                # for k = K-2 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K - k + 1>) x_k in x1
                # (U<K - k + 1>) u_k in u1
                # (D<K - k + 1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K - k + 1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                self.add_polytope_at(inequality_idx=get_x_ineq_idx(self.K),
                                     equality_idx=get_x_ineq_idx(self.K),
                                     var_offset=self.get_x_ind(1).start,
                                     polytope=self.x1)
                self.add_polytope_at(inequality_idx=get_u_ineq_idx(self.K),
                                     equality_idx=get_u_ineq_idx(self.K),
                                     var_offset=self.get_u_ind(1).start,
                                     polytope=self.q1.get_U())
                # output:
                # min s_K
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_K-1 <= x1_ub, 0 <= s_K-1, v_K-1 free, u1_lb <= u_K-1 <= u1_ub,
                # x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X0) x_0 in x0
                # (X1) x_K in x2
                # (U0) u_0 in u1
                # (U1) u_K-1 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # (S1) v_K-2 <= s_K-2, -v_K-2 <= s_K-2, (s_bar/delta) * s_K-2 <= s_K-1
                # for k = K-2 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K - k + 1>) x_k in x1
                # (U<K - k + 1>) u_k in u1
                # (D<K - k + 1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K - k + 1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                # (X3) x_1 in x1
                # (U3) u_1 in u1
                lhs = sparse.hstack((self.q1.get_A(), -I_n, -self.q1.get_B(), I_n), format='coo')
                self.add_equality_from_sparse(
                    equality_idx=get_d_eq_idx(self.K),
                    lhs=sparse.coo_matrix((lhs.data, (lhs.row, lhs.col + self.get_x_ind(1).start))),
                    rhs=self.q1.get_c()
                )
                # output:
                # min s_K
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_K-1 <= x1_ub, 0 <= s_K-1, v_K-1 free, u1_lb <= u_K-1 <= u1_ub,
                # x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X0) x_0 in x0
                # (X1) x_K in x2
                # (U0) u_0 in u1
                # (U1) u_K-1 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # (S1) v_K-2 <= s_K-2, -v_K-2 <= s_K-2, (s_bar/delta) * s_K-2 <= s_K-1
                # for k = K-2 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K - k + 1>) x_k in x1
                # (U<K - k + 1>) u_k in u1
                # (D<K - k + 1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K - k + 1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1
                # x1_lb <= x_1 <= x1_ub, 0 <= s_1, v_1 free, u1_lb <= u_1 <= u1_ub,
                # (X3) x_1 in x1
                # (U3) u_1 in u1
                # (D3) x_2 = A1 @ x_1 + B1 @ u_1 + c1 + v_1
                self.add_slack_const(inequality_idx=get_s_ineq_idx(self.K), k=1)
                # output:
                # min s_K
                # s.t.
                # x0_lb <= x_0 <= x0_ub, 0 <= s_0, v_0 free, u0_lb <= u_0 <= u0_ub,
                # x1_lb <= x_K-1 <= x1_ub, 0 <= s_K-1, v_K-1 free, u1_lb <= u_K-1 <= u1_ub,
                # x2_lb <= x_K <= x2_ub, 0 <= s_K
                # (X0) x_0 in x0
                # (X1) x_K in x2
                # (U0) u_0 in u1
                # (U1) u_K-1 in u2
                # (D0) x_1 = A0 @ x_0 + B0 @ u_0 + c0 + v_0
                # (D1) x_K = A2 @ x_K-1 + B2 @ u_K-1 + c2 + v_K-1
                # (S0) v_0 <= s_0, -v_0 <= s_0, (s_bar/delta) * s_0 <= s_1
                # (S1) v_K-1 <= s_K-1, -v_K-1 <= s_K-1, (s_bar/delta) * s_K-1 <= s_K
                # for k = K-1 .. 1
                # x1_lb <= x_k <= x1_ub, 0 <= s_k, v_k free, u1_lb <= u_k <= u1_ub,
                # (X<K - k + 1>) x_k in x1
                # (U<K - k + 1>) u_k in u1
                # (D<K - k + 1>) x_k+1 = A1 @ x_k + B1 @ u_k + c1 + v_k
                # (S<K - k + 1>) v_k <= s_k, -v_k <= s_k, (s_bar/delta) * s_k <= s_k+1

    def add_polytope_at(self, inequality_idx: int, equality_idx: int, var_offset: int, polytope):
        from idstl.Polytope import Polytope
        assert isinstance(polytope, Polytope)
        if polytope.get_num_of_inequalities() > 0:
            A_ub = polytope.get_A_ub()
            lhs = sparse.coo_matrix((A_ub.data, (A_ub.row, A_ub.col + var_offset)))
            rhs = polytope.get_b_ub()
            self.add_inequality_from_sparse(inequality_idx=inequality_idx, lhs=lhs, rhs=rhs)
        if polytope.get_num_of_equalities() > 0:
            A_eq = polytope.get_A_eq()
            lhs = sparse.coo_matrix((A_eq.data, (A_eq.row, A_eq.col + var_offset)))
            rhs = polytope.get_b_eq()
            self.add_equality_from_sparse(equality_idx=equality_idx, lhs=lhs, rhs=rhs)

    def __add_X2_const(self):
        col_offset = self.get_x_ind(self.K).start
        if self.x2.get_num_of_inequalities() > 0:
            A_ub = self.x2.get_A_ub()
            lhs = sparse.coo_matrix((A_ub.data, (A_ub.row, A_ub.col + col_offset)))
            rhs = self.x2.get_b_ub()
            self.add_inequality_from_sparse(inequality_idx=get_x_ineq_idx(self.K), lhs=lhs, rhs=rhs)
        if self.x2.get_num_of_equalities() > 0:
            A_eq = self.x2.get_A_eq()
            lhs = sparse.coo_matrix((A_eq.data, (A_eq.row, A_eq.col + col_offset)))
            rhs = self.x2.get_b_eq()
            self.add_equality_from_sparse(equality_idx=get_x_ineq_idx(self.K), lhs=lhs, rhs=rhs)

    def copy(self):
        new_model = copy(self)
        new_model.lb = np.array(new_model.lb)
        new_model.ub = np.array(new_model.ub)
        new_model.c = np.array(new_model.c)
        new_model.A_ub_V = np.array(new_model.A_ub_V)
        new_model.A_ub_I = np.array(new_model.A_ub_I)
        new_model.A_ub_J = np.array(new_model.A_ub_J)
        new_model.b_ub_V = np.array(new_model.b_ub_V)
        new_model.b_ub_I = np.array(new_model.b_ub_I)
        new_model.A_eq_V = np.array(new_model.A_eq_V)
        new_model.A_eq_I = np.array(new_model.A_eq_I)
        new_model.A_eq_J = np.array(new_model.A_eq_J)
        new_model.b_eq_V = np.array(new_model.b_eq_V)
        new_model.b_eq_I = np.array(new_model.b_eq_I)
        new_model.inequality_idx_lhs_range = np.array(new_model.inequality_idx_lhs_range)
        new_model.inequality_idx_rhs_range = np.array(new_model.inequality_idx_rhs_range)
        new_model.equality_idx_lhs_range = np.array(new_model.equality_idx_lhs_range)
        new_model.equality_idx_rhs_range = np.array(new_model.equality_idx_rhs_range)
        new_model.clear_solution()
        return new_model

    def __str__(self):
        from idstl.Context import TOLERANCE
        my_str = 'min ' + str.join(' + ', [self.__j_to_var_str(j)
                                           for j in range(self.num_of_vars) if np.abs(self.c[j]) > TOLERANCE]) + '\n'
        my_str += 'subject to \n'
        for idx in range(self.equality_idx_lhs_range.size):
            lhs_range = self.equality_idx_lhs_range[idx]
            rhs_range = self.equality_idx_rhs_range[idx]
            if isinstance(lhs_range, range):
                assert isinstance(rhs_range, range)
                my_str += 'equality ' + str(idx) + ':\n'
                my_str += self.__const_str(lhs_V=self.A_eq_V[lhs_range],
                                           lhs_I=self.A_eq_I[lhs_range],
                                           lhs_J=self.A_eq_J[lhs_range],
                                           sense='==',
                                           rhs_V=self.b_eq_V[rhs_range],
                                           rhs_I=self.b_eq_I[rhs_range])
        for idx in range(self.inequality_idx_lhs_range.size):
            lhs_range = self.inequality_idx_lhs_range[idx]
            rhs_range = self.inequality_idx_rhs_range[idx]
            if isinstance(lhs_range, range):
                assert isinstance(rhs_range, range)
                my_str += 'inequality ' + str(idx) + ':\n'
                my_str += self.__const_str(lhs_V=self.A_ub_V[lhs_range],
                                           lhs_I=self.A_ub_I[lhs_range],
                                           lhs_J=self.A_ub_J[lhs_range],
                                           sense='<=',
                                           rhs_V=self.b_ub_V[rhs_range],
                                           rhs_I=self.b_ub_I[rhs_range])
        for j in range(self.num_of_vars):
            if self.lb[j] > -INFINITY and self.ub[j] < INFINITY:
                my_str += str(np.round(self.lb[j], 2)) + ' <= ' + self.__j_to_var_str(j) + ' <= ' \
                          + str(np.round(self.ub[j], 2)) + '\n'
            elif self.lb[j] > -INFINITY:
                my_str += str(np.round(self.lb[j], 2)) + ' <= ' + self.__j_to_var_str(j) + '\n'
            elif self.ub[j] < INFINITY:
                my_str += self.__j_to_var_str(j) + ' <= ' + str(np.round(self.ub[j], 2)) + '\n'
            else:
                my_str += self.__j_to_var_str(j) + ' free\n'
        if self.is_optimal:
            my_str += '\n = ' + str(np.round(self.objVal, 3))
        return my_str

    def __const_str(self, lhs_V: np.ndarray, lhs_I: np.ndarray, lhs_J: np.ndarray, sense: str,
                    rhs_V: np.ndarray, rhs_I: np.ndarray):
        rows = np.sort(np.unique(np.hstack((lhs_I, rhs_I))))
        num_of_rows = rows.size
        if num_of_rows > 0:
            lhs_str_arr = [np.array([], dtype=np.object) for _ in range(num_of_rows)]
            rhs_str = ['0' for _ in range(num_of_rows)]
            j_asc = np.argsort(lhs_J)
            for v, i, j in zip(lhs_V[j_asc], lhs_I[j_asc], lhs_J[j_asc]):
                v_str = '(' + str(np.round(v, 2)) + ' * ' + self.__j_to_var_str(j) + ')'
                row = np.searchsorted(rows, i)
                lhs_str_arr[row] = np.append(lhs_str_arr[row], v_str)
            for v, i in zip(rhs_V, rhs_I):
                row = np.searchsorted(rows, i)
                rhs_str[row] = str(np.round(v, 2))
            return str.join('\n', np.array([str.join(' + ', lhs_str_arr[i]) + ' ' + sense + ' ' + rhs_str[i]
                                            for i in range(num_of_rows)])) + '\n'
        return ''

    def __j_to_var_str(self, j: int):
        v_str = None
        v_idx = int((j - 1) % (self.n + self.m))
        k = int(np.floor((j - 1) / (self.n + self.m)))
        if j == 0:
            v_str = 's'
        elif not self.is_initialized and v_idx < self.n:
            v_str = 'x' + str(v_idx) + '_' + str(k)
        elif self.is_initialized and self.m <= v_idx:
            v_str = 'x' + str(v_idx - self.m) + '_' + str(k + 1)
        elif not self.is_initialized and self.n <= v_idx:
            v_str = 'u' + str(v_idx - self.n) + '_' + str(k)
        elif self.is_initialized and v_idx < self.m:
            v_str = 'u' + str(v_idx) + '_' + str(k)
        return v_str

    def get_v_ind(self, k: int) -> range:
        """
        if is initialized then

            X = [s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        else

            X = [x_0,s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        where:
        * s_k is an slack variable (R)
        * v_k is a vector of proxy slack variables (R^n)
        * u_k is a vector of input variables (R^m)
        * x_k is a vector of state variables (R^n)

        Therefore, :
        * if is initialized, v_ind.start = 1 + n + m + (k-1) * (n + 1 + n + m) + n + 1
        = 1 + (k - 1) * (n + 1 + n + m) + (n + 1 + n + m) = 1 + k * (n + 1 + n + m)
        * else, v_ind.start = k * (n + 1 + n + m) + n + 1
        :param k:
        :return: v_ind as range : v_k = X[v_ind]
        """
        if self.is_initialized:
            start = k * (2 * self.n + 1 + self.m) + 1
        else:
            start = k * (2 * self.n + 1 + self.m) + self.n + 1
        return range(start, start + self.n)

    def get_x_ind(self, k: int) -> range:
        """
        if is initialized then

            X = [s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        else

            X = [x_0,s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        where:
        * s_k is an slack variable (R)
        * v_k is a vector of proxy slack variables (R^n)
        * u_k is a vector of input variables (R^m)
        * x_k is a vector of state variables (R^n)

        Therefore, :
        * if k == 0 and is initialized, error!,
        * if k > 0 and is initialized, x_ind.start = 1 + n + m + (k-1) * (n + 1 + n + m) = k * (n + 1 + n + m) - n
        * else, x_ind.start = k * (n + 1 + n + m)
        :param k:
        :return: x_ind as range : x_k = X[x_ind]
        """
        if self.is_initialized:
            if k == 0:
                raise IndexError
            else:
                start = k * (2 * self.n + 1 + self.m) - self.n
        else:
            start = k * (2 * self.n + 1 + self.m)
        return range(start, start + self.n)

    def get_u_ind(self, k: int) -> range:
        """
        if is initialized then

            X = [s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        else

            X = [x_0,s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        where:
        * s_k is an slack variable (R)
        * v_k is a vector of proxy slack variables (R^n)
        * u_k is a vector of input variables (R^m)
        * x_k is a vector of state variables (R^n)

        Therefore, :
        * if is initialized, u_ind.start = 1 + n + m + (k-1) * (n + 1 + n + m) + n + 1 + n =
        = k * (n + 1 + n + m) + 1 + n
        * else, x_ind.start = k * (n + 1 + n + m) + 1 + 2 * n
        :param k:
        :return: x_ind as range : x_k = X[x_ind]
        """
        if self.is_initialized:
            if k == 0:
                raise IndexError
            else:
                start = k * (2 * self.n + 1 + self.m) + 1 + self.n
        else:
            start = k * (2 * self.n + 1 + self.m) + 1 + 2 * self.n
        return range(start, start + self.m)

    def get_s_ind(self, k: int) -> range:
        """
        if is initialized then

            X = [s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        else

            X = [x_0,s_0,v_0,u_0,x_1,s_1,v_1,u_1,...,v_K-1,u_K-1,x_K,s_K]

        where:
        * s_k is an slack variable (R)
        * v_k is a vector of proxy slack variables (R^n)
        * u_k is a vector of input variables (R^m)
        * x_k is a vector of state variables (R^n)

        Therefore, :
        * if is initialized, s_ind.start = 1 + n + m + (k-1) * (n + 1 + n + m) + n =
        = k * (n + 1 + n + m)
        * else, x_ind.start = k * (n + 1 + n + m) + n
        :param k:
        :return: x_ind as range : x_k = X[x_ind]
        """
        if self.is_initialized:
            if k == 0:
                raise IndexError
            else:
                start = k * (2 * self.n + 1 + self.m)
        else:
            start = k * (2 * self.n + 1 + self.m) + self.n
        return range(start, start + 1)

    def mk_loop_const(self, L: int = None):
        if L is not None and 0 < L <= self.K:
            assert self.is_initialized
            self.add_equality(
                equality_idx=-1,
                lhs_V=np.hstack((np.ones(self.n), -np.ones(self.n))),
                lhs_I=np.hstack((np.arange(self.n), np.arange(self.n))),
                lhs_J=np.hstack((np.array(self.get_x_ind(self.K)), np.array(self.get_x_ind(L - 1)))),
                rhs_V=np.zeros(0),
                rhs_I=np.zeros(0)
            )

    @staticmethod
    def merge(a, b):
        """
        Merge removing a.x_K, b.x_0, b.u_0 and b.s,
        because a.x_{K-1} = b.x_0 and a.x_K = b.x_1.

        :param a:
        :param b:
        :return:
        """
        if b is not None:
            assert isinstance(a, DistanceModel)
            assert isinstance(b, DistanceModel)
            new_model = a.copy()
            # a.[s, x(0), u(0), ..., x(K-1), u(K-1)] + b.[x(1), u(1), ..., u(K-1), x(K)]
            # remove a.[x(K)] and b.[s, x(0), u(0)] = 2 * n + m + 1
            j_offset = new_model.num_of_vars - 2 * new_model.n - new_model.m - 1
            new_model.num_of_vars += b.num_of_vars - 2 * new_model.n - new_model.m - 1
            new_model.c = np.hstack((new_model.c, b.c[2 * new_model.n + new_model.m + 1:]))
            new_model.lb = np.hstack((new_model.lb[:-new_model.n], b.lb[1 + b.n + b.m:]))
            new_model.ub = np.hstack((new_model.ub[:-new_model.n], b.ub[1 + b.n + b.m:]))
            # concatenate(b.[xe(0) xe(1) xe(2)],
            #             new_model.[x_ub(0) u_ub(0) ... x_ub(K-1) u_ub(K-1)],
            #             b.[x_ub(1) u_ub(1) ...x_ub(K-1) u_ub(K-1)])
            new_model.del_inequality(0)
            new_model.del_inequality(1)
            new_model.del_inequality(2)
            ineq_idx_offset = get_u_ineq_idx(new_model.K - 1) - 3
            for inequality_idx in range(b.inequality_idx_lhs_range.size):
                lhs_range = b.inequality_idx_lhs_range[inequality_idx]
                rhs_range = b.inequality_idx_rhs_range[inequality_idx]
                if inequality_idx <= 2 and isinstance(lhs_range, range):
                    new_model.add_inequality(
                        inequality_idx=inequality_idx,
                        lhs_V=b.A_ub_V[lhs_range],
                        lhs_I=b.A_ub_I[lhs_range],
                        lhs_J=np.array([0 if b.A_ub_J[i] == 0 else b.A_ub_J[i] + j_offset for i in lhs_range]),
                        rhs_V=b.b_ub_V[rhs_range],
                        rhs_I=b.b_ub_I[rhs_range],
                    )
                elif inequality_idx >= get_x_ineq_idx(1) and isinstance(lhs_range, range):
                    new_model.add_inequality(
                        inequality_idx=inequality_idx + ineq_idx_offset,
                        lhs_V=b.A_ub_V[lhs_range],
                        lhs_I=b.A_ub_I[lhs_range],
                        lhs_J=b.A_ub_J[lhs_range] + j_offset,
                        rhs_V=b.b_ub_V[rhs_range],
                        rhs_I=b.b_ub_I[rhs_range],
                    )
                # concatenate(new_model.[x_eq(0) u_eq(0) d_eq(0) ... x_eq(K-1) u_eq(K-1) d_eq(K-1)],
                #             b.[x_eq(1) u_ub(1) d_eq(1) ...x_ub(K-1) u_ub(K-1) d_eq(K-1)])
                eq_idx_offset = get_d_eq_idx(new_model.K - 1)
                for equality_idx in range(b.equality_idx_lhs_range.size):
                    lhs_range = b.equality_idx_lhs_range[equality_idx]
                    rhs_range = b.equality_idx_rhs_range[equality_idx]
                    if equality_idx >= get_x_eq_idx(1) and isinstance(lhs_range, range):
                        new_model.add_equality(
                            equality_idx=equality_idx + eq_idx_offset,
                            lhs_V=b.A_eq_V[lhs_range],
                            lhs_I=b.A_eq_I[lhs_range],
                            lhs_J=b.A_eq_J[lhs_range] + j_offset,
                            rhs_V=b.b_eq_V[rhs_range],
                            rhs_I=b.b_eq_I[rhs_range],
                        )
            new_model.K += b.K - 2
            return new_model
