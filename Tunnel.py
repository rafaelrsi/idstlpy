from typing import Union
from idstl.TransitionSystem import TransitionSystem
from idstl.Formula import Formula
from copy import deepcopy
import numpy as np
from idstl.TunnelModel import TunnelModel
from idstl.Transition import Transition


class Tunnel:
    def __init__(self, transition_system: TransitionSystem = None, 
                 delta: Union[int, float] = None, 
                 s_bar: Union[int, float] = None, 
                 seq_of_partitions_idx: np.ndarray = None,
                 seq_of_symbol_idx: np.ndarray = None,
                 formula: Formula = None,
                 seq_of_formula_eval: np.ndarray = None,
                 loop_initial_idx: int = None,
                 parent: 'Tunnel' = None,
                 child_repeats_idx: int = None):
        from idstl.Context import Context
        if parent is None:
            assert transition_system is not None
            assert seq_of_partitions_idx is not None
            assert seq_of_symbol_idx is not None
            assert formula is not None
            assert seq_of_formula_eval is not None
            assert loop_initial_idx is not None
            assert child_repeats_idx is None
            self.child_repeats_idx = 0
            self.ts = transition_system
            self.root = None
            self.parent = None
            self.seq_of_partition_idx = seq_of_partitions_idx
            self.seq_of_symbol_idx = seq_of_symbol_idx
            dimension = transition_system.get_dimension()
            num_of_inputs = transition_system.get_num_of_inputs()
            initial_state = transition_system.get_initial_state()
            initial_invariant = transition_system.get_initial_partition().get_polytope()
            self.tunnel_model = TunnelModel(dimension, num_of_inputs, delta, s_bar, 
                                            initial_state, initial_invariant,
                                            loop_initial_idx, formula, seq_of_formula_eval)
            for step in range(self.seq_of_partition_idx.size - 1):
                partition_from_idx = self.seq_of_partition_idx[step]
                symbol_idx = self.seq_of_symbol_idx[step]
                partition_to_idx = self.seq_of_partition_idx[step + 1]
                transition = transition_system.get_transition_from_idx(partition_from_idx, symbol_idx, partition_to_idx)
                assert isinstance(transition, Transition)
                target = transition_system.get_partition_from_idx(partition_to_idx)
                self_loop = transition_system.get_transition_from_idx(partition_from_idx, 0, partition_from_idx)
                assert isinstance(self_loop, Transition)
                self.tunnel_model.append_transition(transition.get_system(), 
                                                    target.get_polytope(), 
                                                    self_loop.get_system())
        else:
            assert transition_system is None
            assert seq_of_partitions_idx is None
            assert seq_of_symbol_idx is None
            assert formula is None
            assert seq_of_formula_eval is None
            assert loop_initial_idx is None
            assert child_repeats_idx is not None
            self.child_repeats_idx = child_repeats_idx
            self.parent = parent
            self.root = self.parent.root
            self.ts = parent.ts
            self.seq_of_partition_idx = self.parent.seq_of_partition_idx
            self.seq_of_symbol_idx = self.parent.seq_of_symbol_idx
            self.tunnel_model = deepcopy(parent.tunnel_model)  # type: TunnelModel
            self.tunnel_model.add_self_loop_at(child_repeats_idx)
        step = self.child_repeats_idx
        Context.add_lp_elapsed_time(self.tunnel_model.zero_prefix(step))
        assert self.tunnel_model.is_optimal
        while step < self.tunnel_model.num_of_steps - 1 and self.tunnel_model.objVal == 0:
            step += 1
            Context.add_lp_elapsed_time(self.tunnel_model.zero_prefix(step))
            assert self.tunnel_model.is_optimal
        self.zero_prefix_step = step
        self.distance = self.tunnel_model.objVal

    def __len__(self):
        return self.tunnel_model.num_of_steps

    def __eq__(self, other):
        return self.seq_of_partition_idx == other.seq_of_partition_idx

    def __lt__(self, other):
        assert not self.is_infeasible() and not self.is_infeasible()
        are_equivalent = len(other) == len(self)
        if not are_equivalent:
            raise NotImplementedError
        if np.all(np.equal(self.tunnel_model.num_of_self_loops, other.tunnel_model.num_of_self_loops)):
            return False
        if self.zero_prefix_step > other.zero_prefix_step:
            return True
        if self.zero_prefix_step == other.zero_prefix_step and self.distance < other.distance:
            return True
        if self.zero_prefix_step == other.zero_prefix_step and self.distance == other.distance:
            for i in range(self.zero_prefix_step, -1, -1):
                if self.tunnel_model.num_of_self_loops[i] > other.tunnel_model.num_of_self_loops[i]:
                    return True
                elif self.tunnel_model.num_of_self_loops[i] < other.tunnel_model.num_of_self_loops[i]:
                    return False
        return False

    def __str__(self):
        return str(self.tunnel_model)

    def repeat_at(self, idx: int):
        assert 0 <= idx < len(self)
        return Tunnel(parent=self, child_repeats_idx=idx)

    def get_context(self):
        return self.ts.get_context()

    def get_trajectory(self):
        return self.tunnel_model.get_trajectory()

    def get_path(self):
        return np.hstack((
            np.repeat(self.seq_of_partition_idx[:-1], self.tunnel_model.num_of_self_loops.astype(int)),
            self.seq_of_partition_idx[-1]
        ))

    def is_feasible(self):
        return self.zero_prefix_step == self.tunnel_model.num_of_steps - 1 and self.distance == 0.0

    def is_infeasible(self):
        return self.tunnel_model.is_unsat()

    def is_sat(self):
        return self.tunnel_model.is_sat()

    def is_root(self):
        return self.parent is None
