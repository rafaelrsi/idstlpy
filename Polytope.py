from typing import Union
import numpy as np
from scipy import sparse
from scipy.spatial import ConvexHull, Delaunay
from idstl.LPModel import INFINITY, LPModel


class Polytope:
    def __init__(self, dimension: int,
                 vertices: Union[list, np.ndarray, sparse.spmatrix] = None,
                 A_ub: Union[list, np.ndarray, sparse.spmatrix] = None,
                 b_ub: Union[list, np.ndarray, sparse.spmatrix] = None,
                 A_eq: Union[list, np.ndarray, sparse.spmatrix] = None,
                 b_eq: Union[list, np.ndarray, sparse.spmatrix] = None):
        """
        Create a polytope P of the form:

        P := {x in R^dimension : A_ub x <= b_ub, A_eq x == b_eq} such that for all x in P, lb <= x <= ub

        or a full-dimensional polytope P of the form:

        P := {x in R^dimension : A_ub x <= b_ub} == conv(vertices) such that for all x in P, lb <= x <= ub


        --------

        Usage:

        Polytope(dimension, vertices): It returns a full-dimensional polytope. [Requires number of vertices greater than
        dimension]

        Polytope(dimension[, vertices], A_ub, b_ub[, A_eq, b_eq]): Returns a polytope
        (not necessarily full-dimensional).


        :param dimension: positive integer
        :param vertices: array of points of same dimension [vertices.shape[1] == dimension]
        :param A_ub: matrix (num_of_ineq x dimension)
        :param b_ub: array (num_of_ineq)
        :param A_eq: matrix (num_of_eq x dimension)
        :param b_eq: array (num_of_eq)
        """
        TOLERANCE = 1e-6
        assert isinstance(dimension, int) and dimension > 0
        lb = -INFINITY * np.ones(dimension)
        ub = INFINITY * np.ones(dimension)
        if vertices is not None and len(vertices) > dimension:
            if not isinstance(vertices, np.ndarray):
                vertices = np.array(vertices)
            assert vertices.ndim == 2 and vertices.shape[1] == dimension
            if dimension == 1:
                A_ub = np.array([[-1], [1]])
                b_ub = np.array([-np.amin(vertices), np.amax(vertices)])
                vertices = np.array([[np.amin(vertices)], [np.amax(vertices)]])
                lb = np.amin(vertices)
                ub = np.amax(vertices)
            elif vertices.shape[0] > dimension:
                hull = ConvexHull(vertices)
                H = hull.equations
                i = 0
                while i < H.shape[0]:
                    j = i + 1
                    while j < H.shape[0]:
                        if np.linalg.norm(H[i, :] - H[j, :]) <= TOLERANCE:
                            H = np.delete(H, j, axis=0)
                        else:
                            j += 1
                    i += 1
                A_ub = H[:, :dimension]
                b_ub = -np.reshape(H[:, dimension], H.shape[0])
                vertices = np.array(vertices[hull.vertices, :])
                lb = np.reshape(hull.min_bound, hull.min_bound.size)
                ub = np.reshape(hull.max_bound, hull.max_bound.size)
        else:
            if A_ub is not None:
                assert b_ub is not None
                if isinstance(A_ub, list):
                    A_ub = np.array(A_ub)
                if isinstance(b_ub, list):
                    b_ub = np.array(b_ub)
                if isinstance(b_ub, np.ndarray) and b_ub.ndim == 1:
                    b_ub = np.reshape(np.array(b_ub), (len(b_ub), 1))
                if not isinstance(A_ub, sparse.coo_matrix):
                    A_ub = sparse.coo_matrix(A_ub)
                if not isinstance(b_ub, sparse.coo_matrix):
                    b_ub = sparse.coo_matrix(b_ub)
                assert A_ub.ndim == 2 and A_ub.shape[1] == dimension, "Invalid halfspace dimension"
                assert b_ub.ndim == 2 and A_ub.shape[0] == b_ub.shape[0] and b_ub.shape[1] == 1, \
                    "Invalid halfspace dimension"
            if A_eq is not None:
                assert b_eq is not None
                if isinstance(A_eq, list):
                    A_eq = np.array(A_eq)
                if isinstance(b_eq, list):
                    b_eq = np.array(b_eq)
                if isinstance(b_eq, np.ndarray) and b_eq.ndim == 1:
                    b_eq = np.reshape(np.array(b_eq), (len(b_eq), 1))
                if not isinstance(A_eq, sparse.coo_matrix):
                    A_eq = sparse.coo_matrix(A_eq)
                if not isinstance(b_eq, sparse.coo_matrix):
                    b_eq = sparse.coo_matrix(b_eq)
                assert A_eq.ndim == 2 and A_eq.shape[1] == dimension, "Invalid halfspace dimension"
                assert b_eq.ndim == 2 and A_eq.shape[0] == b_eq.shape[0] and b_eq.shape[1] == 1, \
                    "Invalid halfspace dimension"
            model = LPModel(num_of_vars=dimension, lb=lb, ub=ub)
            if A_ub is not None:
                model.add_inequality(inequality_idx=0, lhs_V=A_ub.data, lhs_I=A_ub.row, lhs_J=A_ub.col,
                                     rhs_V=b_ub.data, rhs_I=b_ub.row)
            if A_eq is not None:
                model.add_equality(equality_idx=0, lhs_V=A_eq.data, lhs_I=A_eq.row, lhs_J=A_eq.col,
                                   rhs_V=b_eq.data, rhs_I=b_eq.row)
            for i in range(dimension):
                model.set_objective(1, i)
                model.solve()
                if model.is_optimal:
                    lb[i] = model.get_objective()
                model.set_objective(-1, i)
                model.solve()
                if model.is_optimal:
                    ub[i] = -model.get_objective()
                model.set_objective(0, i)
            if A_ub is not None:
                A_ub = A_ub.toarray()
                b_ub = b_ub.toarray()
            if A_eq is not None:
                A_eq = A_eq.toarray()
                b_eq = b_eq.toarray()
        if A_ub is not None:
            A_norm = np.array([[np.dot(h, h)] for h in A_ub])
            A_ub = A_ub / A_norm
            b_ub = np.array([b_ub[i] / A_norm[i, 0] for i in range(len(b_ub))])
            A_ub[np.abs(A_ub) <= TOLERANCE] = 0
            b_ub[np.abs(b_ub) <= TOLERANCE] = 0
        if A_eq is not None:
            A_norm = np.array([[np.dot(h, h)] for h in A_eq])
            A_eq = A_eq / A_norm
            b_ub = np.array([b_eq[i] / A_norm[i, 0] for i in range(len(b_eq))])
            A_eq[np.abs(A_eq) <= TOLERANCE] = 0
            b_ub[np.abs(b_eq) <= TOLERANCE] = 0
        self.__dimension = dimension
        self.__A_ub = None
        self.__b_ub = None
        self.__A_eq = None
        self.__b_eq = None
        if A_ub is not None:
            self.__A_ub = sparse.coo_matrix(A_ub)
            self.__b_ub = sparse.coo_matrix(np.reshape(b_ub, (b_ub.size, 1)))
        if A_eq is not None:
            self.__A_eq = sparse.coo_matrix(A_eq)
            self.__b_eq = sparse.coo_matrix(np.reshape(b_eq, (b_eq.size, 1)))
        self.__vertices = vertices
        self.__lb = lb
        self.__ub = ub

    def __contains__(self, item):
        x = np.reshape(np.array(item).flatten(), (self.get_dimension(), 1))
        contains = self.get_num_of_inequalities() > 0 or self.get_num_of_equalities() > 0
        if self.get_num_of_inequalities() > 0:
            A_ub = self.get_A_ub().toarray()
            b_ub = self.get_b_ub().toarray()
            contains = contains and all(np.dot(A_ub, x) <= b_ub)
        if self.get_num_of_equalities() > 0:
            A_eq = self.get_A_eq().toarray()
            b_eq = self.get_b_eq().toarray()
            contains = contains and all(np.dot(A_eq, x) <= b_eq)
        return contains

    def get_dimension(self) -> int:
        return self.__dimension

    def get_num_of_inequalities(self) -> int:
        if self.__A_ub is None:
            return 0
        return self.__A_ub.shape[0]

    def get_A_ub(self):
        return self.__A_ub

    def get_b_ub(self):
        return self.__b_ub

    def get_num_of_equalities(self) -> int:
        if self.__A_eq is None:
            return 0
        return self.__A_eq.shape[0]

    def get_A_eq(self):
        return self.__A_eq

    def get_b_eq(self):
        return self.__b_eq

    def get_vertices(self, axis=None):
        if axis is None:
            return self.__vertices
        v = self.__vertices[:, axis]
        hull = ConvexHull(v)
        return v[hull.vertices, :]

    def get_num_of_vertices(self):
        if self.__vertices is None:
            return 0
        return int(self.__vertices.size)

    def is_full_dimensional(self):
        return self.get_num_of_vertices() > self.get_dimension()

    def get_lb(self, idx=None):
        if idx is None:
            return self.__lb
        return float(self.__lb[idx])

    def get_ub(self, idx=None):
        if idx is None:
            return self.__ub
        return float(self.__ub[id])

    def get_centroid(self, axis: Union[list, np.ndarray] = None):
        """
        We compute a centroid of a polytope using simplices center of mass, as presented by Kleder (2020).  
        
        Michael Kleder (2020). Centroid of a Convex n-Dimensional Polyhedron 
        (https://www.mathworks.com/matlabcentral/fileexchange/8514-centroid-of-a-convex-n-dimensional-polyhedron), 
        MATLAB Central File Exchange. Retrieved October 23, 2020.
        
        :param axis: 
        :return: 
        """
        assert self.get_num_of_vertices() > 0
        vertices = self.get_vertices(axis)
        dimension = vertices.shape[1]
        if vertices.shape[0] > dimension >= 2:
            centroid = np.zeros(dimension)
            tri = Delaunay(vertices)
            num_of_triangles = len(tri.simplices)
            simplices_volume = np.zeros(num_of_triangles)
            for i in range(num_of_triangles):
                simplice_vertices = vertices[tri.simplices[i, :]]
                simplice_hull = ConvexHull(simplice_vertices)
                simplices_volume[i] = simplice_hull.volume
                centroid += simplices_volume[i] * np.mean(simplice_vertices, axis=0)
            centroid /= sum(simplices_volume)
        else:
            centroid = np.mean(vertices, axis=0)
        return centroid


if __name__ == '__main__':
    import idstl
    P = idstl.Polytope(2, A_ub=[[1, 0], [-1, 0], [0, 1], [0, -1]], b_ub=[1, 0, 1, 0])
    print(P.get_lb())
    print(P.get_ub())
