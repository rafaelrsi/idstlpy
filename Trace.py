import numpy as np
from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release


class Trace:
    def __init__(self,
                 formula: Formula,
                 formula_eval: np.ndarray,
                 num_of_repetitions: np.ndarray,
                 clock: np.ndarray,
                 loop_idx: int):
        assert isinstance(formula, Formula)
        assert isinstance(formula_eval, np.ndarray)
        assert isinstance(num_of_repetitions, np.ndarray)
        assert isinstance(clock, np.ndarray)
        assert isinstance(loop_idx, int)
        assert formula_eval.ndim == 2
        assert num_of_repetitions.ndim == 1
        assert clock.ndim == 1
        assert formula_eval.shape[0] == len(formula) and formula_eval.shape[1] == num_of_repetitions.size
        assert clock.size == num_of_repetitions.size + 1
        assert clock[0] == 0
        self.__phi = formula
        self.__phi_eval = formula_eval
        self.__phi_clock_ub = formula.get_upper_time_bound()
        self.__num_of_repetitions = num_of_repetitions
        self.__clock = clock
        self.__loop_idx = loop_idx
        self.__trace = None
        self.__is_sat = None

    def __str__(self):
        from idstl.Formula import AtomicProposition
        if self.__trace is None:
            steps = np.arange(self.get_num_of_steps())
            trace = np.zeros(self.get_num_of_steps(), dtype=np.object)
            for phi in self.__phi.get_closure():
                if isinstance(phi, AtomicProposition):
                    phi_idx = phi.get_idx()
                    for i in steps[self.__phi_eval[phi_idx, :]]:
                        if not isinstance(trace[i], np.ndarray):
                            trace[i] = np.array([str(phi)], dtype=np.object)
                        else:
                            trace[i] = np.append(trace[i], str(phi))
            for i in range(trace.size):
                if not isinstance(trace[i], np.ndarray):
                    trace[i] = '{ }^' + str(int(self.__num_of_repetitions[i]))
                else:
                    trace[i] = '{' + str.join(', ', trace[i]) + '}^' + str(int(self.__num_of_repetitions[i]))
            if 0 < self.__loop_idx < self.get_num_of_steps():
                trace = str.join(' ', trace[:self.__loop_idx]) + ' ( ' \
                        + str.join(' ', trace[self.__loop_idx:]) + ' )^w'
            else:
                trace = str.join(' ', trace)
            self.__trace = trace
        else:
            trace = self.__trace
        return str(trace)

    def __len__(self):
        return int(self.__clock[-1])

    def get_bound(self) -> int:
        return len(self) - 1

    def get_num_of_steps(self):
        return self.__num_of_repetitions.size

    def get_loop_ini(self) -> int:
        if 0 < self.__loop_idx < self.__num_of_repetitions.size:
            return self.get_clock_lb(self.__loop_idx)
        return self.get_clock_ub(self.__num_of_repetitions.size - 1)

    def get_clock_lb(self, idx: int) -> int:
        return self.__clock[idx]

    def get_clock_ub(self, idx: int) -> int:
        return self.__clock[idx + 1]

    def get_formula(self) -> Formula:
        return self.__phi

    def get_formula_eval(self) -> np.ndarray:
        return self.__phi_eval

    def get_formula_upper_time_bound(self) -> int:
        return self.__phi_clock_ub

    def is_sat(self) -> bool:
        if self.__is_sat is None:
            self.__is_sat = self.check(self.__phi, 0)
        return self.__is_sat

    def __check_atomic_proposition(self, formula: AtomicProposition, idx: int) -> (bool, bool):
        return self.__phi_eval[formula.get_idx(), idx] is True

    def __check_not(self, formula: Not, idx: int) -> (bool, bool):
        return self.__phi_eval[formula.get_idx(), idx] is True \
               and self.__check_atomic_proposition(formula.get_child(), idx) is False

    def __check_and(self, formula: And, idx: int) -> bool:
        return self.__phi_eval[formula.get_idx(), idx] is True \
               and all([self.check(formula.get_child(child_idx), idx)
                        for child_idx in range(formula.get_num_of_children())])

    def __check_or(self, formula: Or, idx: int) -> bool:
        return self.__phi_eval[formula.get_idx(), idx] is True \
               and any([self.check(formula.get_child(child_idx), idx)
                        for child_idx in range(formula.get_num_of_children())])

    def __check_until(self, formula: Until, idx: int) -> bool:
        """

        Let trace = L(q_0)L(q_1)...L(q_{L-1}) ( L(q_L)...L(q_K) )^w

        if k < L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_{L-1}) ( L(q_L)...L(q_K) )^w,

        if k > L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_K) ( L(q_{L + K + 1 - k})...L(q_{2 * K + 1 - k}) )^w

        --------------


        (K,L)-trace semantics (i.e., L(q_k)L(q_{k+1})...L(q_{L-1}) ( L(q_L)...L(q_K) )^w:


        Suffix(k, trace) [a,b] satisfies phi_1 U[a,b] phi_2 iff:

        * there exists kp in [k..K] AND [k + a..k + b] such that Suffix(kp, trace) [a,b] satisfies phi_2 
        and for all kpp in [k..kp], Suffix(kpp, trace) [a,b] satisfies phi_1; or,
        * there exists kp in [L..K] such that Suffix(kp, trace) [a,b] satisfies phi_2 and for all kpp in [k..K], 
        Suffix(kpp, trace) [a,b] satisfies phi_1; or,


        :param formula:
        :param idx:
        :return: Suffix(k, trace) satisfies phi
        """
        if self.__phi_eval[formula.get_idx(), idx] is True:
            phi_1 = formula.get_phi_1()
            phi_2 = formula.get_phi_2()
            a, b = formula.get_interval()
            try:
                ip = next(i for i in range(self.get_num_of_steps()) if self.check(phi_2, i) is True)
            except StopIteration:
                return False
            if (self.get_clock_lb(idx) + a <= self.get_clock_ub(ip)
                    and self.get_clock_lb(ip) <= self.get_clock_ub(idx) + b):
                return all([self.check(phi_1, i) for i in range(idx, ip)])
            return all([self.check(phi_1, i) for i in range(idx, self.get_num_of_steps())])
        return False

    def __check_release(self, formula: Release, idx: int) -> bool:
        """

        Let trace = L(q_0)L(q_1)...L(q_{L-1}) ( L(q_L)...L(q_K) )^w

        if k < L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_{L-1}) ( L(q_L)...L(q_K) )^w,

        if k >= L Suffix(k, trace) :=

        *  L(q_k)L(q_{k+1})...L(q_K) ( L(q_{L + K + 1 - k})...L(q_{2 * K + 1 - k}) )^w

        ---------------

        (K,L)-trace semantics (i.e., Suffix(k, trace) = L(q_0)L(q_1)...L(q_{L-1}) ( L(q_L)...L(q_K) )^w:

        Suffix(k, trace) [a,b] satisfies phi iff:

        * there exists kp in [k + a..k + b] such that Suffix(kp, trace) [a,b] satisfies phi_1 and for all kpp 
        in [k + a..kp], Suffix(kpp, trace) [a,b] satisfies phi_2; or,
        * there exists kp in [L..K] such that Suffix(kp, trace) [a,b] satisfies phi_1 and for all kpp in [L..K], 
        Suffix(kpp, trace) [a,b] satisfies phi_2; or,
        * there exists NO kp in [k..K] such that Suffix(kp, trace) [a,b] satisfies phi_1, AND k + b <= K, AND 
        for all kpp in [k + a..k + b], Suffix(kpp, trace) [a,b] satisfies phi_2; or,
        * there exists NO kp in [k..K] such that Suffix(kp, trace) [a,b] satisfies phi_1, AND K < k + b, 
        AND b - a <= K - L, AND for all kpp in [L..L + K - k - b] OR [k + a..K], Suffix(kpp, trace) [a,b] 
        satisfies phi_2; or,
        * there exists NO kp in [k..K] such that Suffix(kp, trace) [a,b] satisfies phi_1, AND K < k + b, 
        AND b - a > K - L, AND for all kpp in [min(k + a, L)..K], Suffix(kpp, trace) [a,b] satisfies phi_2; or,


        :param formula:
        :param idx:
        :return: Suffix(k, trace) satisfies phi
        """
        if self.__phi_eval[formula.get_idx(), idx] is True:
            phi_1 = formula.get_phi_1()
            phi_2 = formula.get_phi_2()
            a, b = formula.get_interval()
            k = {'a': self.get_clock_lb(idx), 'b': self.get_clock_ub(idx)}
            try:
                ip = next(i for i in range(self.get_num_of_steps()) if self.check(phi_1, i) is True)
                if (self.get_clock_lb(idx) + a <= self.get_clock_ub(ip)
                        and self.get_clock_lb(ip) <= self.get_clock_ub(idx) + b):
                    return all([self.check(phi_2, i) for i in range(ip + 1)
                                if k['a'] + a < self.get_clock_ub(i) and self.get_clock_lb(i) < k['b'] + b])
            except StopIteration:
                if idx < self.__loop_idx:
                    L = {'a': self.get_bound() - k['b'], 'b': self.get_bound() - k['a']}
                    K = {'a': self.get_bound() - self.get_loop_ini() + L['a'],
                         'b': self.get_bound() - self.get_loop_ini() + L['b']}
                    if k['b'] + b <= K['a']:
                        # => k['b'] + a <= K['a']
                        return all([self.check(phi_2, i) for i in range(self.get_num_of_steps())
                                    if k['a'] + a < self.get_clock_ub(i) and self.get_clock_lb(i) < k['b'] + b])
                    elif b - a <= K['b'] - L['a']:
                        return all([self.check(phi_2, i) for i in range(self.get_num_of_steps())
                                    if ((self.__loop_idx <= i and self.get_clock_lb(i) < L['b'] + K['b'] - k['b'] - b)
                                        or k['a'] + a < self.get_clock_ub(i))])
                    else:
                        return all([self.check(phi_2, i) for i in range(self.get_num_of_steps())
                                    if self.__loop_idx <= i or k['a'] + a < self.get_clock_ub(i)])
            return all([self.check(phi_2, i) for i in range(self.__loop_idx, self.get_num_of_steps())])
        return False

    def check(self, formula: Formula, idx: int = 0) -> bool:
        ret_val = formula.get_value()
        if isinstance(formula, AtomicProposition):
            ret_val = self.__check_atomic_proposition(formula, idx)
        elif isinstance(formula, Not):
            ret_val = self.__check_not(formula, idx)
        elif isinstance(formula, And):
            ret_val = self.__check_and(formula, idx)
        elif isinstance(formula, Or):
            ret_val = self.__check_or(formula, idx)
        elif isinstance(formula, Until):
            ret_val = self.__check_until(formula, idx)
        elif isinstance(formula, Release):
            ret_val = self.__check_release(formula, idx)
        return ret_val
