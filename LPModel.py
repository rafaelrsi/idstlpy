import numpy as np
from scipy import sparse
from typing import Union
import gurobipy as gp
from gurobipy import GRB
import time

INFINITY = GRB.INFINITY


def solve_lp(c: np.ndarray, lb: np.ndarray, ub: np.ndarray,
             Aub=None, bub=None, Aeq=None, beq=None, env=None, require_optimal: bool = False):
    start = time.time()
    if env is None:
        env = gp.Env(empty=True)
        env.setParam('OutputFlag', 0)
        env.start()
    model = gp.Model(env=env)
    model.ModelSense = GRB.MINIMIZE
    x = gp.MVar(np.array([model.addVar(lb=lb[i], ub=ub[i], obj=c[i], vtype=GRB.CONTINUOUS,
                                       name='x' + str(i), column=None) for i in range(len(c))]))
    if Aub is not None:
        model.addMConstrs(A=Aub, x=x, sense='<=', b=bub)
    if Aeq is not None:
        model.addMConstrs(A=Aeq, x=x, sense='=', b=beq)
    model.optimize()
    is_optimal = model.status == GRB.OPTIMAL
    if is_optimal:
        x = np.array([v.x for v in model.getVars()])
        objVal = model.objVal
    else:
        x = None
        objVal = None
        if require_optimal:
            model.write('log.lp')
    end = time.time()
    elapsed_time = end - start
    return elapsed_time, is_optimal, objVal, x


class LPModel:
    """
    Solves a linear programming problem:

    min c' x

    subject to A_ub x <= b_ub, A_eq x == b_eq, lb <= x <= ub
    """

    env = None

    def __init__(self, num_of_vars: int, lb: np.ndarray, ub: np.ndarray):
        if LPModel.env is None:
            LPModel.env = gp.Env(empty=True)
            LPModel.env.setParam('OutputFlag', 0)
            LPModel.env.start()
        self.num_of_vars = num_of_vars
        self.lb = lb
        self.ub = ub
        self.c = np.zeros(num_of_vars)
        self.A_ub_V = np.array([])
        self.A_ub_I = np.array([], dtype=int)
        self.A_ub_J = np.array([], dtype=int)
        self.b_ub_V = np.array([])
        self.b_ub_I = np.array([], dtype=int)
        self.A_eq_V = np.array([])
        self.A_eq_I = np.array([], dtype=int)
        self.A_eq_J = np.array([], dtype=int)
        self.b_eq_V = np.array([])
        self.b_eq_I = np.array([], dtype=int)
        self.inequality_idx_lhs_range = np.array([], dtype=np.object)
        self.inequality_idx_rhs_range = np.array([], dtype=np.object)
        self.equality_idx_lhs_range = np.array([], dtype=np.object)
        self.equality_idx_rhs_range = np.array([], dtype=np.object)
        self.is_optimal = None
        self.objVal = None
        self.x = None

    def __str__(self):
        my_str = 'c = ' + str(self.c) + '\n'
        my_str += '[A_ub, b_ub] = \n' + str(np.hstack((self.get_A_ub().toarray(), self.get_b_ub().toarray()))) + '\n'
        my_str += '[A_eq, b_eq] = \n' + str(np.hstack((self.get_A_eq().toarray(), self.get_b_eq().toarray()))) + '\n'
        return my_str

    @property
    def num_of_inequalities(self):
        return self.inequality_idx_lhs_range.size

    @property
    def num_of_equalities(self):
        return self.equality_idx_lhs_range.size

    @property
    def num_of_ineq_rows(self):
        return int(np.max(np.hstack((self.A_ub_I, self.b_ub_I, -1))) + 1)

    @property
    def num_of_eq_rows(self):
        return int(np.max(np.hstack((self.A_eq_I, self.b_eq_I, -1))) + 1)

    def clear_solution(self):
        self.is_optimal = None
        self.objVal = None
        self.x = None

    def get_A_ub(self):
        return sparse.coo_matrix((self.A_ub_V.astype(float), (self.A_ub_I.astype(int), self.A_ub_J.astype(int))),
                                 shape=(self.num_of_ineq_rows, self.num_of_vars))

    def get_b_ub(self):
        return sparse.coo_matrix((self.b_ub_V.astype(float), (self.b_ub_I.astype(int), np.zeros(self.b_ub_V.shape))),
                                 shape=(self.num_of_ineq_rows, 1))

    def get_A_eq(self):
        return sparse.coo_matrix((self.A_eq_V.astype(float), (self.A_eq_I.astype(int), self.A_eq_J.astype(int))),
                                 shape=(self.num_of_eq_rows, self.num_of_vars))

    def get_b_eq(self):
        return sparse.coo_matrix((self.b_eq_V.astype(float), (self.b_eq_I.astype(int), np.zeros(self.b_eq_V.shape))),
                                 shape=(self.num_of_eq_rows, 1))

    def add_inequalities_from(self, list_of_inequalities: list):
        for set_of_A_ub, b_ub, inequality_idx in list_of_inequalities:
            b_ub = sparse.coo_matrix(b_ub)
            lhs_V = np.array([])
            lhs_I = np.array([])
            lhs_J = np.array([])
            for A_ub, j_offset in set_of_A_ub:
                A_ub = sparse.coo_matrix(A_ub)
                lhs_V = np.append(lhs_V, A_ub.data)
                lhs_I = np.append(lhs_I, A_ub.row)
                lhs_J = np.append(lhs_J, A_ub.col + j_offset)
            rhs_V = b_ub.data
            rhs_I = b_ub.row
            self.add_inequality(inequality_idx, lhs_V, lhs_I, lhs_J, rhs_V, rhs_I)
        self.clear_solution()

    def add_inequality_from_sparse(self, inequality_idx: int, lhs: sparse.coo_matrix, rhs: sparse.coo_matrix):
        return self.add_inequality(inequality_idx=inequality_idx,
                                   lhs_V=lhs.data, lhs_I=lhs.row, lhs_J=lhs.col,
                                   rhs_V=rhs.data, rhs_I=rhs.row)

    def add_inequality(self, inequality_idx: int, lhs_V: np.ndarray, lhs_I: np.ndarray, lhs_J: np.ndarray,
                       rhs_V: np.ndarray, rhs_I: np.ndarray):
        lhs_range = range(self.A_ub_V.size, self.A_ub_V.size + lhs_V.size)
        rhs_range = range(self.b_ub_V.size, self.b_ub_V.size + rhs_V.size)
        self.add_map_inequality(inequality_idx, lhs_range, rhs_range)
        start = self.num_of_ineq_rows
        self.A_ub_V = np.hstack((self.A_ub_V, lhs_V))
        self.A_ub_I = np.hstack((self.A_ub_I, lhs_I + start))
        self.A_ub_J = np.hstack((self.A_ub_J, lhs_J))
        self.b_ub_V = np.hstack((self.b_ub_V, rhs_V))
        self.b_ub_I = np.hstack((self.b_ub_I, rhs_I + start))
        return lhs_range, rhs_range

    def add_map_inequality(self, inequality_idx: int, lhs_range: range, rhs_range: range):
        from idstl.Context import add_map_idx_object
        self.inequality_idx_lhs_range = add_map_idx_object(self.inequality_idx_lhs_range, inequality_idx, lhs_range)
        self.inequality_idx_rhs_range = add_map_idx_object(self.inequality_idx_rhs_range, inequality_idx, rhs_range)

    def is_ineq_range_empty(self, inequality_idx: int):
        return isinstance(self.inequality_idx_lhs_range[inequality_idx], int)

    def del_inequality(self, inequality_idx: int):
        if 0 <= inequality_idx < self.inequality_idx_lhs_range.size:
            lhs_range = self.inequality_idx_lhs_range[inequality_idx]
            rhs_range = self.inequality_idx_rhs_range[inequality_idx]
            self.safe_delete_inequality(lhs_range, rhs_range)
            self.inequality_idx_lhs_range[inequality_idx] = 0
            self.inequality_idx_rhs_range[inequality_idx] = 0
            self.clear_solution()

    def safe_delete_inequality(self, lhs_range: range = None, rhs_range: range = None):
        lhs_delta = 0
        rhs_delta = 0
        if lhs_range is not None and isinstance(lhs_range, range) and lhs_range.stop > lhs_range.start:
            lhs_delta = lhs_range.stop - lhs_range.start
        if rhs_range is not None and isinstance(rhs_range, range) and rhs_range.stop > rhs_range.start:
            rhs_delta = rhs_range.stop - rhs_range.start
        for inequality_idx in range(self.inequality_idx_lhs_range.size):
            if lhs_delta > 0:
                ineq_lhs_range = self.inequality_idx_lhs_range[inequality_idx]
                if isinstance(ineq_lhs_range, range) and ineq_lhs_range.start >= lhs_range.stop:
                    self.inequality_idx_lhs_range[inequality_idx] = range(ineq_lhs_range.start - lhs_delta,
                                                                          ineq_lhs_range.stop - lhs_delta)
                elif isinstance(ineq_lhs_range, range) and ineq_lhs_range.start >= lhs_range.start:
                    self.inequality_idx_lhs_range[inequality_idx] = range(ineq_lhs_range.start,
                                                                          ineq_lhs_range.stop - lhs_delta)
            if rhs_delta > 0:
                ineq_rhs_range = self.inequality_idx_rhs_range[inequality_idx]
                if isinstance(ineq_rhs_range, range) and ineq_rhs_range.start >= rhs_range.stop:
                    self.inequality_idx_rhs_range[inequality_idx] = range(ineq_rhs_range.start - rhs_delta,
                                                                          ineq_rhs_range.stop - rhs_delta)
                elif isinstance(ineq_rhs_range, range) and ineq_rhs_range.start >= rhs_range.start:
                    self.inequality_idx_rhs_range[inequality_idx] = range(ineq_rhs_range.start,
                                                                          ineq_rhs_range.stop - rhs_delta)
        if lhs_delta > 0:
            self.A_ub_V = np.delete(self.A_ub_V, lhs_range)
            self.A_ub_I = np.delete(self.A_ub_I, lhs_range)
            self.A_ub_J = np.delete(self.A_ub_J, lhs_range)
        if rhs_delta > 0:
            self.b_ub_V = np.delete(self.b_ub_V, rhs_range)
            self.b_ub_I = np.delete(self.b_ub_I, rhs_range)

    def add_equalities_from(self, list_of_equalities: list):
        for set_of_A_eq, b_eq, equality_idx in list_of_equalities:
            b_eq = sparse.coo_matrix(b_eq)
            lhs_V = np.array([])
            lhs_I = np.array([])
            lhs_J = np.array([])
            for A_eq, j_offset in set_of_A_eq:
                A_eq = sparse.coo_matrix(A_eq)
                lhs_V = np.append(lhs_V, A_eq.data)
                lhs_I = np.append(lhs_I, A_eq.row)
                lhs_J = np.append(lhs_J, A_eq.col + j_offset)
            rhs_V = b_eq.data
            rhs_I = b_eq.row
            self.add_equality(equality_idx, lhs_V, lhs_I, lhs_J, rhs_V, rhs_I)
        self.clear_solution()

    def add_equality_from_sparse(self, equality_idx: int, lhs: sparse.coo_matrix, rhs: sparse.coo_matrix):
        return self.add_equality(equality_idx=equality_idx,
                                 lhs_V=lhs.data, lhs_I=lhs.row, lhs_J=lhs.col,
                                 rhs_V=rhs.data, rhs_I=rhs.row)

    def add_equality(self, equality_idx: int, lhs_V: np.ndarray, lhs_I: np.ndarray, lhs_J: np.ndarray,
                     rhs_V: np.ndarray, rhs_I: np.ndarray):
        lhs_range = range(self.A_eq_V.size, self.A_eq_V.size + lhs_V.size)
        rhs_range = range(self.b_eq_V.size, self.b_eq_V.size + rhs_V.size)
        self.add_map_equality(equality_idx, lhs_range, rhs_range)
        start = self.num_of_eq_rows
        self.A_eq_V = np.hstack((self.A_eq_V, lhs_V))
        self.A_eq_I = np.hstack((self.A_eq_I, lhs_I + start))
        self.A_eq_J = np.hstack((self.A_eq_J, lhs_J))
        self.b_eq_V = np.hstack((self.b_eq_V, rhs_V))
        self.b_eq_I = np.hstack((self.b_eq_I, rhs_I + start))
        return lhs_range, rhs_range

    def add_map_equality(self, equality_idx: int, lhs_range: range, rhs_range: range):
        from idstl.Context import add_map_idx_object
        self.equality_idx_lhs_range = add_map_idx_object(self.equality_idx_lhs_range, equality_idx, lhs_range)
        self.equality_idx_rhs_range = add_map_idx_object(self.equality_idx_rhs_range, equality_idx, rhs_range)

    def is_eq_range_empty(self, equality_idx: int):
        return not isinstance(self.equality_idx_lhs_range[equality_idx], range)

    def del_equality(self, equality_idx: int):
        if 0 <= equality_idx < self.equality_idx_lhs_range.size:
            lhs_range = self.equality_idx_lhs_range[equality_idx]
            rhs_range = self.equality_idx_rhs_range[equality_idx]
            self.safe_delete_equality(lhs_range, rhs_range)
            self.equality_idx_lhs_range[equality_idx] = 0
            self.equality_idx_rhs_range[equality_idx] = 0
            self.clear_solution()

    def safe_delete_equality(self, lhs_range: range = None, rhs_range: range = None):
        lhs_delta = 0
        rhs_delta = 0
        if lhs_range is not None and isinstance(lhs_range, range) and lhs_range.stop > lhs_range.start:
            lhs_delta = lhs_range.stop - lhs_range.start
        if rhs_range is not None and isinstance(rhs_range, range) and rhs_range.stop > rhs_range.start:
            rhs_delta = rhs_range.stop - rhs_range.start
        for equality_idx in range(self.equality_idx_lhs_range.size):
            if lhs_delta > 0:
                eq_lhs_range = self.equality_idx_lhs_range[equality_idx]
                if isinstance(eq_lhs_range, range) and eq_lhs_range.start >= lhs_range.stop:
                    self.equality_idx_lhs_range[equality_idx] = range(eq_lhs_range.start - lhs_delta,
                                                                      eq_lhs_range.stop - lhs_delta)
                elif isinstance(eq_lhs_range, range) and eq_lhs_range.start >= lhs_range.start:
                    self.equality_idx_lhs_range[equality_idx] = range(eq_lhs_range.start,
                                                                      eq_lhs_range.stop - lhs_delta)
            if rhs_delta > 0:
                eq_rhs_range = self.equality_idx_rhs_range[equality_idx]
                if isinstance(eq_rhs_range, range) and eq_rhs_range.start >= rhs_range.stop:
                    self.equality_idx_rhs_range[equality_idx] = range(eq_rhs_range.start - rhs_delta,
                                                                      eq_rhs_range.stop - rhs_delta)
                elif isinstance(eq_rhs_range, range) and eq_rhs_range.start >= rhs_range.start:
                    self.equality_idx_rhs_range[equality_idx] = range(eq_rhs_range.start,
                                                                      eq_rhs_range.stop - rhs_delta)
        if lhs_delta > 0:
            self.A_eq_V = np.delete(self.A_eq_V, lhs_range)
            self.A_eq_I = np.delete(self.A_eq_I, lhs_range)
            self.A_eq_J = np.delete(self.A_eq_J, lhs_range)
        if rhs_delta > 0:
            self.b_eq_V = np.delete(self.b_eq_V, rhs_range)
            self.b_eq_I = np.delete(self.b_eq_I, rhs_range)

    def set_objective(self,
                      data: Union[float, int, list, set],
                      row: Union[int, list, slice, range, np.ndarray]):
        self.c[row] = data
        self.clear_solution()

    def solve(self):
        Aub = None
        bub = None
        Aeq = None
        beq = None
        if self.num_of_inequalities > 0:
            Aub = self.get_A_ub()
            bub = self.get_b_ub().toarray().flatten()
        if self.num_of_equalities > 0:
            Aeq = self.get_A_eq()
            beq = self.get_b_eq().toarray().flatten()
        elapsed_time, self.is_optimal, self.objVal, self.x = solve_lp(
            self.c, self.lb, self.ub, Aub, bub, Aeq, beq, LPModel.env
        )
        return elapsed_time

    def get_objective(self):
        return self.objVal

    def get_x(self):
        return self.x
