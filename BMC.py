import z3
import numpy as np


class Variables:
    def __init__(self):
        self.map_keys_to_idx = np.zeros((0, 2), dtype=object)
        self.variables = np.zeros(0, dtype=object)

    def search_key(self, key: str):
        i = np.searchsorted(self.map_keys_to_idx[:, 0], key)
        if i == self.map_keys_to_idx.shape[0] or (self.map_keys_to_idx[i, 0] != key):
            return None
        return self.map_keys_to_idx[i, 1]

    def bitvec_at(self, key: str, k: int, bitlen: int) -> z3.BitVecRef:
        row = np.searchsorted(self.map_keys_to_idx[:, 0], key)
        if row == self.map_keys_to_idx.shape[0] or (self.map_keys_to_idx[row, 0] != key):
            if k > 0:
                raise IndexError
            v = z3.BitVec(key + '_' + str(k), bitlen)
            idx = self.variables.size
            self.variables = np.append(self.variables, 0)
            self.variables[idx] = np.array([v])
            self.map_keys_to_idx = np.insert(self.map_keys_to_idx, row, [key, idx], axis=0)
        else:
            idx = self.map_keys_to_idx[row, 1]
            if k == self.variables[idx].size:
                v = z3.BitVec(key + '_' + str(k), bitlen)
                self.variables[idx] = np.hstack((self.variables[idx], v))
            elif 0 <= k < self.variables[idx].size:
                v = self.variables[idx][k]
            else:
                raise IndexError
        return v

    def bitvec(self, key: str, bitlen: int) -> z3.BitVecRef:
        row = np.searchsorted(self.map_keys_to_idx[:, 0], key)
        if row == self.map_keys_to_idx.shape[0] or (self.map_keys_to_idx[row, 0] != key):
            v = z3.BitVec(key, bitlen)
            idx = self.variables.size
            self.variables = np.append(self.variables, 0)
            self.variables[idx] = v
            self.map_keys_to_idx = np.insert(self.map_keys_to_idx, row, [key, idx], axis=0)
        else:
            idx = self.map_keys_to_idx[row, 1]
            v = self.variables[idx]
        return v

    def bool_at(self, key: str, k: int) -> z3.BoolRef:
        row = np.searchsorted(self.map_keys_to_idx[:, 0], key)
        if row == self.map_keys_to_idx.shape[0] or (self.map_keys_to_idx[row, 0] != key):
            if k > 0:
                raise IndexError
            v = z3.Bool(key + '_' + str(k))
            idx = self.variables.size
            self.variables = np.append(self.variables, 0)
            self.variables[idx] = np.array([v])
            self.map_keys_to_idx = np.insert(self.map_keys_to_idx, row, [key, idx], axis=0)
        else:
            idx = self.map_keys_to_idx[row, 1]
            if k == self.variables[idx].size:
                v = z3.Bool(key + '_' + str(k))
                self.variables[idx] = np.hstack((self.variables[idx], v))
            elif 0 <= k < self.variables[idx].size:
                v = self.variables[idx][k]
            else:
                raise IndexError
        return v

    def bool(self, key: str) -> z3.BoolRef:
        row = np.searchsorted(self.map_keys_to_idx[:, 0], key)
        if row == self.map_keys_to_idx.shape[0] or (self.map_keys_to_idx[row, 0] != key):
            v = z3.Bool(key)
            idx = self.variables.size
            self.variables = np.append(self.variables, 0)
            self.variables[idx] = v
            self.map_keys_to_idx = np.insert(self.map_keys_to_idx, row, [key, idx], axis=0)
        else:
            idx = self.map_keys_to_idx[row, 1]
            v = self.variables[idx]
        return v


class BMC:
    def __init__(self, stl_formula, transition_system):
        from idstl.Formula import Formula
        from idstl.TransitionSystem import TransitionSystem
        assert isinstance(stl_formula, Formula)
        assert isinstance(transition_system, TransitionSystem)
        self.ts = transition_system
        self.stl_formula = stl_formula
        self.solver = z3.Solver()
        self.model = None
        self.set_of_cex = np.array([], dtype=object)
        self.K = 0
        self.variables = Variables()
        self.num_of_subformulas = self.stl_formula.get_num_of_formulas()
        # if self.ts.get_num_of_symbols() == 1:
        #     transitions = self.ts.get_array_of_transitions()
        #     adj = np.zeros((self.ts.get_num_of_partitions(), self.ts.get_num_of_partitions()), dtype=bool)
        #     for i in range(transitions.size):
        #         adj[transitions[i].get_from_idx(), transitions[i].get_to_idx()] = True
        #     self.transitions = np.zeros((0, 2), dtype=object)
        #     start = 0
        #     while start < self.ts.get_num_of_partitions():
        #         stop = start + 1
        #         while stop < self.ts.get_num_of_partitions() and np.all(np.equal(adj[start, :], adj[stop, :])):
        #             stop += 1
        #         self.transitions = np.vstack(
        #             (self.transitions, np.array([0, 0], dtype=object)))
        #         self.transitions[-1, 0] = range(start, stop)
        #         self.transitions[-1, 1] = np.where(adj[start, :])[0]
        #         start = stop
        # else:
        #     self.transitions = self.ts.get_array_of_transitions()
        self.transitions = self.ts.get_array_of_transitions()
        self.t_bitlen = int(self.transitions.size).bit_length()
        self.s_bitlen = self.ts.get_num_of_partitions().bit_length()
        self.a_bitlen = self.ts.get_num_of_symbols().bit_length()
        self.f_bitlen = self.num_of_subformulas.bit_length()
        self.example = None

    def __str__(self):
        my_str = 'BMC is '
        if self.example is not None:
            my_str += 'sat with K = ' + str(self.K) + ' and L = ' + str(self.example['L']) + '\n'
            my_str += 'Tunnel = ' + str(['P_' + str(idx) for idx in self.example['seq_of_partition_ind']]) + '\n'
            my_str += 'Actions = ' + str(['sig_' + str(idx) for idx in self.example['seq_of_symbol_ind']]) + '\n'
            for phi in self.stl_formula.get_closure():
                if phi.get_value() is None:
                    my_str += str(phi) + ' = ' + str(self.example['formula_eval'][phi.get_idx(), :]) + '\n'
        else:
            my_str += ' unsat with K = ' + str(self.K) + '\n'
        return my_str

    def get_loop_free_path_status(self):
        my_str = 'BMC is '
        # noinspection PyBroadException
        try:
            m = self.solver.model()
            seq_of_partition_ind = np.array(
                [int(m[self.variables.bitvec_at('s', k, self.s_bitlen)].as_long())
                 for k in range(self.K + 1)],
                dtype=int
            )
            seq_of_symbol_ind = np.array(
                [int(m[self.variables.bitvec_at('a', k, self.s_bitlen)].as_long())
                 for k in range(self.K)],
                dtype=int
            )
            formula_eval = np.array(
                [
                    [bool(m[self.variables.bool_at('f' + str(idx), k)]) for k in range(self.K + 1)]
                    for idx in range(self.num_of_subformulas)
                ], dtype=bool
            )
            my_str += 'sat with K = ' + str(self.K) + '\n'
            my_str += 'Tunnel = ' + str(['P_' + str(idx) for idx in seq_of_partition_ind]) + '\n'
            my_str += 'Actions = ' + str(['sig_' + str(idx) for idx in seq_of_symbol_ind]) + '\n'
            for phi in self.stl_formula.get_closure():
                if phi.get_value() is None:
                    my_str += str(phi) + ' = ' + str(formula_eval[phi.get_idx(), :]) + '\n'
        except:
            my_str += ' unsat with K = ' + str(self.K) + '\n'
        return my_str

    def get_smt2_code(self):
        return self.solver.to_smt2()

    def subformula(self, i: int):
        return self.stl_formula.get_subformula(i)

    def assert_subformula_encoding_at(self, k: int, subformula) -> z3.ExprRef:
        from idstl.Formula import AtomicProposition, Not, And, Or, Until, Release
        phi = self.variables.bool_at('f' + str(subformula.get_idx()), k)
        if isinstance(subformula, AtomicProposition):
            s_k = self.variables.bitvec_at('s', k, self.s_bitlen)
            expr = z3.Or([s_k == int(s) for s in self.ts.inv_labeling_function(subformula.get_label_idx())])
            self.solver.add(phi == expr)
        elif isinstance(subformula, Not):
            s_k = self.variables.bitvec_at('s', k, self.s_bitlen)
            expr = z3.And([s_k != int(s) for s in self.ts.inv_labeling_function(subformula.get_label_idx())])
            self.solver.add(phi == expr)
        elif isinstance(subformula, And):
            expr = z3.And([self.variables.bool_at('f' + str(child.get_idx()), k) for child in subformula.children])
            self.solver.add(phi == expr)
        elif isinstance(subformula, Or):
            expr = z3.Or([self.variables.bool_at('f' + str(child.get_idx()), k) for child in subformula.children])
            self.solver.add(phi == expr)
        elif isinstance(subformula, Until) and subformula.a > 0:
            phi_1 = self.variables.bool_at('f' + str(subformula.child_1.get_idx()), k)
            phi_2 = self.variables.bool_at('f' + str(subformula.child_2.get_idx()), k)
            phi_a = self.variables.bool_at('fa' + str(subformula.get_idx()), k)
            phi_a_nxt = self.variables.bool_at('fa' + str(subformula.get_idx()), k + 1)
            phi_nxt = self.variables.bool_at('f' + str(subformula.get_idx()), k + 1)
            expr = z3.Or(phi_a, phi_nxt)
            self.solver.add(phi == expr)
            expr = z3.Or(phi_2, z3.And(phi_1, phi_a_nxt))
            self.solver.add(phi_a == expr)
        elif isinstance(subformula, Until):
            phi_1 = self.variables.bool_at('f' + str(subformula.child_1.get_idx()), k)
            phi_2 = self.variables.bool_at('f' + str(subformula.child_2.get_idx()), k)
            phi_nxt = self.variables.bool_at('f' + str(subformula.get_idx()), k + 1)
            expr = z3.Or(phi_2, z3.And(phi_1, phi_nxt))
            self.solver.add(phi == expr)
        elif isinstance(subformula, Release) and subformula.a > 0:
            phi_1 = self.variables.bool_at('f' + str(subformula.child_1.get_idx()), k)
            phi_2 = self.variables.bool_at('f' + str(subformula.child_2.get_idx()), k)
            phi_a = self.variables.bool_at('fa' + str(subformula.get_idx()), k)
            phi_a_nxt = self.variables.bool_at('fa' + str(subformula.get_idx()), k + 1)
            phi_nxt = self.variables.bool_at('f' + str(subformula.get_idx()), k + 1)
            expr = z3.Or(phi_a, phi_nxt)
            self.solver.add(phi == expr)
            expr = z3.And(phi_2, z3.Or(phi_1, phi_a_nxt))
            self.solver.add(phi_a == expr)
        elif isinstance(subformula, Release):
            phi_1 = self.variables.bool_at('f' + str(subformula.child_1.get_idx()), k)
            phi_2 = self.variables.bool_at('f' + str(subformula.child_2.get_idx()), k)
            phi_nxt = self.variables.bool_at('f' + str(subformula.get_idx()), k + 1)
            expr = z3.And(phi_2, z3.Or(phi_1, phi_nxt))
            self.solver.add(phi == expr)
        else:
            raise TypeError
        return expr

    def create_cex(self, seq_of_partition_ind: np.ndarray, seq_of_symbols_ind: np.ndarray):
        cex_idx = self.set_of_cex.size
        seq_sz = seq_of_partition_ind.size
        N = int(2 * seq_sz - 1)
        cex = {
            'idx': cex_idx,
            'K': int(seq_sz - 1),
            'N': N,
            'bitlen': N.bit_length(),
            's_i': np.arange(seq_sz).astype(int),
            's_i_e': np.arange(seq_sz, 2 * seq_sz - 1).astype(int),
            's': seq_of_partition_ind.astype(int),
            'a': seq_of_symbols_ind.astype(int),
        }
        self.set_of_cex = np.hstack((self.set_of_cex, cex))
        return cex

    def create_example(self):
        m = self.solver.model()
        seq_of_partition_ind = np.array(
            [int(m[self.variables.bitvec_at('s', k, self.s_bitlen)].as_long())
             for k in range(self.K + 1)],
            dtype=int
        )
        if self.ts.get_num_of_symbols() == 1:
            seq_of_symbol_ind = np.zeros(self.K, dtype=int)
        else:
            seq_of_symbol_ind = np.array(
                [int(m[self.variables.bitvec_at('a', k, self.s_bitlen)].as_long())
                 for k in range(self.K)],
                dtype=int
            )
        formula_eval = np.array(
            [
                [bool(m[self.variables.bool_at('f' + str(idx), k)]) for k in range(self.K + 1)]
                for idx in range(self.num_of_subformulas)
            ], dtype=bool
        )
        L = self.K + 1
        if bool(m[self.variables.bool('el')]):
            L = next(k for k in range(self.K + 1) if bool(m[self.variables.bool_at('l', k)]))
        return {
            'seq_of_partition_ind': seq_of_partition_ind,
            'seq_of_symbol_ind': seq_of_symbol_ind,
            'formula_eval': formula_eval,
            'K': self.K,
            'L': L
        }

    def s_k(self, k: int):
        return self.variables.bitvec_at('s', k, self.s_bitlen)

    def a_k(self, k: int):
        return self.variables.bitvec_at('a', k, self.a_bitlen)

    @property
    def s_E(self):
        return self.variables.bitvec('s_E', self.s_bitlen)

    def phi_i_k(self, i: int, k: int):
        return self.variables.bool_at('f' + str(i), k)

    def phi_i_L(self, i: int):
        return self.variables.bool('f' + str(i) + '_L')

    def phi_i_E(self, i: int):
        return self.variables.bool('f' + str(i) + '_E')

    def event_phi_i_2_E(self, i: int):
        return self.variables.bool('ef' + str(i) + '_E')

    def event_phi_i_2_k(self, i: int, k: int):
        return self.variables.bool_at('ef' + str(i), k)

    def always_phi_i_2_E(self, i: int):
        return self.variables.bool('af' + str(i) + '_E')

    def always_phi_i_2_k(self, i: int, k: int):
        return self.variables.bool_at('af' + str(i), k)

    def phi_i_aux_k(self, i: int, k: int):
        return self.variables.bool_at('fa' + str(i), k)

    def phi_i_aux_L(self, i: int):
        return self.variables.bool('fa' + str(i) + '_L')

    def phi_i_aux_E(self, i: int):
        return self.variables.bool('fa' + str(i) + '_E')

    def event_phi_i_aux_2_E(self, i: int):
        return self.variables.bool('efa' + str(i) + '_E')

    def event_phi_i_aux_2_k(self, i: int, k: int):
        return self.variables.bool_at('efa' + str(i), k)

    def always_phi_i_aux_2_E(self, i: int):
        return self.variables.bool('afa' + str(i) + '_E')

    def always_phi_i_aux_2_k(self, i: int, k: int):
        return self.variables.bool_at('afa' + str(i), k)

    def loop_k(self, k: int):
        return self.variables.bool_at('l', k)

    def in_loop_k(self, k: int):
        return self.variables.bool_at('il', k)

    @property
    def exists_loop(self):
        return self.variables.bool('el')

    def s_cex_k(self, cex, k: int):
        return self.variables.bitvec_at('s_cex_' + str(cex['idx']), k, cex['bitlen'])

    def encode_atomic_proposition_at(self, subformula, k: int):
        from idstl.Formula import AtomicProposition
        assert isinstance(subformula, AtomicProposition)
        states = self.ts.inv_labeling_function(subformula.get_label_idx())
        if states.size > 0:
            self.solver.add(
                self.phi_i_k(subformula.get_idx(), k) == z3.Or(
                    [self.s_k(k) == int(s) for s in states]
                )
            )

    def encode_not_at(self, subformula, k: int):
        from idstl.Formula import Not
        assert isinstance(subformula, Not)
        self.solver.add(
            self.phi_i_k(subformula.get_idx(), k) == z3.And(
                [self.s_k(k) != int(s) for s in self.ts.inv_labeling_function(subformula.get_label_idx())]
            )
        )

    def encode_and_at(self, subformula, k: int):
        from idstl.Formula import And, Formula
        assert isinstance(subformula, And)
        self.solver.add(
            self.phi_i_k(subformula.get_idx(), k) == z3.And([
                self.phi_i_k(child.get_idx(), k) if isinstance(child, Formula) and child.value is None
                else z3.BoolVal(child.value)
                for child in subformula.children
            ])
        )

    def encode_or_at(self, subformula, k: int):
        from idstl.Formula import And, Formula
        assert isinstance(subformula, And)
        self.solver.add(
            self.phi_i_k(subformula.get_idx(), k) == z3.Or([
                self.phi_i_k(child.get_idx(), k) if isinstance(child, Formula) and child.value is None
                else z3.BoolVal(child.value)
                for child in subformula.children
            ])
        )

    def encode_k_dep_smt(self, phi_K: z3.BoolRef, phi_E: z3.BoolRef,
                         prefix_phi_2_K: z3.BoolRef, prefix_phi_2_E: z3.BoolRef):
        self.solver.add(prefix_phi_2_K == prefix_phi_2_E)
        self.solver.add(phi_E == phi_K)

    def encode_until_at_smt(self, phi: z3.BoolRef, phi_nxt: z3.BoolRef, phi_1: z3.BoolRef, phi_2: z3.BoolRef):
        self.solver.add(phi == z3.Or(phi_2, z3.And(phi_1, phi_nxt)))

    def encode_release_at_smt(self, phi: z3.BoolRef, phi_nxt: z3.BoolRef, phi_1: z3.BoolRef, phi_2: z3.BoolRef):
        self.solver.add(phi == z3.And(phi_2, z3.Or(phi_1, phi_nxt)))

    def encode_until_base_smt(self, phi_E: z3.BoolRef, event_phi_2_E: z3.BoolRef, event_phi_2_0: z3.BoolRef):
        self.solver.add(z3.Implies(self.exists_loop, z3.Implies(phi_E, event_phi_2_E)))
        self.solver.add(z3.Not(event_phi_2_0))

    def encode_release_base_smt(self, phi_E: z3.BoolRef, always_phi_2_E: z3.BoolRef, always_phi_2_0: z3.BoolRef):
        self.solver.add(z3.Implies(self.exists_loop, z3.Implies(always_phi_2_E, phi_E)))
        self.solver.add(always_phi_2_0)

    def encode_until_k_ind_smt_at(self, k: int, phi_2: z3.BoolRef,
                                  event_phi_2: z3.BoolRef, event_phi_2_prev: z3.BoolRef):
        self.solver.add(z3.Or(event_phi_2 == event_phi_2_prev, z3.And(self.in_loop_k(k), phi_2)))

    def encode_release_k_ind_smt_at(self, k: int, phi_2: z3.BoolRef,
                                    always_phi_2: z3.BoolRef, always_phi_2_prev: z3.BoolRef):
        self.solver.add(z3.And(always_phi_2 == always_phi_2_prev, z3.Or(z3.Not(self.in_loop_k(k)), phi_2)))

    def encode_until_at(self, subformula, k: int):
        from idstl.Formula import Until
        assert isinstance(subformula, Until) and subformula.a == 0 and 0 <= k <= self.K
        self.encode_until_at_smt(
            phi=self.phi_i_k(subformula.get_idx(), k),
            phi_nxt=self.phi_i_k(subformula.get_idx(), k + 1),
            phi_1=self.phi_i_k(subformula.child_1.get_idx(), k) if subformula.child_1.value is None
            else z3.BoolVal(subformula.child_1.value),
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k) if subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value)
        )

    def encode_until_base(self, subformula):
        from idstl.Formula import Until
        assert isinstance(subformula, Until) and subformula.a == 0 and self.K == 0
        self.encode_until_base_smt(
            phi_E=self.phi_i_E(subformula.get_idx()),
            event_phi_2_E=self.event_phi_i_2_E(subformula.get_idx()),
            event_phi_2_0=self.event_phi_i_2_k(subformula.get_idx(), 0)
        )

    def encode_until_k_dep(self, subformula):
        from idstl.Formula import Until
        assert isinstance(subformula, Until) and subformula.a == 0
        self.encode_k_dep_smt(
            phi_K=self.phi_i_k(subformula.get_idx(), self.K),
            phi_E=self.phi_i_E(subformula.get_idx()),
            prefix_phi_2_K=self.event_phi_i_2_k(subformula.get_idx(), self.K),
            prefix_phi_2_E=self.event_phi_i_2_E(subformula.get_idx())
        )

    def encode_until_k_ind(self, subformula, k: int):
        from idstl.Formula import Until, Formula
        assert isinstance(subformula, Until) and subformula.a == 0 and 1 <= k <= self.K
        self.encode_until_k_ind_smt_at(
            k=k,
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k)
            if isinstance(subformula.child_2, Formula) and subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value),
            event_phi_2=self.event_phi_i_2_k(subformula.get_idx(), k),
            event_phi_2_prev=self.event_phi_i_2_k(subformula.get_idx(), k - 1)
        )

    def encode_eventually_until_at(self, subformula, k: int):
        from idstl.Formula import Until
        assert isinstance(subformula, Until) and subformula.a > 0 and 0 <= k <= self.K
        self.encode_until_at_smt(
            phi=self.phi_i_k(subformula.get_idx(), k),
            phi_nxt=self.phi_i_k(subformula.get_idx(), k + 1),
            phi_1=z3.BoolVal(True),
            phi_2=self.phi_i_aux_k(subformula.get_idx(), k)
        )
        self.encode_until_at_smt(
            phi=self.phi_i_aux_k(subformula.get_idx(), k),
            phi_nxt=self.phi_i_aux_k(subformula.get_idx(), k + 1),
            phi_1=self.phi_i_k(subformula.child_1.get_idx(), k) if subformula.child_1.value is None
            else z3.BoolVal(subformula.child_1.value),
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k) if subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value)
        )

    def encode_eventually_until_base(self, subformula):
        from idstl.Formula import Until
        assert isinstance(subformula, Until) and subformula.a > 0
        self.encode_until_base_smt(
            phi_E=self.phi_i_E(subformula.get_idx()),
            event_phi_2_E=self.event_phi_i_2_E(subformula.get_idx()),
            event_phi_2_0=self.event_phi_i_2_k(subformula.get_idx(), 0)
        )
        self.encode_until_base_smt(
            phi_E=self.phi_i_aux_E(subformula.get_idx()),
            event_phi_2_E=self.event_phi_i_aux_2_E(subformula.get_idx()),
            event_phi_2_0=self.event_phi_i_aux_2_k(subformula.get_idx(), 0)
        )

    def encode_eventually_until_k_dep(self, subformula):
        from idstl.Formula import Until
        assert isinstance(subformula, Until) and subformula.a > 0
        self.encode_k_dep_smt(
            phi_K=self.phi_i_k(subformula.get_idx(), self.K),
            phi_E=self.phi_i_E(subformula.get_idx()),
            prefix_phi_2_K=self.event_phi_i_2_k(subformula.get_idx(), self.K),
            prefix_phi_2_E=self.event_phi_i_2_E(subformula.get_idx())
        )
        self.encode_k_dep_smt(
            phi_K=self.phi_i_aux_k(subformula.get_idx(), self.K),
            phi_E=self.phi_i_aux_E(subformula.get_idx()),
            prefix_phi_2_K=self.event_phi_i_aux_2_k(subformula.get_idx(), self.K),
            prefix_phi_2_E=self.event_phi_i_aux_2_E(subformula.get_idx())
        )

    def encode_eventually_until_k_ind(self, subformula, k: int):
        from idstl.Formula import Until, Formula
        assert isinstance(subformula, Until) and subformula.a > 0 and 1 <= k <= self.K
        self.encode_until_k_ind_smt_at(
            k=k,
            phi_2=self.phi_i_aux_k(subformula.child_2.get_idx(), k),
            event_phi_2=self.event_phi_i_2_k(subformula.get_idx(), k),
            event_phi_2_prev=self.event_phi_i_2_k(subformula.get_idx(), k - 1)
        )
        self.encode_until_k_ind_smt_at(
            k=k,
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k)
            if isinstance(subformula.child_2, Formula) and subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value),
            event_phi_2=self.event_phi_i_aux_2_k(subformula.get_idx(), k),
            event_phi_2_prev=self.event_phi_i_aux_2_k(subformula.get_idx(), k - 1)
        )

    def encode_release_at(self, subformula, k: int):
        from idstl.Formula import Release
        assert isinstance(subformula, Release) and subformula.a == 0 and 0 <= k <= self.K
        self.encode_release_at_smt(
            phi=self.phi_i_k(subformula.get_idx(), k),
            phi_nxt=self.phi_i_k(subformula.get_idx(), k + 1),
            phi_1=self.phi_i_k(subformula.child_1.get_idx(), k) if subformula.child_1.value is None
            else z3.BoolVal(subformula.child_1.value),
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k) if subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value)
        )

    def encode_release_base(self, subformula):
        from idstl.Formula import Release
        assert isinstance(subformula, Release) and subformula.a == 0
        self.encode_release_base_smt(
            phi_E=self.phi_i_E(subformula.get_idx()),
            always_phi_2_E=self.always_phi_i_2_E(subformula.get_idx()),
            always_phi_2_0=self.always_phi_i_2_k(subformula.get_idx(), 0)
        )

    def encode_release_k_dep(self, subformula):
        from idstl.Formula import Release
        assert isinstance(subformula, Release) and subformula.a == 0
        self.encode_k_dep_smt(
            phi_K=self.phi_i_k(subformula.get_idx(), self.K),
            phi_E=self.phi_i_E(subformula.get_idx()),
            prefix_phi_2_K=self.always_phi_i_2_k(subformula.get_idx(), self.K),
            prefix_phi_2_E=self.always_phi_i_2_E(subformula.get_idx())
        )

    def encode_release_k_ind(self, subformula, k: int):
        from idstl.Formula import Release, Formula
        assert isinstance(subformula, Release) and subformula.a == 0 and 1 <= k <= self.K
        self.encode_release_k_ind_smt_at(
            k=k,
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k)
            if isinstance(subformula.child_2, Formula) and subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value),
            always_phi_2=self.always_phi_i_2_k(subformula.get_idx(), k),
            always_phi_2_prev=self.always_phi_i_2_k(subformula.get_idx(), k - 1)
        )

    def encode_eventually_release_at(self, subformula, k: int):
        from idstl.Formula import Release
        assert isinstance(subformula, Release) and subformula.a > 0 and 0 <= k <= self.K
        self.encode_until_at_smt(
            phi=self.phi_i_k(subformula.get_idx(), k),
            phi_nxt=self.phi_i_k(subformula.get_idx(), k + 1),
            phi_1=z3.BoolVal(True),
            phi_2=self.phi_i_aux_k(subformula.get_idx(), k)
        )
        self.encode_release_at_smt(
            phi=self.phi_i_aux_k(subformula.get_idx(), k),
            phi_nxt=self.phi_i_aux_k(subformula.get_idx(), k + 1),
            phi_1=self.phi_i_k(subformula.child_1.get_idx(), k) if subformula.child_1.value is None
            else z3.BoolVal(subformula.child_1.value),
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k) if subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value)
        )

    def encode_eventually_release_base(self, subformula):
        from idstl.Formula import Release
        assert isinstance(subformula, Release) and subformula.a > 0
        self.encode_until_base_smt(
            phi_E=self.phi_i_E(subformula.get_idx()),
            event_phi_2_E=self.event_phi_i_2_E(subformula.get_idx()),
            event_phi_2_0=self.event_phi_i_2_k(subformula.get_idx(), 0)
        )
        self.encode_release_base_smt(
            phi_E=self.phi_i_aux_E(subformula.get_idx()),
            always_phi_2_E=self.always_phi_i_aux_2_E(subformula.get_idx()),
            always_phi_2_0=self.always_phi_i_aux_2_k(subformula.get_idx(), 0)
        )

    def encode_eventually_release_k_dep(self, subformula):
        from idstl.Formula import Release
        assert isinstance(subformula, Release) and subformula.a > 0
        self.encode_k_dep_smt(
            phi_K=self.phi_i_k(subformula.get_idx(), self.K),
            phi_E=self.phi_i_E(subformula.get_idx()),
            prefix_phi_2_K=self.event_phi_i_2_k(subformula.get_idx(), self.K),
            prefix_phi_2_E=self.event_phi_i_2_E(subformula.get_idx())
        )
        self.encode_k_dep_smt(
            phi_K=self.phi_i_aux_k(subformula.get_idx(), self.K),
            phi_E=self.phi_i_aux_E(subformula.get_idx()),
            prefix_phi_2_K=self.always_phi_i_aux_2_k(subformula.get_idx(), self.K),
            prefix_phi_2_E=self.always_phi_i_aux_2_E(subformula.get_idx())
        )

    def encode_eventually_release_k_ind(self, subformula, k: int):
        from idstl.Formula import Release, Formula
        assert isinstance(subformula, Release) and subformula.a > 0 and 1 <= k <= self.K
        self.encode_until_k_ind_smt_at(
            k=k,
            phi_2=self.phi_i_aux_k(subformula.get_idx(), k),
            event_phi_2=self.event_phi_i_2_k(subformula.get_idx(), k),
            event_phi_2_prev=self.event_phi_i_2_k(subformula.get_idx(), k - 1)
        )
        self.encode_release_k_ind_smt_at(
            k=k,
            phi_2=self.phi_i_k(subformula.child_2.get_idx(), k)
            if isinstance(subformula.child_2, Formula) and subformula.child_2.value is None
            else z3.BoolVal(subformula.child_2.value),
            always_phi_2=self.always_phi_i_aux_2_k(subformula.get_idx(), k),
            always_phi_2_prev=self.always_phi_i_aux_2_k(subformula.get_idx(), k - 1)
        )

    def initialize_subformula(self, subformula):
        from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release
        assert isinstance(subformula, Formula)
        self.solver.add(z3.Implies(z3.Not(self.exists_loop), z3.Not(self.phi_i_L(subformula.get_idx()))))
        if isinstance(subformula, AtomicProposition):
            self.encode_atomic_proposition_at(subformula, 0)
        elif isinstance(subformula, Not):
            self.encode_not_at(subformula, 0)
        elif isinstance(subformula, And):
            self.encode_and_at(subformula, 0)
            for child in subformula.children:
                if child.value is None:
                    self.initialize_subformula(child)
        elif isinstance(subformula, Or):
            self.encode_or_at(subformula, 0)
            for child in subformula.children:
                if child.value is None:
                    self.initialize_subformula(child)
        elif isinstance(subformula, Until) and subformula.a > 0:
            self.encode_eventually_until_at(subformula, 0)
            self.encode_eventually_until_base(subformula)
            if subformula.child_1.value is None:
                self.initialize_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.initialize_subformula(subformula.child_2)
        elif isinstance(subformula, Until) and subformula.a == 0:
            self.encode_until_at(subformula, 0)
            self.encode_until_base(subformula)
            if subformula.child_1.value is None:
                self.initialize_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.initialize_subformula(subformula.child_2)
        elif isinstance(subformula, Release) and subformula.a > 0:
            self.encode_eventually_release_at(subformula, 0)
            self.encode_eventually_release_base(subformula)
            if subformula.child_1.value is None:
                self.initialize_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.initialize_subformula(subformula.child_2)
        elif isinstance(subformula, Release) and subformula.a == 0:
            self.encode_release_at(subformula, 0)
            self.encode_release_base(subformula)
            if subformula.child_1.value is None:
                self.initialize_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.initialize_subformula(subformula.child_2)
        else:
            raise TypeError

    def k_dep_subformula(self, subformula):
        from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release
        assert isinstance(subformula, Formula)
        self.solver.add(self.phi_i_L(subformula.get_idx()) == self.phi_i_k(subformula.get_idx(), self.K + 1))
        if isinstance(subformula, AtomicProposition):
            pass
        elif isinstance(subformula, Not):
            pass
        elif isinstance(subformula, And):
            for child in subformula.children:
                if child.value is None:
                    self.k_dep_subformula(child)
        elif isinstance(subformula, Or):
            for child in subformula.children:
                if child.value is None:
                    self.k_dep_subformula(child)
        elif isinstance(subformula, Until) and subformula.a > 0:
            self.encode_eventually_until_k_dep(subformula)
            if subformula.child_1.value is None:
                self.k_dep_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.k_dep_subformula(subformula.child_2)
        elif isinstance(subformula, Until) and subformula.a == 0:
            self.encode_until_k_dep(subformula)
            if subformula.child_1.value is None:
                self.k_dep_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.k_dep_subformula(subformula.child_2)
        elif isinstance(subformula, Release) and subformula.a > 0:
            self.encode_eventually_release_k_dep(subformula)
            if subformula.child_1.value is None:
                self.k_dep_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.k_dep_subformula(subformula.child_2)
        elif isinstance(subformula, Release) and subformula.a == 0:
            self.encode_release_k_dep(subformula)
            if subformula.child_1.value is None:
                self.k_dep_subformula(subformula.child_1)
            if subformula.child_2.value is None:
                self.k_dep_subformula(subformula.child_2)
        else:
            raise TypeError

    def k_ind_subformula(self, subformula, k: int):
        from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release
        assert isinstance(subformula, Formula)
        self.solver.add(
            z3.Implies(self.loop_k(k),
                       self.phi_i_L(subformula.get_idx()) == self.phi_i_k(subformula.get_idx(), k + 1))
        )
        if isinstance(subformula, AtomicProposition):
            self.encode_atomic_proposition_at(subformula, k)
        elif isinstance(subformula, Not):
            self.encode_not_at(subformula, k)
        elif isinstance(subformula, And):
            self.encode_and_at(subformula, k)
            for child in subformula.children:
                if child.value is None:
                    self.k_ind_subformula(child, k)
        elif isinstance(subformula, Or):
            self.encode_or_at(subformula, k)
            for child in subformula.children:
                if child.value is None:
                    self.k_ind_subformula(child, k)
        elif isinstance(subformula, Until) and subformula.a > 0:
            self.encode_eventually_until_at(subformula, k)
            self.encode_eventually_until_k_ind(subformula, k)
            if subformula.child_1.value is None:
                self.k_ind_subformula(subformula.child_1, k)
            if subformula.child_2.value is None:
                self.k_ind_subformula(subformula.child_2, k)
        elif isinstance(subformula, Until) and subformula.a == 0:
            self.encode_until_at(subformula, k)
            self.encode_until_k_ind(subformula, k)
            if subformula.child_1.value is None:
                self.k_ind_subformula(subformula.child_1, k)
            if subformula.child_2.value is None:
                self.k_ind_subformula(subformula.child_2, k)
        elif isinstance(subformula, Release) and subformula.a > 0:
            self.encode_eventually_release_at(subformula, k)
            self.encode_eventually_release_k_ind(subformula, k)
            if subformula.child_1.value is None:
                self.k_ind_subformula(subformula.child_1, k)
            if subformula.child_2.value is None:
                self.k_ind_subformula(subformula.child_2, k)
        elif isinstance(subformula, Release) and subformula.a == 0:
            self.encode_release_at(subformula, k)
            self.encode_release_k_ind(subformula, k)
            if subformula.child_1.value is None:
                self.k_ind_subformula(subformula.child_1, k)
            if subformula.child_2.value is None:
                self.k_ind_subformula(subformula.child_2, k)
        else:
            raise TypeError

    def initialize_ts(self):
        self.solver.add(self.s_k(0) == self.ts.get_initial_partition_idx())

    def initialize_loop(self):
        self.solver.add(z3.Not(self.loop_k(0)))
        self.solver.add(z3.Not(self.in_loop_k(0)))

    def initialize_formula(self, formula):
        self.solver.add(self.phi_i_k(formula.get_idx(), 0))
        self.initialize_subformula(formula)

    def k_ind_cex(self, cex, k: int):
        expr = z3.Or(
            z3.Or([
                z3.And(self.s_cex_k(cex, k - 1) == int(cex['s_i'][i]),
                       self.a_k(k - 1) == 0,
                       self.s_k(k) == int(cex['s'][i]),
                       self.s_cex_k(cex, k) == int(cex['s_i'][i]))
                for i in range(cex['K'])
            ]),
            z3.Or([
                z3.And(self.s_cex_k(cex, k - 1) == int(cex['s_i'][i]),
                       self.a_k(k - 1) != int(cex['a'][i]),
                       self.a_k(k - 1) != 0,
                       self.s_cex_k(cex, k) == int(cex['s_i_e'][i]))
                for i in range(cex['K'])
            ]),
            z3.Or([
                z3.And(self.s_cex_k(cex, k - 1) == int(cex['s_i'][i]),
                       self.s_k(k) != int(cex['s'][i + 1]),
                       self.s_k(k) != int(cex['s'][i]),
                       self.s_cex_k(cex, k) == int(cex['s_i_e'][i]))
                for i in range(cex['K'])
            ]),
            z3.Or([
                z3.And(self.s_cex_k(cex, k - 1) == int(cex['s_i_e'][i]),
                       self.s_cex_k(cex, k) == int(cex['s_i_e'][i]))
                for i in range(cex['K'])
            ]),
            z3.Or([
                z3.And(self.s_cex_k(cex, k - 1) == int(cex['s_i'][i]),
                       self.a_k(k - 1) == int(cex['a'][i]),
                       self.s_k(k) == int(cex['s'][i + 1]),
                       self.s_cex_k(cex, k) == int(cex['s_i'][i + 1]))
                for i in range(cex['K'])
            ])
        )
        self.solver.add(expr)

    def initialize_cex(self, cex):
        self.solver.add(self.s_cex_k(cex, 0) == int(cex['s_i'][0]))
        for k in range(1, self.K + 1):
            self.k_ind_cex(cex, k)

    def k_dep_loop(self):
        self.solver.add(self.exists_loop == self.in_loop_k(self.K))
        self.solver.add(self.s_E == self.s_k(self.K))

    def k_dep_cex(self, cex):
        self.solver.add(z3.Or([self.s_cex_k(cex, self.K) == int(s_i_e) for s_i_e in cex['s_i_e']]))

    def phi_i_neq_phi_j_array(self, subformula, i: int, j: int):
        from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release
        assert isinstance(subformula, Formula)
        arr = np.array([self.phi_i_k(subformula.get_idx(), i) != self.phi_i_k(subformula.get_idx(), j)])
        if isinstance(subformula, (AtomicProposition, Not)):
            pass
        elif isinstance(subformula, (And, Or)):
            for child in subformula.children:
                if child.value is None:
                    arr = np.hstack((arr, self.phi_i_neq_phi_j_array(child, i, j)))
        elif isinstance(subformula, (Until, Release)) and subformula.a > 0:
            arr = np.hstack(
                (arr, self.phi_i_aux_k(subformula.get_idx(), i) != self.phi_i_aux_k(subformula.get_idx(), j))
            )
            if subformula.child_1.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_1, i, j)))
            if subformula.child_2.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_2, i, j)))
        elif isinstance(subformula, (Until, Release)) and subformula.a == 0:
            if subformula.child_1.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_1, i, j)))
            if subformula.child_2.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_2, i, j)))
        else:
            raise TypeError
        return arr

    def aux_phi_i_neq_aux_phi_j_array(self, subformula, i: int, j: int):
        from idstl.Formula import Formula, AtomicProposition, Not, And, Or, Until, Release
        assert isinstance(subformula, Formula)
        arr = np.array([])
        if isinstance(subformula, (AtomicProposition, Not)):
            pass
        elif isinstance(subformula, (And, Or)):
            for child in subformula.children:
                if child.value is None:
                    arr = np.hstack((arr, self.phi_i_neq_phi_j_array(child, i, j)))
        elif isinstance(subformula, Until) and subformula.a > 0:
            arr = np.hstack((
                arr,
                self.event_phi_i_2_k(subformula.get_idx(), i) != self.event_phi_i_2_k(subformula.get_idx(), j),
                self.event_phi_i_aux_2_k(subformula.get_idx(), i) != self.event_phi_i_aux_2_k(subformula.get_idx(), j)
            ))
            if subformula.child_1.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_1, i, j)))
            if subformula.child_2.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_2, i, j)))
        elif isinstance(subformula, Until) and subformula.a == 0:
            arr = np.hstack((
                arr,
                self.event_phi_i_2_k(subformula.get_idx(), i) != self.event_phi_i_2_k(subformula.get_idx(), j)
            ))
            if subformula.child_1.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_1, i, j)))
            if subformula.child_2.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_2, i, j)))
        elif isinstance(subformula, Release) and subformula.a > 0:
            arr = np.hstack((
                arr,
                self.event_phi_i_2_k(subformula.get_idx(), i) != self.event_phi_i_2_k(subformula.get_idx(), j),
                self.always_phi_i_aux_2_k(subformula.get_idx(), i) != self.always_phi_i_aux_2_k(subformula.get_idx(), j)
            ))
            if subformula.child_1.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_1, i, j)))
            if subformula.child_2.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_2, i, j)))
        elif isinstance(subformula, Release) and subformula.a == 0:
            arr = np.hstack((
                arr,
                self.always_phi_i_2_k(subformula.get_idx(), i) != self.always_phi_i_2_k(subformula.get_idx(), j),
            ))
            if subformula.child_1.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_1, i, j)))
            if subformula.child_2.value is None:
                arr = np.hstack((arr, self.phi_i_neq_phi_j_array(subformula.child_2, i, j)))
        else:
            raise TypeError
        return arr

    def k_dep_loop_free_path(self):
        for i in range(self.K + 1):
            for j in range(i + 1, self.K + 1):
                self.solver.add(
                    z3.Or(self.s_k(i) != self.s_k(j),
                          self.in_loop_k(i) != self.in_loop_k(j),
                          z3.Or(self.phi_i_neq_phi_j_array(self.stl_formula, i, j).tolist()),
                          z3.And(self.in_loop_k(i),
                                 self.in_loop_k(j),
                                 z3.Or(self.aux_phi_i_neq_aux_phi_j_array(self.stl_formula, i, j).tolist())))
                )

    def k_ind_ts(self, k: int):
        if self.ts.get_num_of_symbols() == 1:
            # self.solver.add(z3.Or([
            #     z3.And(int(self.transitions[i, 0].start) <= self.s_k(k - 1),
            #            self.s_k(k - 1) < int(self.transitions[i, 0].stop),
            #            z3.Or([self.s_k(k) == int(self.transitions[i, 1][j])
            #                   for j in range(self.transitions[i, 1].size)]))
            #     for i in range(self.transitions.shape[0])
            # ]))
            self.solver.add(z3.Or([
                z3.And(self.s_k(k - 1) == transition.get_from_idx(),
                       self.s_k(k) == transition.get_to_idx())
                for transition in self.transitions
            ]))
        else:
            self.solver.add(z3.Or([
                z3.And(self.s_k(k - 1) == transition.get_from_idx(),
                       self.a_k(k - 1) == transition.get_symbol_idx(),
                       self.s_k(k) == transition.get_to_idx())
                for transition in self.transitions
            ]))

    def k_ind_loop(self, k: int):
        self.solver.add(z3.Implies(self.loop_k(k), self.s_k(k - 1) == self.s_E))
        self.solver.add(z3.Implies(self.in_loop_k(k - 1), z3.Not(self.loop_k(k))))
        self.solver.add(z3.Or(self.in_loop_k(k) == self.in_loop_k(k - 1), self.loop_k(k)))

    def check(self, seq_of_partition_ind: np.ndarray = None, seq_of_symbols_ind: np.ndarray = None):
        from idstl.Context import Context
        import time
        if self.K == 0:
            assert self.set_of_cex.size == 0 and seq_of_partition_ind is None and seq_of_symbols_ind is None
            self.initialize_ts()
            self.initialize_loop()
            self.initialize_formula(self.stl_formula)
            is_sat = True
        else:
            assert isinstance(seq_of_partition_ind, np.ndarray) and seq_of_partition_ind.size > 1
            assert isinstance(seq_of_symbols_ind, np.ndarray)
            assert seq_of_symbols_ind.size == seq_of_partition_ind.size - 1
            cex = self.create_cex(seq_of_partition_ind, seq_of_symbols_ind)
            self.solver.pop()
            self.initialize_cex(cex)
            self.solver.push()
            self.k_dep_loop()
            for cex in self.set_of_cex:
                self.k_dep_cex(cex)
            self.k_dep_subformula(self.stl_formula)
            start = time.time()
            is_sat = self.solver.check() == z3.sat
            stop = time.time()
            if Context.is_verbose():
                if not is_sat:
                    print('unsat with K = ' + str(self.K) + ' |cex| = ' + str(self.set_of_cex.size))
            Context.add_bmc_elapsed_time(elapsed_time=stop - start)
            if is_sat:
                self.example = self.create_example()
                return self.example
            self.example = None
            self.solver.pop()
            self.solver.push()
            self.solver.add(self.exists_loop == self.in_loop_k(self.K))
            for cex in self.set_of_cex:
                self.k_dep_cex(cex)
            self.k_dep_loop_free_path()
            start = time.time()
            is_sat = self.solver.check() == z3.sat
            stop = time.time()
            Context.add_bmc_elapsed_time(elapsed_time=stop - start)
            if Context.is_verbose():
                if is_sat:
                    print('it is not unsatisfiable with K = ' + str(self.K) + ' |cex| = ' + str(self.set_of_cex.size))
            if is_sat:
                self.example = self.create_example()
            else:
                self.example = None
        while is_sat:
            if self.K > 0:
                self.solver.pop()
            self.K += 1
            self.k_ind_ts(self.K)
            self.k_ind_loop(self.K)
            for cex in self.set_of_cex:
                self.k_ind_cex(cex, self.K)
            self.k_ind_subformula(self.stl_formula, self.K)
            self.solver.push()
            self.k_dep_loop()
            for cex in self.set_of_cex:
                self.k_dep_cex(cex)
            self.k_dep_subformula(self.stl_formula)
            start = time.time()
            is_sat = self.solver.check() == z3.sat
            stop = time.time()
            Context.add_bmc_elapsed_time(elapsed_time=stop - start)
            if Context.is_verbose():
                if not is_sat:
                    print('unsat with K = ' + str(self.K) + ' |cex| = ' + str(self.set_of_cex.size))
            for cex in self.set_of_cex:
                self.k_dep_cex(cex)
            if is_sat:
                self.example = self.create_example()
                return self.example
            self.example = None
            self.solver.pop()
            self.solver.push()
            self.solver.add(self.exists_loop == self.in_loop_k(self.K))
            start = time.time()
            self.k_dep_loop_free_path()
            stop = time.time()
            if Context.is_verbose():
                print('Elapsed time ' + str(stop - start) + ' secs.')
            start = time.time()
            is_sat = self.solver.check() == z3.sat
            stop = time.time()
            Context.add_bmc_elapsed_time(elapsed_time=stop - start)
            if Context.is_verbose():
                if is_sat:
                    print('it is not unsatisfiable with K = ' + str(self.K) + ' |cex| = ' + str(self.set_of_cex.size))
            if is_sat:
                self.example = self.create_example()
            else:
                self.example = None
        return None
