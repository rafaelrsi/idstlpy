from idstl.System import System
from idstl.Partition import Partition
from idstl.Context import Context
import numpy as np


class TransitionTriplet:
    def __init__(self, partition_from_idx: int, symbol_idx: int, partition_to_idx: int):
        self.__partition_from_idx = partition_from_idx
        self.__partition_to_idx = partition_to_idx
        self.__symbol_idx = symbol_idx

    def __str__(self):
        return '(' + str(self.__partition_from_idx) + ', ' \
               + str(self.__symbol_idx) + ', ' \
               + str(self.__partition_from_idx) + ')'

    def __lt__(self, other):
        assert isinstance(other, TransitionTriplet)
        o_from = other.get_from_idx()
        o_symbol = other.get_symbol_idx()
        oto = other.get_to_idx()
        return (self.get_from_idx() < o_from
                or (self.get_from_idx() == o_from
                    and self.get_symbol_idx() < o_symbol)
                or (self.get_from_idx() == o_from
                    and self.get_symbol_idx() == o_symbol
                    and self.get_to_idx() < oto))

    def __gt__(self, other):
        assert isinstance(other, TransitionTriplet)
        o_from = other.get_from_idx()
        o_symbol = other.get_symbol_idx()
        oto = other.get_to_idx()
        return (self.get_from_idx() > o_from
                or (self.get_from_idx() == o_from
                    and self.get_symbol_idx() > o_symbol)
                or (self.get_from_idx() == o_from
                    and self.get_symbol_idx() == o_symbol
                    and self.get_to_idx() > oto))

    def __eq__(self, other):
        assert isinstance(other, TransitionTriplet)
        o_from = other.get_from_idx()
        o_symbol = other.get_symbol_idx()
        oto = other.get_to_idx()
        return self.get_from_idx() == o_from and self.get_symbol_idx() == o_symbol and self.get_to_idx() == oto

    def get_from_idx(self) -> int:
        return self.__partition_from_idx

    def get_to_idx(self) -> int:
        return self.__partition_to_idx

    def get_symbol_idx(self) -> int:
        return self.__symbol_idx


class Transition(TransitionTriplet):
    def __init__(self, system: System, partition_from_idx: int, symbol_idx: int, partition_to_idx: int,
                 ctx: Context = None):
        super().__init__(partition_from_idx, symbol_idx, partition_to_idx)
        assert ctx is None or isinstance(ctx, Context)
        self.__ctx = ctx
        assert isinstance(system, System)
        self.__system = system
        self.__is_feasible = None

    def sim(self, x0: np.ndarray, u: np.ndarray):
        x = self.get_system().sim(x0, u)
        if (np.all([x[:, k] in self.get_from() for k in range(x.shape[1] - 1)])
                and np.all([x[:, k] in self.get_to() for k in range(1, x.shape[1])])):
            return x
        return None

    def get_system(self) -> System:
        return self.__system

    def get_context(self) -> Context:
        return self.__ctx

    def get_dimension(self) -> int:
        return self.get_system().get_dimension()

    def get_num_of_inputs(self) -> int:
        return self.get_system().get_num_of_inputs()

    def get_from(self) -> Partition:
        return Context.get_partition(self.get_from_idx(), self.get_context())

    def get_to(self) -> Partition:
        return Context.get_partition(self.get_to_idx(), self.get_context())

    def get_symbol(self) -> int:
        return Context.get_symbol(self.get_symbol_idx(), self.get_context())

    def is_feasible(self) -> bool:
        if self.__is_feasible is None:
            self.__is_feasible = self.get_system().is_reachable(self.get_from().get_polytope(),
                                                                self.get_to().get_polytope())
        return self.__is_feasible
